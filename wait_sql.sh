#!/bin/sh

until nc -z -v -w30 "localhost" 3306
do
  echo "Waiting for database connection..."
  sleep 1
done
