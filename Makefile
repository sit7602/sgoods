SHELL = /bin/sh
.DEFAULT_GOAL := help

docker_bin := $(shell command -v docker 2> /dev/null)
docker_compose_bin := $(shell command -v docker-compose 2> /dev/null)

help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

up_local: ## Start all containers FOR LOCAL DEVELOPMENT
	$(docker_compose_bin) -p sgoods_local -f docker-compose-local.yml up -d --build

up_dev: ## Start all containers IN DEVELOPMENT MODE
	$(docker_compose_bin) -p sgoods_dev -f docker-compose-dev.yml up -d --build

up_prod: ## Start all containers IN PRODUCTION MODE
	$(docker_compose_bin) -p sgoods_prod -f docker-compose-prod.yml up -d --build

down_local: ## Stop all started FOR LOCAL DEVELOPMENT
	$(docker_compose_bin) -p sgoods_local -f docker-compose-local.yml down

down_dev: ## Stop all started IN DEVELOPMENT MODE
	$(docker_compose_bin) -p sgoods_dev -f docker-compose-dev.yml down

down_prod: ## Stop all started IN PRODUCTION MODE
	$(docker_compose_bin) -p sgoods_prod -f docker-compose-prod.yml down

restart_web: ## Restart web container
	$(docker_compose_bin) restart web

console: ## Container console
	$(docker_compose_bin) exec app-client sh

