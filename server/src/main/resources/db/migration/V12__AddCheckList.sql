create table checklist (id bigint not null,
                        url varchar(255) not null,
                        description varchar(255),
                        order_question smallint not null,
                        question varchar(100) not null,
                        category_id bigint, primary key (id)) engine=InnoDB;

alter table checklist
    add constraint FKee1hhggg9jiul85mcpophuhf8
        foreign key (category_id)
            references product_category (id);

INSERT INTO sgoods.checklist
    (id, url, description, order_question, question, category_id)
VALUES
    (1, 'url1', 'Краткое описание того, что нужно проверить 1', 1, 'Вы проверили * 1?', 1000000),
    (2, 'url2', 'Краткое описание того, что нужно проверить 2', 2, 'Вы проверили * 2?', 1000000),
    (3, 'url3', 'Краткое описание того, что нужно проверить 3', 3, 'Вы проверили * 3?', 1000000);