create table bug_report (id bigint not null, current_page varchar(255), description varchar(4000),
                         title varchar(255) not null, user_id DECIMAL(25,0) not null, primary key (id)) engine=InnoDB;
alter table message_attachment add column lat double precision null;
alter table message_attachment add column lon double precision null;
alter table message_attachment add column message_id integer;
alter table message_attachment add column bug_report_id bigint;
alter table message_attachment add constraint FK1kpxa77vmjsrm8obbjyit8ldh
    foreign key (message_id) references messages (id);
alter table message_attachment add constraint FK24luisn6vc94uc2wgcdq83ekd
    foreign key (bug_report_id) references bug_report (id);