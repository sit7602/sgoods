alter table user modify last_name varchar(25) null;

alter table product_category drop foreign key FKkoq6v0wxac786v6b41iq4s7ou;
drop index UKsg6qufee0vapbjuwpu8rvi039 on product_category;

alter table announcement drop foreign key FKa11i2g4pdxspvcil437vumyqn;
alter table announcement modify product_category bigint not null;

alter table product_category modify id bigint not null;
alter table product_category modify parent_category bigint null;

alter table product_category add constraint UKsg6qufee0vapbjuwpu8rvi039 unique (parent_category, category);
alter table product_category add constraint FKkoq6v0wxac786v6b41iq4s7ou
    foreign key (parent_category) references product_category (id);

alter table announcement add constraint FKa11i2g4pdxspvcil437vumyqn
    foreign key (product_category) references product_category (id);

delete from field_description where parameter in ('Название объявления', 'Описание объявления', 'Цена', 'Телефон');