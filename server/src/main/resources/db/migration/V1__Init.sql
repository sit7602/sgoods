create table announcement (id bigint not null, create_date datetime not null, description varchar(4000) not null, lat double precision not null, lon double precision not null, name varchar(50) not null, price integer not null, status integer not null, type integer not null, product_category integer not null, buyer_id DECIMAL(25,0), seller_id DECIMAL(25,0) not null, primary key (id)) engine=InnoDB;
create table field_description (id bigint not null, order_parameter smallint not null, parameter varchar(50) not null, product_category_id bigint not null, prompt varchar(100), value_type smallint not null, primary key (id)) engine=InnoDB;
create table hibernate_sequence (next_val bigint) engine=InnoDB;

insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );

create table message_attachment (id integer not null, type integer not null, url varchar(255) not null, primary key (id)) engine=InnoDB;
create table messages (id integer not null, text varchar(500) not null, message_attachment integer, recipient_id DECIMAL(25,0) not null, sender_id DECIMAL(25,0) not null, primary key (id)) engine=InnoDB;
create table notification (id bigint not null, data varchar(4000) not null, date_time datetime not null, type integer, user_id DECIMAL(25,0) not null, primary key (id)) engine=InnoDB;
create table offer (id bigint not null, description varchar(500), new_price integer, offer_status integer, offer_valid_until datetime, announcement_id bigint, buyer_id DECIMAL(25,0), buyer_account_id bigint not null, primary key (id)) engine=InnoDB;
create table photo (id integer not null, announcement_id bigint, fileurl varchar(250) not null, primary key (id)) engine=InnoDB;
create table product_category (id integer not null, category varchar(50), parent_category integer, primary key (id)) engine=InnoDB;
create table purchase (id bigint not null, buyer_accept bit not null, seller_accept bit not null, start_time datetime, offer_id bigint, primary key (id)) engine=InnoDB;
create table user (id DECIMAL(25,0) not null, first_name varchar(25) not null, last_name varchar(25) not null, photo varchar(250) not null, primary key (id)) engine=InnoDB;
create table user_details (user_id DOUBLE(25,0) not null, phone varchar(10) not null, registration_date datetime not null, primary key (user_id)) engine=InnoDB;
create table user_settings (user_id DOUBLE(25,0) not null, utc smallint, call_time_end time, call_time_start time, default_user_card_id bigint, hide_user_date bit not null, primary key (user_id)) engine=InnoDB;
create table user_account (id bigint not null, balance double precision not null, card_number varchar(16) not null, reserved double precision, user_id DECIMAL(25,0), primary key (id)) engine=InnoDB;

alter table product_category add constraint UKsg6qufee0vapbjuwpu8rvi039 unique (parent_category, category);
alter table user_details add constraint UK_hwi9icwrbompowjo1l8mqqu49 unique (phone);
alter table announcement add constraint FKa11i2g4pdxspvcil437vumyqn foreign key (product_category) references product_category (id);
alter table announcement add constraint FKgeycwuecxgwlljn033try25g8 foreign key (buyer_id) references user (id);
alter table announcement add constraint FKkjp0ij7gmr87d524p9mfrylx4 foreign key (seller_id) references user (id);
alter table messages add constraint FKhta5rqvu10b86rcdvbw6t0q07 foreign key (message_attachment) references message_attachment (id);
alter table messages add constraint FKhky628e8v09g8h9qg27jab05v foreign key (recipient_id) references user (id);
alter table messages add constraint FKip9clvpi646rirksmm433wykx foreign key (sender_id) references user (id);
alter table notification add constraint FKb0yvoep4h4k92ipon31wmdf7e foreign key (user_id) references user (id);
alter table offer add constraint FKketnoqdi3j8fvf9vpphy84bik foreign key (buyer_id) references user (id);
alter table offer add constraint FK32677u3xsrit3yp56vw663rib foreign key (buyer_account_id) references user_account (id);
alter table offer add constraint FKdjswhupluprv1d9nkyrpnbgga foreign key (announcement_id) references announcement (id);
alter table photo add constraint FKgq65fpi3pdkspnks9x4wrqps2 foreign key (announcement_id) references announcement (id);
alter table product_category add constraint FKkoq6v0wxac786v6b41iq4s7ou foreign key (parent_category) references product_category (id);
alter table purchase add constraint FK8b5b4vp8smfuvge8vgx767b43 foreign key (offer_id) references offer (id)