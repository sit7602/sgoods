create table favorite_announcement (
                                    id bigint not null,
                                    announcement_id bigint,
                                    user_id DECIMAL(25,0),
                                    primary key (id)
                                  )
                    engine=InnoDB;
alter table favorite_announcement add
    constraint FKfyiy8diagrob90y0jmeniaab5
        foreign key (announcement_id)
            references announcement (id);
alter table favorite_announcement add
    constraint FK4kimv5ojysofbfr10hxutqu2j
        foreign key (user_id)
            references user (id);
alter table favorite_announcement add
    constraint UKl35utyvs18fdb27yjstgsxr8f
        unique (user_id, announcement_id);