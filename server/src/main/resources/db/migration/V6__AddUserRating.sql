create table user_review (id bigint not null,
                          auto_create bit not null,
                          description varchar(1000),
                          mark integer not null,
                          type integer,
                          offer_id bigint not null,
                          user_from DECIMAL(25,0),
                          user_to DECIMAL(25,0),
             primary key (id)) engine=InnoDB;
alter table user_review add constraint FK7jo1l6sni3vhvfrbxla52w1il foreign key (user_from) references user (id);
alter table user_review add constraint FK8gcinsijb9dfnixte18ycdo43 foreign key (user_to) references user (id);
alter table user_review add constraint FK35vd8fdloqi2g6eocqja0dcy3 foreign key (offer_id) references offer (id);