package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.userReview.UserReviewFrom;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface UserReviewFromRepository extends JpaRepository<UserReviewFrom, Integer> {
    List<UserReviewFrom> findAllByUserFrom(BigInteger userFrom);

    UserReviewFrom findByUserFromAndOfferId(BigInteger userFrom, long offerId);
}
