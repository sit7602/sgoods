package com.spp.sgoods.repository;

import com.spp.sgoods.model.BugReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.List;

public interface BugReportRepository extends JpaRepository<BugReport, Integer> {
    @NonNull
    List<BugReport> findAll();
}