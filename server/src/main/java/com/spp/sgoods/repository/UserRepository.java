package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findById(BigInteger userId);
}
