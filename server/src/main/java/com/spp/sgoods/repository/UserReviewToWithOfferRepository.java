package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.userReview.UserReviewToWithOffer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface UserReviewToWithOfferRepository extends JpaRepository<UserReviewToWithOffer, Integer> {
    List<UserReviewToWithOffer> findAllByUserTo(BigInteger userTo);
}
