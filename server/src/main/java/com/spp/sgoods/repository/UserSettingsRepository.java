package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.UserSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface UserSettingsRepository extends JpaRepository<UserSettings, Integer> {
        UserSettings getUserSettingsByUserId(BigInteger userId);
}