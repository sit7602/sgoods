package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.AnnouncementForSearch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnouncementForSearchRepository extends ElasticsearchRepository<AnnouncementForSearch, Long> {
}
