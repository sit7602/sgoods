package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.AnnouncementForSeller;
import com.spp.sgoods.model.purchaseSystem.Offer;
import com.spp.sgoods.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface AnnouncementForSellerRepository extends JpaRepository<AnnouncementForSeller, Integer> {
    AnnouncementForSeller findById(long announcementId);
    AnnouncementForSeller findByIdAndSeller_Id(long id, BigInteger seller_id);
    AnnouncementForSeller findByIdAndSeller_IdAndOffers(long id, BigInteger sellerId, Offer offer);

    List<AnnouncementForSeller> findAllBySeller(User seller);

    boolean existsByIdAndSeller_IdAndOffers(long id, BigInteger seller_id, Offer offer);
}