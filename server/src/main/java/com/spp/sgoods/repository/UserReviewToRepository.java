package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.userReview.UserReviewTo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface UserReviewToRepository extends JpaRepository<UserReviewTo, Integer> {
    List<UserReviewTo> findAllByUserTo(BigInteger userTo);
}