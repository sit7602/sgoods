package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.Announcement;
import com.spp.sgoods.model.announcement.AnnouncementStatus;
import com.spp.sgoods.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Integer>{
    Announcement findById(long announcementId);
    List<Announcement> findAllByIdIn(List<Long> ids);

    List<Announcement> findAllByBuyerAndStatus(User buyer, AnnouncementStatus status);

    boolean existsById(long announcementId);

    int countAllByProductCategory_IdAndStatus(long productCategory_id, AnnouncementStatus status);

    List<Announcement> findAllByStatus(AnnouncementStatus status);
}
