package com.spp.sgoods.repository;

import com.spp.sgoods.model.purchaseSystem.Offer;
import com.spp.sgoods.model.purchaseSystem.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    Purchase findFirstByOfferOrderByIdDesc(Offer offer);
}
