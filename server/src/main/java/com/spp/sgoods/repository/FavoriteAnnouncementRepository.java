package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.FavoriteAnnouncement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface FavoriteAnnouncementRepository extends JpaRepository<FavoriteAnnouncement, Integer> {
    List<FavoriteAnnouncement> findAllByUserId(BigInteger userId);

    boolean existsByUserIdAndAnnouncementId(BigInteger userId, long announcementId);

    void deleteByUserIdAndAnnouncementId(BigInteger userId, long announcementId);
}