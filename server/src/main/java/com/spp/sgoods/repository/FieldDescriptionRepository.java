package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.FieldDescription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FieldDescriptionRepository extends JpaRepository<FieldDescription, Integer> {
    List<FieldDescription> findAllByProductCategoryIdInOrderByProductCategoryIdAscOrderParameterAsc(List<Long> productCategoryIdList);
}
