package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Integer> {
    List<UserAccount> findAllByUserId(BigInteger userId);
    UserAccount findById(long id);
}
