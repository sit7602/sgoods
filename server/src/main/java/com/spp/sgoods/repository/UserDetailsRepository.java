package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, Integer> {
    UserDetails getUserDetailsByUserId(BigInteger userId);
}
