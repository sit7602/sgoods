package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhotoRepository extends JpaRepository<Photo, Integer> {
    List<Photo> findAllByAnnouncementId(long announcementId);
}
