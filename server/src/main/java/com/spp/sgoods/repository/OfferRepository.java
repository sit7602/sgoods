package com.spp.sgoods.repository;

import com.spp.sgoods.model.purchaseSystem.Offer;
import com.spp.sgoods.model.purchaseSystem.OfferStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer> {
    Offer getById(long offerId);

    List<Offer> findAllBy();

    boolean existsByAnnouncementIdAndOfferStatusIn(long announcementId, OfferStatus[] offerStatus);
    boolean existsByAnnouncementIdAndBuyer_IdAndOfferStatusIn(long announcementId, BigInteger buyerId, OfferStatus[] offerStatuses);
}
