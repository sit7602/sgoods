package com.spp.sgoods.repository;

import com.spp.sgoods.model.purchaseSystem.OfferStatus;
import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OfferWithAnnouncementRepository extends JpaRepository<OfferWithAnnouncement, Integer> {
    List<OfferWithAnnouncement> findAllByBuyerId(BigInteger buyer_id);
    List<OfferWithAnnouncement> findAllByOfferStatusInAndOfferValidUntilIsLessThan(OfferStatus[] offerStatus,
                                                                                   LocalDateTime offerValidUntil);
}
