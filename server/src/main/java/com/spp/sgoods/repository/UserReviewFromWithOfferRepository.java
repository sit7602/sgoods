package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.userReview.UserReviewFromWithOffer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface UserReviewFromWithOfferRepository extends JpaRepository<UserReviewFromWithOffer, Integer> {
    List<UserReviewFromWithOffer> findAllByUserFrom(BigInteger userFrom);
}
