package com.spp.sgoods.repository;

import com.spp.sgoods.model.user.userReview.UserReview;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReviewRepository extends JpaRepository<UserReview, Integer> {
}
