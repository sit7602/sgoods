package com.spp.sgoods.repository;

import com.spp.sgoods.model.announcement.ProductCategoryForInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ProductCategoryForInfoRepository extends JpaRepository<ProductCategoryForInfo, Integer> {
    List<ProductCategoryForInfo> findAllByParentIdIsNull();
    ProductCategoryForInfo findById(long productId);
}
