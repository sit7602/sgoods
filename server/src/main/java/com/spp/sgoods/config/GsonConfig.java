package com.spp.sgoods.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spp.sgoods.gson.deserializer.AbstractAttachmentDeserializer;
import com.spp.sgoods.gson.deserializer.LocalDateDeserializer;
import com.spp.sgoods.gson.deserializer.LocalDateTimeDeserializer;
import com.spp.sgoods.gson.deserializer.LocalTimeDeserializer;
import com.spp.sgoods.gson.hiddenAnnotation.HiddenAnnotationExclusionStrategy;
import com.spp.sgoods.gson.serializer.LocalDateSerializer;
import com.spp.sgoods.gson.serializer.LocalDateTimeSerializer;
import com.spp.sgoods.gson.serializer.LocalTimeSerializer;
import com.spp.sgoods.model.attachment.AttachmentAbstract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Configuration
public class GsonConfig {
    @Bean
    public Gson gson(){
        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.serializeNulls();
        gsonBuilder.setExclusionStrategies( new HiddenAnnotationExclusionStrategy() );

        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer());
        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
        gsonBuilder.registerTypeAdapter(LocalTime.class, new LocalTimeDeserializer());
        gsonBuilder.registerTypeAdapter(AttachmentAbstract.class,new AbstractAttachmentDeserializer());

        gsonBuilder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer());
        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
        gsonBuilder.registerTypeAdapter(LocalTime.class, new LocalTimeSerializer());

        return gsonBuilder.create();
    }

    @Bean
    public HttpMessageConverter<Object> gsonHttpMessageConverter() {
        GsonHttpMessageConverter gsonHttpMessageConverter = new GsonHttpMessageConverter();
        gsonHttpMessageConverter.setGson(gson());
        return gsonHttpMessageConverter;
    }
}
