package com.spp.sgoods.service;

public interface EmailService {
    boolean sendEmail(String mailTo, String title, String message);
}
