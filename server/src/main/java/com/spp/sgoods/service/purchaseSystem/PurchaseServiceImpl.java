package com.spp.sgoods.service.purchaseSystem;

import com.spp.sgoods.exeption.OfferException;
import com.spp.sgoods.jsonEntity.notification.purchase.*;
import com.spp.sgoods.model.announcement.AnnouncementAbstract;
import com.spp.sgoods.model.announcement.AnnouncementForSeller;
import com.spp.sgoods.model.announcement.AnnouncementStatus;
import com.spp.sgoods.model.purchaseSystem.Offer;
import com.spp.sgoods.model.purchaseSystem.OfferStatus;
import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;
import com.spp.sgoods.model.purchaseSystem.Purchase;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.model.user.UserSettings;
import com.spp.sgoods.model.user.userReview.UserReviewType;
import com.spp.sgoods.repository.*;
import com.spp.sgoods.service.notification.NotificationService;
import com.spp.sgoods.service.user.UserAccountService;
import com.spp.sgoods.service.user.UserReviewService;
import com.spp.sgoods.service.user.UserService;
import com.spp.sgoods.service.util.PdfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    @Value("${content.storage.path}")
    private String contentStoragePath;

    @Value("${content.path}")
    private String contentPath;

    @Autowired
    private AnnouncementForSellerRepository announcementForSellerRepository;

    @Autowired
    private AnnouncementForSearchRepository announcementForSearchRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferWithAnnouncementRepository offerWithAnnouncementRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private UserReviewService userReviewService;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PdfService pdfService;

    @Transactional
    public boolean createOffer(long announcementId, BigInteger buyerId, long cardId, int offerValidSeconds,
                               Integer newPrice, String description) {
        AnnouncementForSeller announcement = announcementForSellerRepository.findById(announcementId);
        if(!announcement.getStatus().equals(AnnouncementStatus.OPEN))
            throw new OfferException("Статус объявления " + announcement.getStatus().name());

        if(offerRepository.existsByAnnouncementIdAndOfferStatusIn(announcementId,
                new OfferStatus[]{OfferStatus.OFFER_ACCEPTED, OfferStatus.SOLD}))
            throw new OfferException("Предложение больше не действует");

        if(offerRepository.existsByAnnouncementIdAndBuyer_IdAndOfferStatusIn(announcementId, buyerId,
                new OfferStatus[]{OfferStatus.OFFER, OfferStatus.BARGAIN}))
            throw new OfferException("Вы уже сделали предложение о покупке");

        User buyer =  userService.getUser(buyerId);
        User seller = announcement.getSeller();
        if(seller.equals(buyer))
            throw new OfferException("Покупатель не может быть продавцом");

        UserAccount buyerAccount = userAccountService.getUserAccount(buyerId, cardId);
        if(announcement.getPrice() > buyerAccount.getBalance())
            throw new OfferException("Недостаточно средств на карте  " + buyerAccount.getId());

        Offer offer;
        if(description == null && newPrice == null) {
            offer = new Offer(buyer, buyerAccount, offerValidSeconds);
            announcement.setOffers(offer);
            buyerAccount.reserved(announcement.getPrice());
            notificationService.createNotification(seller, new PurchaseOfferNotification(announcement, offer));
        }
        else {
            if(!announcement.isBargain())
                throw new OfferException("Продавец не готов торговаться");
            if (announcement.getMinPrice() > newPrice)
                throw new OfferException("Продавец не принял новую цену");
            offer = new Offer(buyer, buyerAccount, offerValidSeconds, newPrice, description);
            announcement.setOffers(offer);
            buyerAccount.reserved(newPrice);
            notificationService.createNotification(seller, new OfferNewPriceNotification(announcement, offer));
        }
        if (announcement.getProductCategory().getId() == 1000000)
            taskScheduler.schedule(() -> purchaseService.acceptOffer(announcementId, seller.getId(), offer.getId()),
                                                                                        Instant.now().plusSeconds(10));
        return true;
    }

    @Transactional
    public boolean autoCreateOffer(long announcementId, Integer price, Integer minPrice) {
        BigInteger buyerId = BigInteger.ZERO;
        long cardId = 0;
        int offerValidSeconds = 3600;
        if (minPrice == null)
            return purchaseOffer(announcementId, buyerId, cardId, offerValidSeconds);
        else
            return bargainOffer(announcementId, buyerId, cardId, offerValidSeconds,
                    (int) (Math.random() * (price - minPrice) + minPrice),
                    "Предложение создано автоматически");
    }

    @Override
    @Transactional
    public boolean autoCreatePurchaseOffer(long announcementId) {
        return autoCreateOffer(announcementId, null, null);
    }

    @Override
    @Transactional
    public boolean autoCreateBargainOffer(long announcementId, Integer price, Integer minPrice) {
        return autoCreateOffer(announcementId, price, minPrice);
    }

    @Override
    @Transactional
    public boolean purchaseOffer(long announcementId, BigInteger buyerId, long cardId, int offerValidSeconds) {
        return createOffer(announcementId, buyerId, cardId, offerValidSeconds, null, null);
    }

    @Override
    @Transactional
    public boolean bargainOffer(long announcementId, BigInteger buyerId, long cardId, int offerValidSeconds,
                                Integer newPrice, String description) {
        return createOffer(announcementId, buyerId, cardId, offerValidSeconds, newPrice, description);
    }

    private Offer findOffer(long announcementId, long offerId) {
        if(offerRepository.existsByAnnouncementIdAndOfferStatusIn(announcementId,
                new OfferStatus[]{OfferStatus.OFFER_ACCEPTED, OfferStatus.SOLD}))
            throw new OfferException("У объявления статус - зарезервировано или продано");

        Offer offer = offerRepository.getById(offerId);
        if(offer == null)
            throw new OfferException("Предложение не найдено");

        if(offer.getAnnouncementId() != announcementId)
            throw new OfferException("Предложение относится к другому объявлению");

        if(offer.getOfferValidUntil().isBefore(LocalDateTime.now()))
            throw new OfferException("Истекло время предложения");

        return offer;
    }

    @Override
    @Transactional
    public boolean acceptOffer(long announcementId, BigInteger sellerId, long offerId) {

        Offer offer = findOffer(announcementId, offerId);

        AnnouncementForSeller announcement = announcementForSellerRepository
                .findByIdAndSeller_IdAndOffers(announcementId, sellerId, offer);
        if(announcement == null)
            throw new OfferException("Объявление не найдено");

        announcement.setStatus(AnnouncementStatus.RESERVED);
        announcement.setBuyer(offer.getBuyer());
        offer.setOfferStatus(OfferStatus.OFFER_ACCEPTED);

        rejectOffers(announcement, announcement.getOffers());

        announcementForSearchRepository.deleteById(announcementId);

        notificationService.createNotification(offer.getBuyer(),
                new ConformationOfSalesNotification(announcement, offer));
        return true;
    }

    @Override
    @Transactional
    public boolean rejectOffer(long announcementId, BigInteger sellerId, long offerId) {
        Offer offer = findOffer(announcementId, offerId);
        AnnouncementForSeller announcement = announcementForSellerRepository
                .findByIdAndSeller_IdAndOffers(announcementId, sellerId, offer);
        if(announcement == null)
            throw new OfferException("Объявление не найдено");

        return rejectOffer(announcement, offer);
    }

    @Override
    @Transactional
    public boolean rejectOffers(long announcementId, BigInteger sellerId) {
        AnnouncementForSeller announcement = announcementForSellerRepository
                .findByIdAndSeller_Id(announcementId, sellerId);
        if(announcement == null)
            throw new OfferException("Объявление не найдено");

        rejectOffers(announcement, announcement.getOffers());

        return true;
    }

    private boolean rejectOffers(AnnouncementAbstract announcement, List<Offer> offers) {
        offers.forEach(offer -> rejectOffer(announcement, offer));
        return true;
    }

    private boolean rejectOffer(AnnouncementAbstract announcement, Offer offer) {
        if (offer.getOfferStatus() !=  OfferStatus.OFFER && offer.getOfferStatus() !=  OfferStatus.BARGAIN)
            return false;

        offer.setOfferStatus(OfferStatus.OFFER_REJECTED);

        UserAccount buyerAccount = offer.getBuyerAccount();
        Integer price;
        if (offer.getNewPrice() == null)
            price = announcement.getPrice();
        else
            price = offer.getNewPrice();
        buyerAccount.unReserved(price);

        notificationService.createNotification(offer.getBuyer(),
                new RejectOfferNotification(announcement, offer));
        return true;
    }

    @Override
    @Transactional
    public Purchase completionOfPurchase(long offerId, BigInteger userId) {
        Offer offer = offerRepository.getById(offerId);
        if (offer == null)
            throw new OfferException("Предложение не найдено");
        if (!offer.getOfferStatus().equals(OfferStatus.OFFER_ACCEPTED))
            throw new OfferException("Предложение не принято");
        AnnouncementForSeller announcement = announcementForSellerRepository.findById(offer.getAnnouncementId());
        User seller = announcement.getSeller();
        User buyer = offer.getBuyer();
        Purchase purchase;
        if (announcement.getProductCategory().getId() != 1000000)
            purchase = purchaseRepository.findFirstByOfferOrderByIdDesc(offer);
        else {
            purchase = new Purchase();
            purchase.setOffer(offer);
            if (seller.getId().equals(BigInteger.ZERO)) {
                purchase.sellerAccept();
            }
            else {
                purchase.buyerAccept();
            }
        }

        if (purchase == null || purchase.getStartTime().plusMinutes(5).isBefore(LocalDateTime.now())) {
            purchase = new Purchase();
            purchase.setOffer(offer);
            User sendTo = null;
            if (buyer.getId().equals(userId)) {
                purchase.buyerAccept();
                sendTo = seller;
            }
            if (seller.getId().equals(userId)) {
                purchase.sellerAccept();
                sendTo = buyer;
            }

            if (sendTo == null)
                throw new OfferException("Предложение не найдено") ;

            purchaseRepository.save(purchase);
            notificationService.createNotification(sendTo, new TransferConformationNotification(announcement, offer));
        }
         else {
            if (buyer.getId().equals(userId) && purchase.isSellerAccept())
                purchase.buyerAccept();

            if (seller.getId().equals(userId) && purchase.isBuyerAccept())
                purchase.sellerAccept();

            if (!purchase.isBuyerAccept() || !purchase.isSellerAccept())
                throw new OfferException("Ошибка при подтверждении");

            announcement.setStatus(AnnouncementStatus.CLOSE);
            offer.setOfferStatus(OfferStatus.SOLD);

            UserSettings sellerSettings = userService.getUserSettings(seller.getId());
            UserAccount sellerAccount = userAccountService.getUserAccount(seller.getId(),
                                                                          sellerSettings.getDefaultUserCardId());
            UserAccount buyerAccount = offer.getBuyerAccount();

            Integer price;
            if (offer.getNewPrice() == null)
                price = announcement.getPrice();
            else
                price = offer.getNewPrice();

            sellerAccount.plus(price);
            buyerAccount.minus(price);

            offer.setContract(generateContract(announcement, price));

            notificationService.createNotification(seller, new SoldNotification(announcement, offer));
            userReviewService.autoCreateUserReview(seller, buyer, UserReviewType.AFTER_THE_PURCHASE_SELLER, offerId);
            notificationService.createNotification(buyer, new SoldNotification(announcement, offer));
            userReviewService.autoCreateUserReview(buyer, seller, UserReviewType.AFTER_THE_PURCHASE_BUYER, offerId);
        }
        return purchase;
    }

    private String generateContract(AnnouncementAbstract announcement, int price) {
        Map<String, Object> variables = new HashMap<>();
        LocalDate localDate = LocalDate.now();
        String date = localDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
                .withLocale(new Locale("ru")));
        Random random = new Random();

        variables.put("numDog", random.nextInt(100000));
        variables.put("product", announcement.getName());
        variables.put("price", price);

        variables.put("date", date);

        variables.put("sellerName", announcement.getSeller().getFullName());
        variables.put("sellerPhone", "89" + (100000000 + random.nextInt(899999999)));
        variables.put("sellerPassportNum", 10000000 + random.nextInt(89999999));
        variables.put("sellerPassportDate", localDate.minusDays(random.nextInt(1000)));

        variables.put("buyerName", announcement.getBuyer().getFullName());
        variables.put("buyerPhone", "89" + (100000000 + random.nextInt(899999999)));
        variables.put("buyerPassportNum", 10000000 + random.nextInt(89999999));
        variables.put("buyerPassportDate", localDate.minusDays(random.nextInt(1000)));

        String contractFileName = UUID.randomUUID().toString() + ".pdf";
        if (!pdfService.generatePdfThymeleaf(variables, "contract", contentStoragePath + "/" + contractFileName))
            return null;
        return contentPath + "/" + contractFileName;
    }

    @Scheduled(fixedDelay = 10000)
    protected void autoCloseOffers(){
        List<OfferWithAnnouncement> offers = offerWithAnnouncementRepository
                .findAllByOfferStatusInAndOfferValidUntilIsLessThan(new OfferStatus[]{OfferStatus.OFFER, OfferStatus.BARGAIN},
                                                                       LocalDateTime.now());
        for (OfferWithAnnouncement offer : offers) {
            offer.setOfferStatus(OfferStatus.STANDBY_TIME_IS_OUT);
            UserAccount buyerAccount = offer.getBuyerAccount();
            Integer price;
            if (offer.getNewPrice() == null)
                price = offer.getAnnouncement().getPrice();
            else
                price = offer.getNewPrice();
            buyerAccount.unReserved(price);
            offerWithAnnouncementRepository.save(offer);

//            Включить после демо
//            notificationService.createNotification(offer.getBuyer(),
//                    new TimeOutWaitingNotification(offer));
        }
    }
}
