package com.spp.sgoods.service.googleApi;

import com.google.gson.Gson;
import com.spp.sgoods.jsonEntity.googleApi.GoogleTokenRequest;
import com.spp.sgoods.jsonEntity.googleApi.GoogleTokenResponse;
import com.spp.sgoods.jsonEntity.googleApi.GoogleUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.spp.sgoods.service.httpClient.HttpClientService;

@Component
public class GoogleOAuthServiceImpl implements GoogleOAuthService{

    @Autowired
    private HttpClientService httpClientService;

    @Autowired
    private Gson gson;

    public GoogleTokenResponse GetUserToken(String code){
        GoogleTokenRequest tokenRequest = new GoogleTokenRequest(
                "302725075150-3ehmnkptnufankminuroda9f7fnnb63i.apps.googleusercontent.com",
                "JoxNbq_G4WtVTBi09V4J2TE2",
                code,
                "authorization_code",
                "http://localhost/login");
        try{
            String jsonResponse = httpClientService.post("https://oauth2.googleapis.com/token", tokenRequest);
            return gson.fromJson(jsonResponse, GoogleTokenResponse.class);
        }
        catch (Exception e){
            return null;
        }
    }

    public GoogleUserInfo GetUserInfo(String accessToken) {
        String url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accessToken;
        try{
            String jsonResponse = httpClientService.get(url);
            return gson.fromJson(jsonResponse, GoogleUserInfo.class);
        }
        catch (Exception e){
            return null;
        }
    }
}
