package com.spp.sgoods.service.authentication;

import com.spp.sgoods.jsonEntity.authentication.TokenResponse;
import com.spp.sgoods.model.TokenStorage;

import java.math.BigInteger;
import java.util.Map;

public interface TokenService {
    TokenResponse createToken(BigInteger userId);
    TokenResponse refreshToken(String accessToken, String refreshToken);
    BigInteger getUserId(String refreshAccess);
    Map<String, TokenStorage> getTokenStorageMap();
}
