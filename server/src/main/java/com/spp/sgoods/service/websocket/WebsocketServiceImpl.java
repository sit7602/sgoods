package com.spp.sgoods.service.websocket;

import com.spp.sgoods.model.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Service;

@Service
public class WebsocketServiceImpl implements WebsocketService{
    @Autowired
    private SimpMessageSendingOperations simpMessageSendingOperations;

    @Autowired
    private SimpUserRegistry simpUserRegistry;

    @Override
    public void sendNotification(Notification notification) {
        simpMessageSendingOperations.convertAndSendToUser(notification.getUser().getId().toString(),
                "/queue/reply",
                notification);
    }
}
