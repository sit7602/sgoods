package com.spp.sgoods.service.util;

import com.spp.sgoods.model.BugReport;

import java.math.BigInteger;
import java.util.List;

public interface BugReportService {
    boolean createBugReport(BigInteger userId, BugReport bugReport);
    List<BugReport> getBugReports();
}
