package com.spp.sgoods.service.websocket;

import com.spp.sgoods.model.notification.Notification;

import java.math.BigInteger;

public interface WebsocketService {
    void sendNotification(Notification notification);
}
