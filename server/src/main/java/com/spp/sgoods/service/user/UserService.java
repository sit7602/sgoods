package com.spp.sgoods.service.user;

import com.spp.sgoods.jsonEntity.announcement.SellerInfo;
import com.spp.sgoods.jsonEntity.googleApi.GoogleUserInfo;
import com.spp.sgoods.jsonEntity.user.UserInfo;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;

import java.math.BigInteger;
import java.util.List;

public interface UserService {
    User getUser(BigInteger userId);
    User createUser(GoogleUserInfo googleUserInfo);

    UserInfo getUserInfo(BigInteger userId);
    UserDetails setUserDetails(BigInteger userId, UserDetails userDetails);
    UserSettings setUserSettings(BigInteger userId, UserSettings userSettings);
    UserInfo setUserDetailsAndSettings(BigInteger userId, UserDetails userDetails, UserSettings userSettings);

    UserDetails getUserDetails(BigInteger userId);
    UserSettings getUserSettings(BigInteger userId);

    SellerInfo getSellerInfo(BigInteger userId);
}
