package com.spp.sgoods.service.announcement;

import com.google.gson.Gson;
import com.spp.sgoods.exeption.AnnouncementParserException;
import com.spp.sgoods.jsonEntity.announcement.ParsedAnnouncementResponse;
import com.spp.sgoods.service.httpClient.HttpClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.UUID;

@Service
public class AnnouncementParserServiceImpl implements AnnouncementParserService{

    @Value("${content.storage.path}")
    private String contentStoragePath;

    @Value("${content.path}")
    private String contentPath;

    @Value("${parser.host}")
    private String parserHost;

    @Autowired
    private HttpClientService httpClient;

    @Autowired
    private Gson gson;

    @Override
    public ParsedAnnouncementResponse parseAnnouncement(String url, BigInteger userId) throws IOException {
        String request = parserHost + "/get_info/?url=" + url;
        String response;
        ParsedAnnouncementResponse parsedAnnouncementResponse;
        try {
            response = httpClient.get(request);
        } catch (Exception e) {
            throw new AnnouncementParserException("Ошибка при парсинге");
        }
        try {
            parsedAnnouncementResponse = gson.fromJson(response, ParsedAnnouncementResponse.class);
        } catch (Exception e) {
            throw new AnnouncementParserException("Объявление не найдено");
        }
        if (parsedAnnouncementResponse.getPhoto() != null) {
            String photoURL = "https://" + parsedAnnouncementResponse.getPhoto();
            File uploadDirectory = new File(contentStoragePath + "/" + userId);
            if (!uploadDirectory.exists()) {
                uploadDirectory.mkdir();
            }
            String filename = UUID.randomUUID().toString() + ".jpg";
            String savePhotoPath = uploadDirectory + "/" + filename;
            try (BufferedInputStream in = new BufferedInputStream(new URL("https://" +
                    parsedAnnouncementResponse.getPhoto()).openStream());
                 FileOutputStream fileOutputStream = new FileOutputStream(savePhotoPath)) {
                byte[] dataBuffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    fileOutputStream.write(dataBuffer, 0, bytesRead);
                }
            }
            parsedAnnouncementResponse.setPhoto(contentPath + "/" + userId + "/" + filename);
            parsedAnnouncementResponse.setFilename(filename);
        }
        return parsedAnnouncementResponse;
    }
}
