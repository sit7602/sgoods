package com.spp.sgoods.service.user;

import com.spp.sgoods.exeption.UserRatingException;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.userReview.*;
import com.spp.sgoods.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;

@Service
public class UserReviewServiceImpl implements UserReviewService {

    @Autowired
    private UserReviewRepository userReviewRepository;

    @Autowired
    private UserReviewToRepository userReviewToRepository;

    @Autowired
    private UserReviewFromRepository userReviewFromRepository;

    @Autowired
    private UserReviewToWithOfferRepository userReviewToWithOfferRepository;

    @Autowired
    private UserReviewFromWithOfferRepository userReviewFromWithOfferRepository;

    @Override
    public boolean autoCreateUserReview(User userFrom, User userTo, UserReviewType type, long offerId) {
        UserReview userReview = new UserReview(userFrom, userTo, 5, type, offerId, true);
        userReviewRepository.save(userReview);
        return true;
    }

    @Override
    public boolean createUserReview(User userFrom, User userTo, int mark, UserReviewType type, long offerId) {
        UserReview userReview = new UserReview(userFrom, userTo, mark, type, offerId, false);
        userReviewRepository.save(userReview);
        return true;
    }

    @Override
    public boolean createUserReview(User userFrom, User userTo, int mark,
                                    UserReviewType type, long offerId, String description) {
        UserReview userReview = new UserReview(userFrom, userTo, mark, type, offerId, description);
        userReviewRepository.save(userReview);
        return true;
    }

    @Override
    @Transactional
    public boolean updateUserReview(BigInteger userId, long offerId, int mark) {
        return updateUserReview(userId, offerId, mark, null);
    }

    @Override
    @Transactional
    public boolean updateUserReview(BigInteger userId, long offerId, int mark, String description) {
        UserReviewFrom userReview = userReviewFromRepository.findByUserFromAndOfferId(userId, offerId);
        if (userReview == null)
            throw new UserRatingException("Отзыв не найден");
        if (!userReview.isAutoCreate())
            throw new UserRatingException("Вы уже дали обратную связь по этой сделке");
        if (mark < 1 || mark > 5)
            throw new UserRatingException("Оценка должны находить в промежутке от 1 до 5");
        userReview.setMark(mark);
        if (description != null)
            userReview.setDescription(description);
        else if (mark != 5)
            throw new UserRatingException("Для оценки ниже 5 должно быть обоснование");
        userReview.setAutoCreate(false);
        userReview.updateCreateDate();
        return true;
    }

    @Override
    public List<UserReviewTo> getUserReviews(BigInteger userId) {
        return userReviewToRepository.findAllByUserTo(userId);
    }

    @Override
    public List<UserReviewToWithOffer> getUserReviewTo(BigInteger userId) {
        return userReviewToWithOfferRepository.findAllByUserTo(userId);
    }

    @Override
    public List<UserReviewFromWithOffer> getUserReviewFrom(BigInteger userId) {
        return userReviewFromWithOfferRepository.findAllByUserFrom(userId);
    }
}