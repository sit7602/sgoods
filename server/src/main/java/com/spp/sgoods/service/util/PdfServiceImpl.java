package com.spp.sgoods.service.util;

import com.lowagie.text.pdf.BaseFont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.FSEntityResolver;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;

@Service
public class PdfServiceImpl implements PdfService {

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Override
    public boolean generatePdfThymeleaf(Map<String, Object> variables, String templatePath, String out) {

        try (OutputStream os = new FileOutputStream(out)) {
            final Context ctx = new Context();
            ctx.setVariables(variables);
            String html = templateEngine.process(templatePath, ctx);

            ITextRenderer renderer = new ITextRenderer();
            //add some fonts - if paths are not right, an exception will be thrown
            renderer.getFontResolver().addFont("Arimo-Bold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            documentBuilderFactory.setValidating(false);
            DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
            builder.setEntityResolver(FSEntityResolver.instance());
            org.w3c.dom.Document document = builder.parse(new ByteArrayInputStream(
                    html.getBytes("UTF-8")));

            renderer.setDocument(document, null);
            renderer.layout();
            renderer.createPDF(os);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
