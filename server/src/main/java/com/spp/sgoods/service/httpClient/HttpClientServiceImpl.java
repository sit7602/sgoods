package com.spp.sgoods.service.httpClient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpClientServiceImpl implements HttpClientService {
    private RestTemplate rest = new RestTemplate();
    private HttpHeaders headers = new HttpHeaders();

    public HttpClientServiceImpl() {
        headers.add("Content-Type", "application/json");
    }

    public String get(String uri) {
        HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
        ResponseEntity<String> responseEntity = rest.exchange(uri, HttpMethod.GET, requestEntity, String.class);
        return responseEntity.getBody();
    }

    public String post(String uri, Object json) {
        HttpEntity<Object> requestEntity = new HttpEntity<>(json, headers);
        ResponseEntity<String> responseEntity = rest.exchange(uri, HttpMethod.POST, requestEntity, String.class);
        return responseEntity.getBody();
    }

    public String delete(String uri) {
        HttpEntity<Object> requestEntity = new HttpEntity<>("", headers);
        ResponseEntity<String> responseEntity = rest.exchange(uri, HttpMethod.DELETE, requestEntity, String.class);
        return responseEntity.getBody();
    }
}
