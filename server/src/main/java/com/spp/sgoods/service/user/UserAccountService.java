package com.spp.sgoods.service.user;

import com.spp.sgoods.model.user.UserAccount;

import java.math.BigInteger;
import java.util.List;

public interface UserAccountService {
    void createUserAccount(BigInteger userId);

    List<UserAccount> getUserAccounts(BigInteger userId);

    UserAccount getUserAccount(BigInteger userId, long cardId);
    UserAccount reserveFunds(BigInteger userId, long cardId);
}
