package com.spp.sgoods.service.util;

import java.util.Map;

public interface PdfService {
    boolean generatePdfThymeleaf(Map<String, Object> variables, String templatePath, String out);
}
