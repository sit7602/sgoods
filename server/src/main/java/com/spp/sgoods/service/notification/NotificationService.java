package com.spp.sgoods.service.notification;

import com.spp.sgoods.jsonEntity.notification.NotificationEntityAbstract;
import com.spp.sgoods.model.notification.Notification;
import com.spp.sgoods.model.user.User;

public interface NotificationService {
    Notification createNotification(User user, NotificationEntityAbstract notificationEntity);
}
