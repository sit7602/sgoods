package com.spp.sgoods.service.announcement;


import com.spp.sgoods.exeption.ProductCategoryException;
import com.spp.sgoods.model.announcement.ProductCategory;
import com.spp.sgoods.model.announcement.ProductCategoryForInfo;
import com.spp.sgoods.repository.ProductCategoryForInfoRepository;
import com.spp.sgoods.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    ProductCategoryForInfoRepository productCategoryForInfoRepository;

    @Autowired
    ProductCategoryRepository productCategoryRepository;

    @Override
    public List<ProductCategoryForInfo> getProductCategoryList() {
        return productCategoryForInfoRepository.findAllByParentIdIsNull();
    }

    @Override
    public ProductCategory getProductCategory(long categoryId) {
        ProductCategory productCategory = productCategoryRepository.findById(categoryId);
        if(productCategory != null)
            return productCategory;
        else
            throw new ProductCategoryException("Категория не найдена");
    }

    @Override
    public List<Long> getChildrenId(long categoryId) {
        ProductCategoryForInfo productCategory = productCategoryForInfoRepository.findById(categoryId);
        List<Long> childrenCategoryId = new ArrayList<>();
        getChildrenIdList(productCategory, childrenCategoryId);
        return childrenCategoryId;
    }

    private void getChildrenIdList(ProductCategoryForInfo productCategory, List<Long> childrenCategoryId){
        List<ProductCategoryForInfo> childrenProductCategories = productCategory.getChildrenCategory();
        if(childrenProductCategories != null)
            for(ProductCategoryForInfo childrenProductCategory: childrenProductCategories){
                childrenCategoryId.add(childrenProductCategory.getId());
                getChildrenIdList(childrenProductCategory, childrenCategoryId);
        }
    }
}
