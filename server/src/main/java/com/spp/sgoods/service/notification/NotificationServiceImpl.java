package com.spp.sgoods.service.notification;

import com.google.gson.Gson;
import com.spp.sgoods.jsonEntity.notification.NotificationEntityAbstract;
import com.spp.sgoods.model.notification.Notification;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.repository.NotificationRepository;
import com.spp.sgoods.service.EmailService;
import com.spp.sgoods.service.websocket.WebsocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService{

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private WebsocketService websocketService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private Gson gson;

    @Override
    public Notification createNotification(User user, NotificationEntityAbstract notificationEntity) {
        Notification notification = new Notification();
        notification.setUser(user);
        notification.setType(notificationEntity.getNotificationType());
        notification.setData(gson.toJson(notificationEntity));
        notificationRepository.save(notification);
        websocketService.sendNotification(notification);
        emailService.sendEmail(user.getEmail(), notification.getType().toString(), notification.getData());
        return notification;
    }
}
