package com.spp.sgoods.service.announcement;

import com.spp.sgoods.model.announcement.FieldDescription;

import java.util.List;

public interface FieldDescriptionService {
    List<FieldDescription> getFieldDescriptionList(long productCategoryId);
}
