package com.spp.sgoods.service.purchaseSystem;

import com.spp.sgoods.model.purchaseSystem.Purchase;

import java.math.BigInteger;

public interface PurchaseService {
    boolean autoCreatePurchaseOffer(long announcementId);
    boolean autoCreateBargainOffer(long announcementId, Integer price, Integer minPrice);

    boolean purchaseOffer(long announcementId, BigInteger buyerId, long cardId, int offerValidSeconds);
    boolean bargainOffer(long announcementId, BigInteger buyerId, long cardId, int offerValidSeconds,
                         Integer newPrice, String description);
    boolean acceptOffer(long announcementId, BigInteger sellerId, long offerId);

    boolean rejectOffer(long announcementId, BigInteger sellerId, long offerId);
    boolean rejectOffers(long announcementId, BigInteger sellerId);

    Purchase completionOfPurchase(long offerId, BigInteger userId);
}
