package com.spp.sgoods.service.googleApi;

import com.spp.sgoods.jsonEntity.googleApi.GoogleTokenResponse;
import com.spp.sgoods.jsonEntity.googleApi.GoogleUserInfo;

public interface GoogleOAuthService {
    GoogleTokenResponse GetUserToken(String code);
    GoogleUserInfo GetUserInfo(String accessToken);
}
