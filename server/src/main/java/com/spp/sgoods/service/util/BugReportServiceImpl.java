package com.spp.sgoods.service.util;

import com.spp.sgoods.model.BugReport;
import com.spp.sgoods.model.attachment.AttachmentAbstract;
import com.spp.sgoods.model.attachment.AttachmentType;
import com.spp.sgoods.model.attachment.PhotoAttachment;
import com.spp.sgoods.repository.BugReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigInteger;
import java.util.List;

@Service
public class BugReportServiceImpl implements BugReportService {

    @Value("${content.storage.path}")
    private String contentStoragePath;

    @Value("${content.path}")
    private String contentPath;

    @Autowired
    private BugReportRepository bugReportRepository;

    @Override
    public boolean createBugReport(BigInteger userId, BugReport bugReport) {
        bugReport.setUserId(userId);
        List<AttachmentAbstract> attachments = bugReport.getAttachments();
        if(attachments != null) {
            for(AttachmentAbstract attachment: attachments){
                if(!attachment.getType().equals(AttachmentType.PHOTO))
                    continue;
                PhotoAttachment photoAttachment = (PhotoAttachment) attachment;
                String filePath = "/" + userId + "/" + photoAttachment.getUrl();
                if(new File(contentStoragePath + filePath).exists())
                    photoAttachment.setUrl(contentPath + filePath);
                else
                    attachments.remove(attachment);
            }
        }
        bugReportRepository.save(bugReport);
        return true;
    }

    @Override
    public List<BugReport> getBugReports() {
        return bugReportRepository.findAll();
    }
}
