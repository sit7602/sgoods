package com.spp.sgoods.service.purchaseSystem;

import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;
import com.spp.sgoods.repository.OfferWithAnnouncementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {
    @Autowired
    private OfferWithAnnouncementRepository offerWithAnnouncementRepository;

    @Override
    public List<OfferWithAnnouncement> getOffersByBuyer(BigInteger buyerId) {
        return offerWithAnnouncementRepository.findAllByBuyerId(buyerId);
    }
}
