package com.spp.sgoods.service.user;

import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.userReview.UserReviewFromWithOffer;
import com.spp.sgoods.model.user.userReview.UserReviewTo;
import com.spp.sgoods.model.user.userReview.UserReviewToWithOffer;
import com.spp.sgoods.model.user.userReview.UserReviewType;

import java.math.BigInteger;
import java.util.List;

public interface UserReviewService {
    boolean autoCreateUserReview(User userFrom, User userTo, UserReviewType type, long offerId);
    boolean createUserReview(User userFrom, User userTo, int mark, UserReviewType type, long offerId);
    boolean createUserReview(User userFrom, User userTo, int mark, UserReviewType type, long offerId, String description);

    boolean updateUserReview(BigInteger userId, long offerId, int mark);
    boolean updateUserReview(BigInteger userId, long offerId, int mark, String description);

    List<UserReviewTo> getUserReviews(BigInteger userId);

    List<UserReviewToWithOffer> getUserReviewTo(BigInteger userId);
    List<UserReviewFromWithOffer> getUserReviewFrom(BigInteger userId);
}