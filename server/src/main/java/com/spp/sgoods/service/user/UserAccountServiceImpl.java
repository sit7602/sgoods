package com.spp.sgoods.service.user;

import com.spp.sgoods.exeption.ForbiddenException;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class UserAccountServiceImpl implements UserAccountService {
    @Autowired
    private UserAccountRepository userAccountRepository;

    @Override
    public void createUserAccount(BigInteger userId) {
        int cardCount = (int) (Math.round(Math.random()) * 2 + 1);
        for(int i = 0; i < cardCount; i++){
            UserAccount userAccount = new UserAccount(userId);
            userAccountRepository.save(userAccount);
        }
    }

    @Override
    public List<UserAccount> getUserAccounts(BigInteger userId) {
        return userAccountRepository.findAllByUserId(userId);
    }

    @Override
    public UserAccount getUserAccount(BigInteger userId, long cardId) {
        UserAccount userAccount = userAccountRepository.findById(cardId);
        if(userAccount == null || !userId.equals(userAccount.getUserId()))
            throw new ForbiddenException("У вас нет доступа к данной карте");
        return userAccount;
    }

    @Override
    public UserAccount reserveFunds(BigInteger userId, long cardId) {
        return null;
    }
}
