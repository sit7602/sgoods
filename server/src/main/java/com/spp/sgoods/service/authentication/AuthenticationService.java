package com.spp.sgoods.service.authentication;

import com.spp.sgoods.jsonEntity.authentication.TokenResponse;
import com.spp.sgoods.jsonEntity.authentication.TokenResponseLogin;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;

public interface AuthenticationService {
    TokenResponseLogin getTokenLogin(String code);
    TokenResponse getTokenRefresh(String accessToken, String refreshToken);
    BigInteger userAuthentication(String accessToken);
    BigInteger userAuthentication(HttpServletRequest request);
}
