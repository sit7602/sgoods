package com.spp.sgoods.service.announcement;

import com.spp.sgoods.jsonEntity.announcement.ParsedAnnouncementResponse;

import java.io.IOException;
import java.math.BigInteger;

public interface AnnouncementParserService {
    ParsedAnnouncementResponse parseAnnouncement(String url, BigInteger userId) throws IOException;
}
