package com.spp.sgoods.service.announcement;

import com.spp.sgoods.model.announcement.FieldDescription;
import com.spp.sgoods.repository.FieldDescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FieldDescriptionServiceImpl implements FieldDescriptionService {

    @Autowired
    private ProductCategoryService productCategoryService;

    @Autowired
    private FieldDescriptionRepository fieldDescriptionRepository;

    @Override
    public List<FieldDescription> getFieldDescriptionList(long productCategoryId) {
        List<Long> productCategoryIdList = productCategoryService
                .getProductCategory(productCategoryId)
                .getPCHierarchyAsList().getIds();
        if (productCategoryIdList == null) return null;

        List<FieldDescription> fieldDescriptionList = fieldDescriptionRepository
                .findAllByProductCategoryIdInOrderByProductCategoryIdAscOrderParameterAsc(productCategoryIdList);
        if (fieldDescriptionList == null) return null;

        short i = 0;
        for (FieldDescription fieldDescription : fieldDescriptionList) {
            i++;
            fieldDescription.setOrderParameter(i);
        }

        return fieldDescriptionList;
    }
}
