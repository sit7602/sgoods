package com.spp.sgoods.service.httpClient;

public interface HttpClientService {

    String get(String uri);
    String post(String uri, Object json);
    String delete(String uri);
}

