package com.spp.sgoods.service.purchaseSystem;

import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;

import java.math.BigInteger;
import java.util.List;

public interface OfferService {
    List<OfferWithAnnouncement> getOffersByBuyer(BigInteger buyerId);
}
