package com.spp.sgoods.service.user;

import com.spp.sgoods.jsonEntity.announcement.SellerInfo;
import com.spp.sgoods.jsonEntity.googleApi.GoogleUserInfo;
import com.spp.sgoods.jsonEntity.user.UserInfo;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;
import com.spp.sgoods.repository.UserDetailsRepository;
import com.spp.sgoods.repository.UserRepository;
import com.spp.sgoods.repository.UserSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Override
    public User getUser(BigInteger userId) {
        return userRepository.findById(userId);
    }

    @Override
    public User createUser(GoogleUserInfo googleUserInfo) {
        User user = new User(googleUserInfo.id, googleUserInfo.given_name,
                             googleUserInfo.family_name, googleUserInfo.picture,
                             googleUserInfo.email);
        user = userRepository.save(user);
        userAccountService.createUserAccount(user.getId());
        return user;
    }

    @Override
    public UserInfo getUserInfo(BigInteger userId) {
        return new UserInfo(getUser(userId), getUserDetails(userId),
                            getUserSettings(userId), userAccountService.getUserAccounts(userId));
    }

    @Override
    @Transactional
    public UserDetails setUserDetails(BigInteger userId, UserDetails userDetails) {
        User user = getUser(userId);
        userDetails.setUser(user);
        userDetailsRepository.save(userDetails);
        return userDetails;
    }

    @Override
    @Transactional
    public UserSettings setUserSettings(BigInteger userId, UserSettings userSettings) {
        User user = getUser(userId);
        userSettings.setUser(user);

        UserAccount userAccount = userAccountService.getUserAccount(userId, userSettings.getDefaultUserCardId());
        userSettings.setDefaultUserCard(userAccount.getId());

        userSettingsRepository.save(userSettings);
        return userSettings;
    }

    @Override
    @Transactional
    public UserInfo setUserDetailsAndSettings(BigInteger userId, UserDetails userDetails, UserSettings userSettings) {
        User user = userRepository.findById(userId);
        setUserDetails(userId, userDetails);
        setUserSettings(userId, userSettings);
        return new UserInfo(user, userDetails, userSettings, userAccountService.getUserAccounts(userId));
    }

    @Override
    public UserDetails getUserDetails(BigInteger userId) {
        return userDetailsRepository.getUserDetailsByUserId(userId);
    }

    @Override
    public UserSettings getUserSettings(BigInteger userId) {
        return userSettingsRepository.getUserSettingsByUserId(userId);
    }

    @Override
    public SellerInfo getSellerInfo(BigInteger userId) {
        return new SellerInfo(getUserDetails(userId), getUserSettings(userId));
    }
}