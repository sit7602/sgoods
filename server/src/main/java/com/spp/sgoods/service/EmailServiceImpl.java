package com.spp.sgoods.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    public JavaMailSender emailSender;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public boolean sendEmail(String mailTo, String title, String text) {
        if(mailTo.isEmpty())
            return false;
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(mailTo);
            message.setSubject(title);
            message.setText(text);
            message.setFrom(from);
            emailSender.send(message);
        }
        catch (Exception e) {
            return false;
        }

        return true;
    }
}
