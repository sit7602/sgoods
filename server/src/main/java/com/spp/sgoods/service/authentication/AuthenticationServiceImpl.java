package com.spp.sgoods.service.authentication;

import com.spp.sgoods.exeption.AuthenticationException;
import com.spp.sgoods.jsonEntity.authentication.TokenResponse;
import com.spp.sgoods.jsonEntity.authentication.TokenResponseLogin;
import com.spp.sgoods.jsonEntity.googleApi.GoogleUserInfo;
import com.spp.sgoods.jsonEntity.user.UserInfo;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;
import com.spp.sgoods.service.googleApi.GoogleOAuthService;
import com.spp.sgoods.service.user.UserAccountService;
import com.spp.sgoods.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    private GoogleOAuthService googleOAuthService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private TokenService tokenService;

    @Override
    public TokenResponseLogin getTokenLogin(String accessToken) {
        GoogleUserInfo googleUserInfo = googleOAuthService.GetUserInfo(accessToken);
        if(googleUserInfo == null)
            return null;
        User user = userService.getUser(googleUserInfo.id);
        UserDetails userDetails = null;
        UserSettings userSettings = null;
        if(user != null) {
            userDetails = userService.getUserDetails(user.getId());
            userSettings = userService.getUserSettings(user.getId());
        }
        else
            user = userService.createUser(googleUserInfo);

        List<UserAccount> userAccounts = userAccountService.getUserAccounts(user.getId());

        TokenResponseLogin tokenResponseLogin = new TokenResponseLogin();
        tokenResponseLogin.tokens = tokenService.createToken(user.getId());

        tokenResponseLogin.userInfo = new UserInfo(user, userDetails, userSettings, userAccounts);
        return tokenResponseLogin;
    }

    @Override
    public TokenResponse getTokenRefresh(String accessToken, String refreshToken) {
        return tokenService.refreshToken(accessToken, refreshToken);
    }

    @Override
    public BigInteger userAuthentication(String accessToken) {
        return tokenService.getUserId(accessToken);
    }

    @Override
    public BigInteger userAuthentication(HttpServletRequest request) {
        String accessToken = request.getHeader("token");
        if(accessToken == null)
            throw new AuthenticationException("Добавьте параметр token в header");
        BigInteger userId = userAuthentication(accessToken);
        if(userId == null) throw new AuthenticationException("Недействительный токен доступа! Попробуйте перезайти в аккаунт");
        return tokenService.getUserId(accessToken);
    }

}
