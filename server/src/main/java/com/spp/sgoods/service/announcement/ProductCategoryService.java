package com.spp.sgoods.service.announcement;

import com.spp.sgoods.model.announcement.ProductCategory;
import com.spp.sgoods.model.announcement.ProductCategoryForInfo;

import java.util.List;

public interface ProductCategoryService {
    List<ProductCategoryForInfo> getProductCategoryList();
    ProductCategory getProductCategory(long categoryId);
    List<Long> getChildrenId(long categoryId);
}
