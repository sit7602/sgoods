package com.spp.sgoods.service.announcement;

import com.spp.sgoods.exeption.AnnouncementException;
import com.spp.sgoods.jsonEntity.announcement.CreateAnnouncementRequest;
import com.spp.sgoods.model.announcement.*;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.repository.*;
import com.spp.sgoods.service.purchaseSystem.PurchaseService;
import com.spp.sgoods.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class AnnouncementServiceImpl implements AnnouncementService {

    @Value("${content.storage.path}")
    private String contentStoragePath;

    @Value("${content.path}")
    private String contentPath;

    @Autowired
    private UserService userService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private AnnouncementRepository announcementRepository;

    @Autowired
    private AnnouncementForSellerRepository announcementForSellerRepository;

    @Autowired
    private AnnouncementForSearchRepository announcementForSearchRepository;

    @Autowired
    private ProductCategoryService productCategoryService;

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private ElasticsearchRestTemplate elasticsearch;

    @Autowired
    private FavoriteAnnouncementRepository favoriteAnnouncementRepository;

    @Autowired
    private TaskScheduler taskScheduler;

    @Override
    public Announcement createAnnouncement(CreateAnnouncementRequest createAnnouncementRequest, BigInteger userId) {
        User seller = userService.getUser(userId);
        ProductCategory productCategory = productCategoryService.getProductCategory(createAnnouncementRequest.categoryId);

        Announcement announcement = new Announcement();
        announcement.setName(createAnnouncementRequest.name);
        announcement.setDescription(createAnnouncementRequest.description);
        announcement.setPrice(createAnnouncementRequest.price);
        announcement.setType(createAnnouncementRequest.type);
        announcement.setStatus(AnnouncementStatus.OPEN);
        announcement.setSeller(seller);
        announcement.setProductCategory(productCategory);
        announcement.setBargain(createAnnouncementRequest.bargain);
        announcement.setMinPrice(createAnnouncementRequest.minPrice);
        announcement.setGeoPoint(createAnnouncementRequest.geoPoint);

        List<Photo> photos = new ArrayList<>();
        for(String filename: createAnnouncementRequest.photoPaths){
            String photoPath = "/" + userId + "/" + filename;
            if(new File(contentStoragePath + photoPath).exists())
                photos.add(new Photo(contentPath + photoPath));
        }
        if (productCategory.getId() == 1000000)
            photos.add(new Photo("404"));
        if(photos.size() == 0)
            throw new AnnouncementException("Необходимо загрузить фото");
        announcement.setPhotos(photos);

        announcementRepository.save(announcement);

        announcementForSearchRepository.save(new AnnouncementForSearch(announcement));

        if (productCategory.getId() == 1000000 && !userId.equals(BigInteger.ZERO))
            if (!announcement.isBargain())
                taskScheduler.schedule(() -> purchaseService.autoCreatePurchaseOffer(announcement.getId()),
                        Instant.now().plusSeconds(10));
            else
                taskScheduler.schedule(() -> purchaseService.autoCreateBargainOffer(announcement.getId(),
                                                                                    announcement.getPrice(),
                                                                                    announcement.getMinPrice()),
                                       Instant.now().plusSeconds(10));

        return announcement;
    }

    @Override
    @Transactional
    public boolean closeAnnouncement(BigInteger userId, long announcementId) {
        Announcement announcement = announcementRepository.findById(announcementId);
        if (announcement.getStatus() != AnnouncementStatus.OPEN && announcement.getStatus() != AnnouncementStatus.REVIEW)
            throw new AnnouncementException("Невозможно закрыть объявление со статусом " + announcement.getStatus());
        if (!announcement.getSeller().getId().equals(userId))
            return false;
        announcement.setStatus(AnnouncementStatus.ARCHIVE);
        purchaseService.rejectOffers(announcementId, userId);
        announcementForSearchRepository.deleteById(announcementId);
        return true;
    }

    @Override
    public Announcement getAnnouncement(long announcementId) {
        Announcement announcement = announcementRepository.findById(announcementId);
        if(announcement != null) {
            announcement.setSellerInfo(userService.getSellerInfo(announcement.getSeller().getId()));
            return announcement;
        }
        else
            throw new AnnouncementException("Объявление не найдено");
    }

    @Override
    public Announcement getAnnouncement(BigInteger userId, long announcementId) {
        Announcement announcement = getAnnouncement(announcementId);
        announcement.setFavorite(favoriteAnnouncementRepository.existsByUserIdAndAnnouncementId(userId, announcementId));
        return announcement;
    }

    @Override
    public List<AnnouncementForSearch> getAnnouncements(Query query) {
        SearchHits<AnnouncementForSearch> searchHits = elasticsearch.search(query,
                AnnouncementForSearch.class, IndexCoordinates.of("announcement"));
        List<AnnouncementForSearch> announcements= searchHits.stream().map(SearchHit::getContent)
                .collect(Collectors.toList());
        announcements.forEach(announcement -> announcement
                        .setPhotos(photoRepository.findAllByAnnouncementId(announcement.getId())));
        announcements.forEach(announcement -> announcement.setProductCategory(new ProductCategory(announcement
                .getProductCategoryIdsTree(), announcement.getProductCategoryNamesTree())));
        return announcements;
    }

    @Override
    public List<AnnouncementForSearch> getAnnouncements(BigInteger userId, Query query){
        List<AnnouncementForSearch> announcements = getAnnouncements(query);
        announcements.forEach(announcement -> announcement
                .setFavorite(favoriteAnnouncementRepository.existsByUserIdAndAnnouncementId(userId, announcement.getId())));
        return announcements;
    }

    @Override
    public List<AnnouncementForSeller> getAnnouncementsBySeller(BigInteger userId) {
        User user = userService.getUser(userId);
        return announcementForSellerRepository.findAllBySeller(user);
    }

    @Override
    public List<Announcement> getAnnouncementsByBuyer(BigInteger userId) {
        User user = userService.getUser(userId);
        return announcementRepository.findAllByBuyerAndStatus(user, AnnouncementStatus.CLOSE);
    }

    @Override
    public boolean setFavoriteAnnouncement(BigInteger userId, long announcementId) {
        if(!announcementRepository.existsById(announcementId)
                || favoriteAnnouncementRepository.existsByUserIdAndAnnouncementId(userId, announcementId))
            return false;
        favoriteAnnouncementRepository.save(new FavoriteAnnouncement(userId, announcementId));
        return true;
    }

    @Override
    public List<Announcement> getFavoriteAnnouncements(BigInteger userId) {
        List<Long> ids = favoriteAnnouncementRepository
                .findAllByUserId(userId)
                .stream()
                .map(FavoriteAnnouncement::getAnnouncementId)
                .collect(Collectors.toList());
        List<Announcement> announcements = announcementRepository.findAllByIdIn(ids);
        announcements.forEach(announcement -> announcement.setFavorite(true));
        return announcements;
    }

    @Override
    @Transactional
    public boolean deleteFavoriteAnnouncement(BigInteger userId, long announcementId) {
        favoriteAnnouncementRepository.deleteByUserIdAndAnnouncementId(userId, announcementId);
        return true;
    }

    @Scheduled(fixedDelay = 10000)
    protected void autoCreateAnnouncement() {
        if (announcementRepository.countAllByProductCategory_IdAndStatus(1000000, AnnouncementStatus.OPEN) >= 3)
            return;
        CreateAnnouncementRequest createAnnouncementRequest = new CreateAnnouncementRequest();
        Random random = new Random();
        createAnnouncementRequest.categoryId = 1000000;
        createAnnouncementRequest.name = "Автоматический созданное объявление";
        createAnnouncementRequest.description = "Автоматический созданное объявление";
        createAnnouncementRequest.price = random.nextInt(10000);
        createAnnouncementRequest.photoPaths = new ArrayList<>();
        createAnnouncementRequest.geoPoint = new GeoPoint(random.nextInt(170) - 85,
                                                         random.nextInt(300) - 150);
        createAnnouncementRequest.type = AnnouncementType.PRIVATE;

        createAnnouncementRequest.bargain = random.nextBoolean();
        if (createAnnouncementRequest.bargain)
            createAnnouncementRequest.minPrice = createAnnouncementRequest.price - random.nextInt(createAnnouncementRequest.price);
        createAnnouncement(createAnnouncementRequest, BigInteger.ZERO);
    }
}