package com.spp.sgoods.service.authentication;

import com.spp.sgoods.exeption.AuthenticationException;
import com.spp.sgoods.jsonEntity.authentication.TokenResponse;
import com.spp.sgoods.model.TokenStorage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TokenServiceImpl implements TokenService {
    @Value("${token.tokenValidity}")
    int tokenValidity;

    Map<String, TokenStorage> tokenStorageMap = new ConcurrentHashMap<>();

    @Override
    public TokenResponse createToken(BigInteger userId) {
        String accessToken = UUID.randomUUID().toString();
        String refreshToken = UUID.randomUUID().toString();

        LocalDateTime decayTime = LocalDateTime.now().plusSeconds(tokenValidity);
        tokenStorageMap.put(accessToken, new TokenStorage(userId, refreshToken, decayTime));
        return new TokenResponse(accessToken, refreshToken, decayTime);
    }

    @Override
    public TokenResponse refreshToken(String accessToken, String refreshToken) {
        TokenStorage oldTokenStorage = tokenStorageMap.getOrDefault(accessToken, null);
        if (oldTokenStorage == null) throw new AuthenticationException("Недействительный токен доступа! Попробуйте перезайти в аккаунт");
        else if (!oldTokenStorage.refreshToken.equals(refreshToken))
            throw new AuthenticationException("Недействительный refresh токен");
        tokenStorageMap.remove(accessToken);
        return createToken(oldTokenStorage.userId);
    }

    @Override
    public BigInteger getUserId(String accessToken) {
        TokenStorage tokenStorage = tokenStorageMap.getOrDefault(accessToken, null);
        if(tokenStorage == null) throw new AuthenticationException("Недействительный токен доступа! Попробуйте перезайти в аккаунт");
        if(LocalDateTime.now().isAfter(tokenStorage.decayTime))
            throw new AuthenticationException("Истек срок действия токена доступа");
        return tokenStorage.userId;
    }

    @Override
    public Map<String, TokenStorage> getTokenStorageMap() {
        return tokenStorageMap;
    }
}