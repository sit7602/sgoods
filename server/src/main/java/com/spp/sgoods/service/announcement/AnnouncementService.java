package com.spp.sgoods.service.announcement;

import com.spp.sgoods.jsonEntity.announcement.CreateAnnouncementRequest;
import com.spp.sgoods.model.announcement.Announcement;
import com.spp.sgoods.model.announcement.AnnouncementForSearch;
import com.spp.sgoods.model.announcement.AnnouncementForSeller;
import org.springframework.data.elasticsearch.core.query.Query;

import java.math.BigInteger;
import java.util.List;

public interface AnnouncementService {
    Announcement createAnnouncement(CreateAnnouncementRequest createAnnouncementRequest, BigInteger userId);

    boolean closeAnnouncement(BigInteger userId, long announcementId);

    Announcement getAnnouncement(long announcementId);
    Announcement getAnnouncement(BigInteger userId, long announcementId);

    List<AnnouncementForSearch> getAnnouncements(Query query);
    List<AnnouncementForSearch> getAnnouncements(BigInteger userID, Query query);

    List<Announcement> getAnnouncementsByBuyer(BigInteger userId);
    List<AnnouncementForSeller> getAnnouncementsBySeller(BigInteger userId);

    boolean setFavoriteAnnouncement(BigInteger userId, long announcementId);
    List<Announcement> getFavoriteAnnouncements(BigInteger userId);

    boolean deleteFavoriteAnnouncement(BigInteger userId, long announcementId);
}
