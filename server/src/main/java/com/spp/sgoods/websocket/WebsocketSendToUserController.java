package com.spp.sgoods.websocket;

import com.google.gson.Gson;
import com.spp.sgoods.model.TokenStorage;
import com.spp.sgoods.service.authentication.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Map;

@Controller
public class WebsocketSendToUserController {
    @Autowired
    private SimpMessageSendingOperations simpMessagingTemplate;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private Gson gson;

    @Autowired
    private SimpUserRegistry simpUserRegistry;

    @MessageMapping("/message")
    public void processMessageFromClient(@Payload String message) {
        Principal principal = new SocketPrincipal(BigInteger.ONE);
        simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/reply", "123");
    }

//    @Scheduled(fixedDelay = 10000, initialDelay = 1000)
//    public void test(){
//        simpUserRegistry.getUsers();
//        for(TokenStorage tokenStorage : tokenService.getTokenStorageMap().values()) {
//            simpMessagingTemplate.convertAndSendToUser(tokenStorage.userId.toString(), "/queue/reply",
//                    tokenStorage);
//        }
//    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
