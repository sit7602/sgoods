package com.spp.sgoods.websocket;

import java.math.BigInteger;
import java.security.Principal;

public class SocketPrincipal implements Principal {
    private BigInteger id;

    public SocketPrincipal(BigInteger id){
        this.id = id;
    }

    @Override
    public String getName() {
        return id.toString();
    }
}
