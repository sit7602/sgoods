package com.spp.sgoods.gson.serializer;

import com.google.gson.*;
import com.spp.sgoods.model.notification.Notification;

import java.lang.reflect.Type;
import java.util.List;

public class NotificationSerializer implements JsonSerializer<Notification> {

    @Override
    public JsonElement serialize(Notification source, Type typeOfSource, JsonSerializationContext context)
    {
        JsonObject jsonObject = context.serialize(source).getAsJsonObject();
        jsonObject.add("data", context.serialize(source.getData()).getAsJsonObject());
        return jsonObject;
    }
}
