package com.spp.sgoods.gson.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateDeserializer implements JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonElement jsonElement, Type typeOF,
                                     JsonDeserializationContext context) throws JsonParseException {
        Instant instant = Instant.ofEpochSecond(jsonElement.getAsLong());
        return LocalDateTime.ofInstant(instant, ZoneOffset.ofHours(0)).toLocalDate();
    }
}