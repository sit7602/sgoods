package com.spp.sgoods.gson.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeSerializer implements JsonSerializer<LocalDateTime> {

    @Override
    public JsonElement serialize(LocalDateTime source, Type typeOfSource, JsonSerializationContext context)
    {
        return new JsonPrimitive(source.toInstant(ZoneOffset.ofHours(0)).getEpochSecond());
    }
}
