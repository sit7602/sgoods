package com.spp.sgoods.gson;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.lang.NonNull;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.messaging.converter.DefaultContentTypeResolver;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import java.nio.charset.StandardCharsets;

@Component
public class GsonMessageConverter extends AbstractMessageConverter {
    @Autowired
    private Gson gson;

    public GsonMessageConverter() {
        super(new MimeType("application", "json"));
        DefaultContentTypeResolver resolver = new DefaultContentTypeResolver();
        resolver.setDefaultMimeType(MimeTypeUtils.APPLICATION_JSON);
        this.setContentTypeResolver(resolver);
    }

    @Override
    protected boolean supports(@NonNull Class<?> aClass) {
        return false;
    }

    @Override
    protected Object convertToInternal(Object payload, MessageHeaders headers, Object conversionHint) {
        return gson.toJson(payload).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    protected boolean canConvertTo(Object payload, MessageHeaders headers) {
        return this.supportsMimeType(headers);
    }
}
