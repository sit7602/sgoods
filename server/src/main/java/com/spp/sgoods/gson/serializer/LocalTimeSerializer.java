package com.spp.sgoods.gson.serializer;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

public class LocalTimeSerializer implements JsonSerializer<LocalTime> {

    @Override
    public JsonElement serialize(LocalTime source, Type typeOfSource, JsonSerializationContext context)
    {
        return new JsonPrimitive(source.toSecondOfDay());
    }
}