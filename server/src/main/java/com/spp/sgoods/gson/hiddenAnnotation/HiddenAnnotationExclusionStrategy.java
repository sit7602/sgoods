package com.spp.sgoods.gson.hiddenAnnotation;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class HiddenAnnotationExclusionStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }

    @Override
    public boolean shouldSkipField(FieldAttributes field) {
        return field.getAnnotation(Hidden.class) != null;
    }
}
