package com.spp.sgoods.gson.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spp.sgoods.model.attachment.*;

import java.lang.reflect.Type;

public class AbstractAttachmentDeserializer implements JsonDeserializer<AttachmentAbstract> {
    @Override
    public AttachmentAbstract deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        AttachmentType attachmentType = context.deserialize(jsonElement.getAsJsonObject().getAsJsonPrimitive("type"), AttachmentType.class);
        switch (attachmentType) {
            case PHOTO:
                return context.deserialize(jsonElement, PhotoAttachment.class);
            case VIDEO:
                return context.deserialize(jsonElement, VideoAttachment.class);
            case GEO_POINT:
                return context.deserialize(jsonElement, GeoPointAttachment.class);
        }
        return null;
    }
}
