package com.spp.sgoods.gson.serializer;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

public class LocalDateSerializer implements JsonSerializer<LocalDate> {

    @Override
    public JsonElement serialize(LocalDate source, Type typeOfSource, JsonSerializationContext context)
    {
        return new JsonPrimitive(source.toEpochDay() * 86400);
    }
}
