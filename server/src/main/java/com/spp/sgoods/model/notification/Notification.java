package com.spp.sgoods.model.notification;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import com.spp.sgoods.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Notification {
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", nullable = false)
    @Hidden
    private User user;

    @Column(nullable = false)
    private LocalDateTime dateTime = LocalDateTime.now();

    @Enumerated(value = EnumType.ORDINAL)
    private NotificationType type;

    @Column(length = 4000, nullable = false)
    private String data;

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
