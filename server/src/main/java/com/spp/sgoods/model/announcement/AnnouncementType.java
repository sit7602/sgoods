package com.spp.sgoods.model.announcement;

import com.google.gson.annotations.SerializedName;

public enum AnnouncementType {
    @SerializedName("0")
    PRIVATE,

    @SerializedName("1")
    REUSABLE
}
