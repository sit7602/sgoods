package com.spp.sgoods.model.announcement;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;

import javax.persistence.*;

@Entity
@Table(name = "FIELD_DESCRIPTION")
public class FieldDescription {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private long productCategoryId;

    @Column(length = 50, nullable = false)
    private String parameter;

    @Column(nullable = false)
    @Hidden
    private short orderParameter;

    @Column(nullable = false)
    private short valueType;

    @Column(length = 100)
    private String prompt;

    public void setOrderParameter(short orderParameter) {
        this.orderParameter = orderParameter;
    }

}
