package com.spp.sgoods.model.purchaseSystem;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import com.spp.sgoods.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "PURCHASE")
public class Purchase {
    @Id
    @GeneratedValue
    @Hidden
    long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Offer offer;

    private boolean sellerAccept = false;

    private boolean buyerAccept = false;

    private LocalDateTime startTime = LocalDateTime.now();

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public boolean isSellerAccept() {
        return sellerAccept;
    }

    public void sellerAccept() {
        this.sellerAccept = true;
    }

    public boolean isBuyerAccept() {
        return buyerAccept;
    }

    public void buyerAccept() {
        this.buyerAccept = true;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }
}
