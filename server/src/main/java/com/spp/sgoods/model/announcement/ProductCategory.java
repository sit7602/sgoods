package com.spp.sgoods.model.announcement;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

@Entity
@Table(name = "PRODUCT_CATEGORY")
public class ProductCategory extends ProductCategoryAbstract {

    @ManyToOne
    @JoinColumn(name = "parent_category", insertable=false, updatable=false)
    private ProductCategory parentCategory;

    @OneToMany(cascade = CascadeType.MERGE)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "category_id")
    private List<Checklist> checklists;

    public List<Checklist> getChecklists() {
        return checklists;
    }

    public ProductCategory(){
    }

    public ProductCategory(String ids, String names){
        ProductCategoryHierarchyStringStack pcHierarchyStringStack =
                new ProductCategoryHierarchyStringStack(ids, names);
        setProductCategory(pcHierarchyStringStack);
    }

    private ProductCategory(ProductCategoryHierarchyStringStack pcHierarchyStringStack){
        setProductCategory(pcHierarchyStringStack);
    }

    private void setProductCategory(ProductCategoryHierarchyStringStack pcHierarchyStringStack){
        this.id = Long.parseLong(pcHierarchyStringStack.getId());
        this.category = pcHierarchyStringStack.getName();
        if(!pcHierarchyStringStack.isIdEmpty())
            parentCategory = new ProductCategory(pcHierarchyStringStack);
    }

    public ProductCategoryHierarchyString getPCHierarchyAsString(){
        ProductCategoryHierarchyList PCHierarchyList = getPCHierarchyList();

        String idsString = PCHierarchyList.getIds().stream()
                .map(String::valueOf)
                .collect(Collectors.joining("/"));

        String namesString = PCHierarchyList.getNames().stream()
                .map(String::valueOf)
                .collect(Collectors.joining("/"));

        return new ProductCategoryHierarchyString(idsString, namesString);
    }

    public ProductCategoryHierarchyList getPCHierarchyAsList(){
        return getPCHierarchyList();
    }

    private ProductCategoryHierarchyList getPCHierarchyList(){
        if(parentCategory == null) {
            List<Long> ids = new ArrayList<>();
            List<String> names = new ArrayList<>();
            ids.add(this.getId());
            names.add(this.getCategory());
            return new ProductCategoryHierarchyList(ids, names);
        }

        ProductCategoryHierarchyList result = this.parentCategory.getPCHierarchyList();
        result.getIds().add(getId());
        result.getNames().add(getCategory());
        return result;
    }

    public static class ProductCategoryHierarchyString {
        private String ids;
        private String names;

        public ProductCategoryHierarchyString(String ids, String names) {
            this.ids = ids;
            this.names = names;
        }

        public String getIds() {
            return ids;
        }

        public String getNames() {
            return names;
        }
    }

    private static class ProductCategoryHierarchyStringStack {
        private Stack<String> ids = new Stack<>();
        private Stack<String> names = new Stack<>();

        public ProductCategoryHierarchyStringStack(String ids, String names) {
            this.ids.addAll(Arrays.asList(ids.split("/")));
            this.names.addAll(Arrays.asList(names.split("/")));
        }

        public String getId() {
            return ids.pop();
        }

        public String getName() {
            return names.pop();
        }

        public boolean isIdEmpty() {
            return ids.empty();
        }

        public boolean idNameEmpty() {
            return names.empty();
        }
    }

    public static class ProductCategoryHierarchyList {
        private List<Long> ids;
        private List<String> names;

        public ProductCategoryHierarchyList(List<Long> ids, List<String> names) {
            this.ids = ids;
            this.names = names;
        }

        public List<Long> getIds() {
            return ids;
        }

        public List<String> getNames() {
            return names;
        }
    }
}