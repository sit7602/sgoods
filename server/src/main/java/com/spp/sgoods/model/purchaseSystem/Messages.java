package com.spp.sgoods.model.purchaseSystem;

import com.spp.sgoods.model.attachment.AttachmentAbstract;
import com.spp.sgoods.model.user.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MESSAGES")
public class Messages {

    @Id
    @GeneratedValue
    private int id;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_id", nullable = false)
    private User recipientId;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id", nullable = false)
    private User senderId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "message_id")
    private List<AttachmentAbstract> messageAttachment;

    @Column(length = 500, nullable = false)
    private String text;

}
