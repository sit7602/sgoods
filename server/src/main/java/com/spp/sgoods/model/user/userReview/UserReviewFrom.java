package com.spp.sgoods.model.user.userReview;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "USER_REVIEW")
public class UserReviewFrom extends UserReviewFromAbstract {

    @Column(name = "offer_id", nullable = false)
    private long offerId;
}
