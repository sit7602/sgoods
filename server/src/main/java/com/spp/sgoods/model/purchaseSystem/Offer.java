package com.spp.sgoods.model.purchaseSystem;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "OFFER")
public class Offer extends OfferAbstract{

    @Column(name = "announcement_id", insertable = false, updatable = false)
    @Hidden
    private long announcementId;


    public Offer(User buyer, UserAccount buyerAccount, int offerValidSeconds) {
        super(buyer, buyerAccount, offerValidSeconds);
    }

    public Offer(User buyer, UserAccount buyerAccount, int offerValidSeconds, int newPrice, String description) {
        super(buyer, buyerAccount, offerValidSeconds, newPrice, description);
    }

    public Offer() {
    }

    public long getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(long announcementId) {
        this.announcementId = announcementId;
    }
}
