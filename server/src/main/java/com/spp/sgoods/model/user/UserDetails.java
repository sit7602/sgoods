package com.spp.sgoods.model.user;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@Table(name = "USER_DETAILS")
public class UserDetails {

    @Id
    @Column(columnDefinition = "DOUBLE(25,0)")
    private BigInteger userId;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private transient User user;

    @Column(length = 10, nullable = false, unique = true)
    private String phone;

    @Column(nullable = false)
    private LocalDateTime registrationDate = LocalDateTime.now();

    private transient long userAddressId;

    public UserDetails() {
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public long getUserAddressId() {
        return userAddressId;
    }

    public User getUser() {
        return user;
    }

    public String getPhone() {
        return phone;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }
}
