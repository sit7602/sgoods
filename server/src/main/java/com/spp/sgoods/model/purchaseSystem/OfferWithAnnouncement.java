package com.spp.sgoods.model.purchaseSystem;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import com.spp.sgoods.model.announcement.Announcement;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "OFFER")
public class OfferWithAnnouncement extends OfferAbstract {
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "announcement_id")
    private Announcement announcement;

    @Column(name = "buyer_id", columnDefinition = "DECIMAL(25,0)", insertable = false, updatable = false)
    @Hidden
    private BigInteger buyerId;

    public Announcement getAnnouncement() {
        return announcement;
    }
}
