package com.spp.sgoods.model.attachment;

import com.google.gson.annotations.SerializedName;

public enum AttachmentType {
    @SerializedName("0")
    PHOTO,

    @SerializedName("1")
    VIDEO,

    @SerializedName("2")
    GEO_POINT
}
