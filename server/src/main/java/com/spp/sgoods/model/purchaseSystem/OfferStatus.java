package com.spp.sgoods.model.purchaseSystem;

import com.google.gson.annotations.SerializedName;

public enum OfferStatus {
    @SerializedName("0")
    OFFER,

    @SerializedName("1")
    BARGAIN,

    @SerializedName("2")
    OFFER_ACCEPTED,

    @SerializedName("3")
    STANDBY_TIME_IS_OUT,

    @SerializedName("4")
    SOLD,

    @SerializedName("5")
    OFFER_REJECTED
}
