package com.spp.sgoods.model.announcement;

import javax.persistence.*;

@MappedSuperclass
@Table(name = "PRODUCT_CATEGORY", uniqueConstraints = @UniqueConstraint(columnNames = {"parent_category", "category"}))
public abstract class ProductCategoryAbstract {

    @Id
    @GeneratedValue
    protected long id;

    @Column(name = "category", length = 50)
    protected String category;

    public long getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }
}
