package com.spp.sgoods.model.user.userReview;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import com.spp.sgoods.model.user.User;

import javax.persistence.*;
import java.math.BigInteger;

@MappedSuperclass
@Table(name = "USER_REVIEW")
public abstract class UserReviewFromAbstract extends UserReviewAbstract {

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_to", nullable = false)
    protected User userTo;

    @Column(name = "user_from", insertable = false, updatable = false, columnDefinition = "DECIMAL(25,0)")
    @Hidden
    private BigInteger userFrom;
}
