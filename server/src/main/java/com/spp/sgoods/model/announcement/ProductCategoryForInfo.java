package com.spp.sgoods.model.announcement;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "PRODUCT_CATEGORY", uniqueConstraints = @UniqueConstraint(columnNames = {"parent_category", "category"}))
public class ProductCategoryForInfo extends ProductCategoryAbstract {

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "parent_category")
    private List<ProductCategoryForInfo> childrenCategory = new LinkedList<>();

    @Column(name = "parent_category")
    @Hidden
    private Long parentId;

    public List<ProductCategoryForInfo> getChildrenCategory() {
        return childrenCategory;
    }
}
