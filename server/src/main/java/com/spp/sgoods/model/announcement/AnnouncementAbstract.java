package com.spp.sgoods.model.announcement;

import com.spp.sgoods.jsonEntity.announcement.SellerInfo;
import com.spp.sgoods.model.user.User;

import javax.persistence.*;

@MappedSuperclass
@Table(name = "ANNOUNCEMENT")
public abstract class AnnouncementAbstract extends AnnouncementEntityAbstract {

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "seller_id", nullable = false)
    private User seller;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "buyer_id")
    private User buyer = null;

    @Transient
    private SellerInfo sellerInfo;

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public SellerInfo getSellerInfo() {
        return sellerInfo;
    }

    public void setSellerInfo(SellerInfo sellerInfo) {
        this.sellerInfo = sellerInfo;
    }
}
