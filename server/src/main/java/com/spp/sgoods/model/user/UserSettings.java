package com.spp.sgoods.model.user;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalTime;

@Entity
@Table(name = "USER_SETTINGS")
public class UserSettings {

    @Id
    @Column(columnDefinition = "DOUBLE(25,0)")
    private BigInteger userId;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private transient User user;

    @Column(nullable = false)
    private boolean hideUserDate = false;

    @Column()
    private LocalTime callTimeStart;

    @Column()
    private LocalTime callTimeEnd;

    @Column()
    private short UTC;

    @Column()
    private long defaultUserCardId;

    public UserSettings() {
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public long getDefaultUserCardId() {
        return defaultUserCardId;
    }

    public void setDefaultUserCard(long defaultUserCardId) {
        this.defaultUserCardId = defaultUserCardId;
    }

    public User getUser() {
        return user;
    }

    public boolean isHideUserDate() {
        return hideUserDate;
    }

    public LocalTime getCallTimeStart() {
        return callTimeStart;
    }

    public LocalTime getCallTimeEnd() {
        return callTimeEnd;
    }

    public short getUTC() {
        return UTC;
    }
}
