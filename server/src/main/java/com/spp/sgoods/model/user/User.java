package com.spp.sgoods.model.user;

import org.hibernate.annotations.Formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "USER")
public class User {

    @Id
    @Column(columnDefinition = "DECIMAL(25,0)")
    private BigInteger id;

    @Column(length = 25, nullable = false)
    private String firstName;

    @Column(length = 25)
    private String lastName;

    @Column(length = 250, nullable = false)
    private String photo;

    @Column(length = 50, nullable = false)
    private String email;

    @Formula(value = "(SELECT IFNULL(AVG(r.mark), 0)  FROM user_review r WHERE r.user_to = id)")
    private double rating;

    public User(BigInteger id, String firstName, String fullName, String photo, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = fullName;
        this.photo = photo;
        this.email = email;
    }

    public User() {
    }

    public BigInteger getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        if (lastName != null)
            return firstName + " " + lastName;
        else
            return firstName;
    }
}
