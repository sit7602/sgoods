package com.spp.sgoods.model.announcement;

import com.spp.sgoods.model.purchaseSystem.Offer;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ANNOUNCEMENT")
public class AnnouncementForSeller extends AnnouncementAbstract {
    @LazyCollection(value = LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "announcement_id", nullable = false)
    private List<Offer> offers = new ArrayList<>();

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(Offer offer) {
        offers.add(offer);
    }
}