package com.spp.sgoods.model.user.userReview;

import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;

import javax.persistence.*;

@Entity
@Table(name = "USER_REVIEW")
public class UserReviewFromWithOffer extends UserReviewFromAbstract {

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "offer_id", nullable = false)
    private OfferWithAnnouncement offer;
}
