package com.spp.sgoods.model.announcement;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.time.LocalDateTime;
import java.util.List;

@Document(indexName = "announcement")
public class AnnouncementForSearch {

    @Id
    @Field(type = FieldType.Integer)
    private long id;

    @Field(type = FieldType.Text)
    private String name;

    @Field(type = FieldType.Text)
    private String description;

    @Field(type = FieldType.Integer)
    private int price;

    @Field(type = FieldType.Date, format = DateFormat.basic_date_time)
    private LocalDateTime createDate = LocalDateTime.now();

    @Field(type = FieldType.Keyword)
    private AnnouncementType type = AnnouncementType.PRIVATE;

    @Field(type = FieldType.Text)
    @Hidden
    private String productCategoryIdsTree;

    @Field(type = FieldType.Text)
    @Hidden
    private String productCategoryNamesTree;

    @Field()
    private GeoPoint geoPoint;

    @Transient
    private List<Photo> photos;

    @Transient
    private ProductCategory productCategory;

    @Transient
    private boolean isFavorite;

    public AnnouncementForSearch(AnnouncementEntityAbstract announcement) {
        this.id = announcement.getId();
        this.name = announcement.getName();
        this.description = announcement.getDescription();
        this.price = announcement.getPrice();
        this.createDate = announcement.getCreateDate();
        this.type = announcement.getType();
        ProductCategory.ProductCategoryHierarchyString pcHierarchy = announcement.getProductCategory().getPCHierarchyAsString();
        this.productCategoryIdsTree = pcHierarchy.getIds();
        this.productCategoryNamesTree = pcHierarchy.getNames();
        this.geoPoint = announcement.getGeoPoint();
    }

    public AnnouncementForSearch() {
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public long getId() {
        return id;
    }

    public String getProductCategoryIdsTree() {
        return productCategoryIdsTree;
    }

    public String getProductCategoryNamesTree() {
        return productCategoryNamesTree;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
