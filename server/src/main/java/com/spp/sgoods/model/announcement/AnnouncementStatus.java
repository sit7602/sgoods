package com.spp.sgoods.model.announcement;

import com.google.gson.annotations.SerializedName;

public enum AnnouncementStatus {
    @SerializedName("0")
    REVIEW,

    @SerializedName("1")
    OPEN,

    @SerializedName("2")
    CLOSE,

    @SerializedName("3")
    ARCHIVE,

    @SerializedName("4")
    RESERVED
}
