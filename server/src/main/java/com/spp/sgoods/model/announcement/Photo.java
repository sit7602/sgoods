package com.spp.sgoods.model.announcement;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;

import javax.persistence.*;

@Entity
@Table(name = "PHOTO")
public class Photo {

    @Id
    @GeneratedValue
    private int id;

    @Column(length = 250, nullable = false)
    private String fileURL;

    @Column(name = "announcement_id", insertable = false, updatable = false)
    @Hidden
    private long announcementId;

    public Photo() {
    }

    public Photo(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getFileURL() {
        return fileURL;
    }
}
