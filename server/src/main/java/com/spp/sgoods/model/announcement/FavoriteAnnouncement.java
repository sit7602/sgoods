package com.spp.sgoods.model.announcement;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "FAVORITE_ANNOUNCEMENT",
       uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "announcement_id"}))
public class FavoriteAnnouncement {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "user_id", nullable = false)
    private BigInteger userId;

    @Column(name = "announcement_id", nullable = false)
    private long announcementId;

    public FavoriteAnnouncement(BigInteger userId, long announcementId) {
        this.userId = userId;
        this.announcementId = announcementId;
    }

    public FavoriteAnnouncement() {
    }

    public long getAnnouncementId() {
        return announcementId;
    }
}