package com.spp.sgoods.model.user.userReview;

import com.google.gson.annotations.SerializedName;

public enum UserReviewType {

    @SerializedName("0")
    AFTER_THE_PURCHASE_SELLER,

    @SerializedName("1")
    AFTER_THE_PURCHASE_BUYER
}
