package com.spp.sgoods.model.notification;

import com.google.gson.annotations.SerializedName;

public enum NotificationType {
    @SerializedName("0")
    PURCHASE_OFFER,

    @SerializedName("1")
    OFFER_NEW_PRICE,

    @SerializedName("2")
    CONFIRMATION_OF_SALES,

    @SerializedName("3")
    TRANSFER_CONFIRMATION,

    @SerializedName("4")
    SOLD,

    @SerializedName("5")
    REJECT_OFFER,

    @SerializedName("6")
    TIME_OUT_WAITING
}
