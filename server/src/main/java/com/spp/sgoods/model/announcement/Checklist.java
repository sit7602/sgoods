package com.spp.sgoods.model.announcement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Checklist {
    @Id
    private long id;

    @Column(nullable = false)
    private short orderQuestion;

    @Column(nullable = false, length = 100)
    private String question;

    private String description;

    @Column(nullable = false)
    private String URL;
}
