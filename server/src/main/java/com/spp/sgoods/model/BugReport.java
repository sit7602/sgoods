package com.spp.sgoods.model;

import com.spp.sgoods.model.attachment.AttachmentAbstract;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
public class BugReport {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false, columnDefinition = "DECIMAL(25,0)")
    private BigInteger userId;

    @Column(nullable = false)
    private String title;

    @Column(length = 4000)
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "bug_report_id")
    private List<AttachmentAbstract> attachments;

    private String currentPage;

    public List<AttachmentAbstract> getAttachments() {
        return attachments;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }
}
