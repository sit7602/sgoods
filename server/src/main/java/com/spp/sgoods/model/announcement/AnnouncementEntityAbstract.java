package com.spp.sgoods.model.announcement;

import com.spp.sgoods.gson.hiddenAnnotation.Hidden;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@MappedSuperclass
@Table(name = "ANNOUNCEMENT")
public abstract class AnnouncementEntityAbstract {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 4000, nullable = false)
    private String description;

    @Column(nullable = false)
    private int price;

    @Column(nullable = false)
    private boolean bargain;

    @Column()
    @Hidden
    private Integer minPrice;

    @Column(nullable = false)
    private final LocalDateTime createDate = LocalDateTime.now();

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private AnnouncementStatus status = AnnouncementStatus.REVIEW;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private AnnouncementType type;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "announcement_id")
    private List<Photo> photos;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_category", nullable = false)
    private ProductCategory productCategory;

    @Embedded
    private GeoPoint geoPoint;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public AnnouncementStatus getStatus() {
        return status;
    }

    public void setStatus(AnnouncementStatus status) {
        this.status = status;
    }

    public AnnouncementType getType() {
        return type;
    }

    public void setType(AnnouncementType type) {
        this.type = type;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public boolean isBargain() {
        return bargain;
    }

    public void setBargain(boolean bargain) {
        this.bargain = bargain;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }
}