package com.spp.sgoods.model.purchaseSystem;

import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class OfferAbstract {
    @Id
    @GeneratedValue
    private long id;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "buyer_id", nullable = false)
    private User buyer;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "buyer_account_id", nullable = false)
    private UserAccount buyerAccount;

    private LocalDateTime offerValidUntil;

    @Enumerated(value = EnumType.ORDINAL)
    private OfferStatus offerStatus;

    private Integer newPrice = null;

    @Column(length = 500)
    private String description = null;

    @Column(length = 150)
    private String contract;

    public OfferAbstract() {
    }

    public OfferAbstract(User buyer, UserAccount buyerAccount, int offerValidSeconds) {
        this.buyer = buyer;
        this.buyerAccount = buyerAccount;
        this.offerStatus = OfferStatus.OFFER;
        this.offerValidUntil = LocalDateTime.now().plusSeconds(offerValidSeconds);
    }

    public OfferAbstract(User buyer, UserAccount buyerAccount, int offerValidSeconds, int newPrice, String description) {
        this.buyer = buyer;
        this.buyerAccount = buyerAccount;
        this.offerStatus = OfferStatus.BARGAIN;
        this.offerValidUntil = LocalDateTime.now().plusSeconds(offerValidSeconds);
        this.newPrice = newPrice;
        this.description = description;
    }

    public OfferStatus getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(OfferStatus offerStatus) {
        this.offerStatus = offerStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getOfferValidUntil() {
        return offerValidUntil;
    }

    public void setOfferValidUntil(LocalDateTime offerValidUntil) {
        this.offerValidUntil = offerValidUntil;
    }

    public Integer getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(Integer newPrice) {
        this.newPrice = newPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserAccount getBuyerAccount() {
        return buyerAccount;
    }

    public void setBuyerAccount(UserAccount buyerAccount) {
        this.buyerAccount = buyerAccount;
    }


    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }
}
