package com.spp.sgoods.model.announcement;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ANNOUNCEMENT")
public class Announcement extends AnnouncementAbstract {
    @Transient
    private boolean isFavorite = false;

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}