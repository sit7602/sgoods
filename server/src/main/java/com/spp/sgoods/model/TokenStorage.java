package com.spp.sgoods.model;


import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class TokenStorage {
    public BigInteger userId;
    public String refreshToken;
    public LocalDateTime decayTime;

    public TokenStorage(BigInteger userId, String refreshToken, LocalDateTime decayTime) {
        this.userId = userId;
        this.refreshToken = refreshToken;
        this.decayTime = decayTime;
    }
}
