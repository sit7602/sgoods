package com.spp.sgoods.model.attachment;

import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class GeoPointAttachment extends AttachmentAbstract {

    @Embedded
    private GeoPoint geoPoint;

}
