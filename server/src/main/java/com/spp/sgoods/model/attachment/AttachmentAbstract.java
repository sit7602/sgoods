package com.spp.sgoods.model.attachment;

import javax.persistence.*;

@Entity
@Table(name = "MESSAGE_ATTACHMENT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        discriminatorType = DiscriminatorType.INTEGER,
        name = "type",
        columnDefinition = "TINYINT(1)"
)
public abstract class AttachmentAbstract {
    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.ORDINAL)
    private AttachmentType type;

    public AttachmentType getType() {
        return type;
    }
}
