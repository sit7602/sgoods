package com.spp.sgoods.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigInteger;

@Entity
public class UserAccount {
    @Id
    @GeneratedValue
    private long id;

    @Column(columnDefinition = "DECIMAL(25,0)")
    private BigInteger userId;

    @Column(length = 16, nullable = false)
    private String cardNumber;

    @Column(nullable = false)
    private double balance;

    @Column()
    private double reserved;

    public UserAccount(BigInteger userId){
        this.userId = userId;
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 3; i++){
            if (i == 1)
                sb.append("********");
            else
                sb.append((int)(1000 + Math.random() * 8999.9999));
        }
        this.cardNumber = sb.toString();
        this.balance = Math.round(Math.random() * 100000);
    }

    public UserAccount(){ }

    public BigInteger getUserId() {
        return userId;
    }

    public long getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public double getReserved() {
        return reserved;
    }

    public void reserved(double reserved) {
        this.balance -= reserved;
        this.reserved += reserved;
    }

    public void unReserved(double unReserved) {
        this.balance += unReserved;
        this.reserved -= unReserved;
    }

    public void minus(double minus){
        this.reserved -= minus;
    }

    public void plus(double plus){
        this.balance += plus;
    }
}
