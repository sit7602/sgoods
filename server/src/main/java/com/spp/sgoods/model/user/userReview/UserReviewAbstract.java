package com.spp.sgoods.model.user.userReview;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class UserReviewAbstract {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private int mark;

    @Column(length = 1000)
    private String description;

    @Enumerated(value = EnumType.ORDINAL)
    private UserReviewType type;

    @Column(nullable = false)
    private boolean autoCreate;

    private LocalDateTime createDate = LocalDateTime.now();

    public UserReviewAbstract() {
    }

    public UserReviewAbstract(int mark, UserReviewType type, boolean idAutoCreate) {
        this.mark = mark;
        this.description = null;
        this.type = type;
        this.autoCreate = idAutoCreate;
    }

    public UserReviewAbstract(int mark, UserReviewType type, String description) {
        this.mark = mark;
        this.description = description;
        this.type = type;
        this.autoCreate = false;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAutoCreate(boolean autoCreate) {
        this.autoCreate = autoCreate;
    }

    public boolean isAutoCreate() {
        return autoCreate;
    }

    public void updateCreateDate() {
        this.createDate = LocalDateTime.now();
    }
}
