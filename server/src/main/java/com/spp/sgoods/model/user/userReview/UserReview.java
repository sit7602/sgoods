package com.spp.sgoods.model.user.userReview;

import com.spp.sgoods.model.user.User;

import javax.persistence.*;

@Entity
@Table(name = "USER_REVIEW")
public class UserReview extends UserReviewAbstract {

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_from", nullable = false)
    protected User userFrom;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_to", nullable = false)
    protected User userTo;

    @Column(name = "offer_id", nullable = false)
    private long offerId;

    public UserReview() {
    }

    public UserReview(User userFrom, User userTo, int mark, UserReviewType type, long offerId, boolean isAutoCreate) {
        super(mark, type, isAutoCreate);
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.offerId = offerId;
    }

    public UserReview(User userFrom, User userTo, int mark, UserReviewType type, long offerId, String description) {
        super(mark, type, description);
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.offerId = offerId;
    }
}
