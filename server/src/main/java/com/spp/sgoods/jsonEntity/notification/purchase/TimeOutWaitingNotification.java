package com.spp.sgoods.jsonEntity.notification.purchase;

import com.spp.sgoods.jsonEntity.notification.NotificationEntityAbstract;
import com.spp.sgoods.model.notification.NotificationType;
import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;

public class TimeOutWaitingNotification extends NotificationEntityAbstract{

    private OfferWithAnnouncement offer;

    public TimeOutWaitingNotification(OfferWithAnnouncement offer) {
        this.offer = offer;
    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.TIME_OUT_WAITING;
    }

}
