package com.spp.sgoods.jsonEntity.notification.purchase;

import com.spp.sgoods.model.announcement.AnnouncementAbstract;
import com.spp.sgoods.model.announcement.Checklist;
import com.spp.sgoods.model.notification.NotificationType;
import com.spp.sgoods.model.purchaseSystem.Offer;
import com.spp.sgoods.model.user.User;

import java.util.List;

public class ConformationOfSalesNotification extends PurchaseNotificationAbstract {
    private User seller;
    private List<Checklist> checklist;

    public ConformationOfSalesNotification(AnnouncementAbstract announcement, Offer offer) {
        super(announcement, offer);
        this.seller = announcement.getSeller();
        this.checklist = announcement.getProductCategory().getChecklists();
    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.CONFIRMATION_OF_SALES;
    }
}
