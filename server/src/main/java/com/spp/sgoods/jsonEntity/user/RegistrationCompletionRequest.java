package com.spp.sgoods.jsonEntity.user;

import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;

public class RegistrationCompletionRequest {
    public UserDetails userDetails;
    public UserSettings userSettings;
}
