package com.spp.sgoods.jsonEntity.notification.purchase;

import com.spp.sgoods.jsonEntity.notification.NotificationEntityAbstract;
import com.spp.sgoods.model.announcement.AnnouncementEntityAbstract;
import com.spp.sgoods.model.notification.NotificationType;
import com.spp.sgoods.model.purchaseSystem.Offer;

public abstract class PurchaseNotificationAbstract extends NotificationEntityAbstract {
    private String announcementName;
    private String photo;
    private Offer offer;

    public PurchaseNotificationAbstract(AnnouncementEntityAbstract announcement, Offer offer) {
        this.announcementName = announcement.getName();
        this.photo = announcement.getPhotos().get(0).getFileURL();
        this.offer = offer;
    }
}
