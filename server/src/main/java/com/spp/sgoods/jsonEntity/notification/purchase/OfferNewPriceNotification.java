package com.spp.sgoods.jsonEntity.notification.purchase;

import com.spp.sgoods.model.announcement.AnnouncementEntityAbstract;
import com.spp.sgoods.model.notification.NotificationType;
import com.spp.sgoods.model.purchaseSystem.Offer;

public class OfferNewPriceNotification extends PurchaseNotificationAbstract {
    public OfferNewPriceNotification(AnnouncementEntityAbstract announcement, Offer offer) {
        super(announcement, offer);
    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.OFFER_NEW_PRICE;
    }
}
