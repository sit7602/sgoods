package com.spp.sgoods.jsonEntity.announcement;

import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;

import java.time.LocalTime;

public class SellerInfo {
    public boolean hideUserDate;
    public String phone;
    public LocalTime callTimeStart;
    public LocalTime callTimeEnd;
    public short UTC;

    public SellerInfo(UserDetails userDetails, UserSettings userSettings) {
        this.hideUserDate = userSettings.isHideUserDate();
        this.phone = userDetails.getPhone();
        this.callTimeStart = userSettings.getCallTimeStart();
        this.callTimeEnd = userSettings.getCallTimeEnd();
        this.UTC = userSettings.getUTC();
    }
}
