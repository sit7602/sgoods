package com.spp.sgoods.jsonEntity.authentication;

import java.time.LocalDateTime;

public class TokenResponse {
    public String accessToken;
    public String refreshToken;
    public LocalDateTime decayTime;

    public TokenResponse(String accessToken, String refreshToken, LocalDateTime decayTime) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.decayTime = decayTime;
    }
}
