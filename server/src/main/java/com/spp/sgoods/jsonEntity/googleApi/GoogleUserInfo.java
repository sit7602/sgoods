package com.spp.sgoods.jsonEntity.googleApi;

import java.math.BigInteger;

public class GoogleUserInfo {
    public BigInteger id;
    public String name;
    public String given_name;
    public String family_name;
    public String picture;
    public String email;
    public String locate;
}
