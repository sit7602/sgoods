package com.spp.sgoods.jsonEntity.announcement;

public class ParsedAnnouncementResponse {
    private String Title;
    private String Description;
    private int Price;
    private String Photo;
    private String Filename;

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public void setFilename(String filename) {
        Filename = filename;
    }
}
