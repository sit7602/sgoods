package com.spp.sgoods.jsonEntity.googleApi;

public class GoogleTokenRequest {
    public String client_id;
    public String client_secret;
    public String code;
    public String grant_type;
    public String redirect_uri;

    public GoogleTokenRequest(String client_id, String client_secret, String code, String grant_type, String redirect_uri) {
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.code = code;
        this.grant_type = grant_type;
        this.redirect_uri = redirect_uri;
    }
}
