package com.spp.sgoods.jsonEntity.authentication;

import com.spp.sgoods.jsonEntity.user.UserInfo;
import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;

public class TokenResponseLogin {
    public TokenResponse tokens;
    public UserInfo userInfo;
}
