package com.spp.sgoods.jsonEntity.googleApi;

public class GoogleTokenResponse {
    public String access_token;
    public String expires_in;
    public String refresh_token;
    public String scope;
    public String token_type;
}
