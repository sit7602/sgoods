package com.spp.sgoods.jsonEntity.notification.purchase;

import com.spp.sgoods.jsonEntity.notification.NotificationEntityAbstract;
import com.spp.sgoods.model.announcement.AnnouncementAbstract;
import com.spp.sgoods.model.announcement.AnnouncementEntityAbstract;
import com.spp.sgoods.model.notification.NotificationType;
import com.spp.sgoods.model.purchaseSystem.Offer;
import com.spp.sgoods.model.user.User;

public class TransferConformationNotification extends PurchaseNotificationAbstract {
    private User seller;

    public TransferConformationNotification(AnnouncementAbstract announcement, Offer offer) {
        super(announcement, offer);
        this.seller = announcement.getSeller();
    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.TRANSFER_CONFIRMATION;
    }
}
