package com.spp.sgoods.jsonEntity.notification;

import com.spp.sgoods.model.notification.NotificationType;

public abstract class NotificationEntityAbstract {
    public abstract NotificationType getNotificationType();
}
