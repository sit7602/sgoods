package com.spp.sgoods.jsonEntity.announcement;

import com.spp.sgoods.model.announcement.AnnouncementType;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.List;

public class CreateAnnouncementRequest {
    public String name;
    public String description;
    public int price;
    public AnnouncementType type;
    public List<String> photoPaths;
    public int categoryId;
    public GeoPoint geoPoint;
    public boolean bargain;
    public int minPrice;
}
