package com.spp.sgoods.jsonEntity.user;

import com.spp.sgoods.model.user.User;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;

import java.util.List;

public class UserInfo {
    User user;
    UserDetails userDetails;
    UserSettings userSettings;
    List<UserAccount> userCards;

    public UserInfo(User user, UserDetails userDetails, UserSettings userSettings, List<UserAccount> userCards) {
        this.user = user;
        this.userDetails = userDetails;
        this.userSettings = userSettings;
        this.userCards = userCards;
    }
}
