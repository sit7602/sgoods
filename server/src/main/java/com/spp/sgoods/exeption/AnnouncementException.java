package com.spp.sgoods.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class AnnouncementException extends RuntimeException{
    public AnnouncementException(String message) {
        super(message);
    }
}
