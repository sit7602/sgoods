package com.spp.sgoods.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class ProductCategoryException extends RuntimeException{
    public ProductCategoryException(String message) {
        super(message);
    }
}
