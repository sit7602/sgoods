package com.spp.sgoods.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class GoogleTokenIsNotValidException extends RuntimeException {
    public GoogleTokenIsNotValidException(String message) {
        super(message);
    }
}
