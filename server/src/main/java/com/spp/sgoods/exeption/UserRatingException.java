package com.spp.sgoods.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class UserRatingException extends RuntimeException{
    public UserRatingException(String message) {
        super(message);
    }
}
