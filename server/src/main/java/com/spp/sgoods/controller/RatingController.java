package com.spp.sgoods.controller;

import com.spp.sgoods.model.user.userReview.UserReviewFromWithOffer;
import com.spp.sgoods.model.user.userReview.UserReviewTo;
import com.spp.sgoods.model.user.userReview.UserReviewToWithOffer;
import com.spp.sgoods.service.authentication.AuthenticationService;
import com.spp.sgoods.service.user.UserReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("rating")
public class RatingController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserReviewService userReviewService;

    @GetMapping("getReviewI")
    private List<UserReviewFromWithOffer> getReviewI(HttpServletRequest request) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return userReviewService.getUserReviewFrom(userId);
    }

    @GetMapping("getReviewMe")
    private List<UserReviewToWithOffer> getReviewMe(HttpServletRequest request) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return userReviewService.getUserReviewTo(userId);
    }

    @GetMapping("update")
    private boolean update(HttpServletRequest request,
                           @RequestParam("offer_id") long offerId,
                           @RequestParam("mark") int mark,
                           @RequestParam(value = "description", required = false) String description) {

        BigInteger userId = authenticationService.userAuthentication(request);
        if(description == null)
            return userReviewService.updateUserReview(userId, offerId, mark);
        else
            return userReviewService.updateUserReview(userId, offerId, mark, description);
    }

    @GetMapping("getUserReviews")
    private List<UserReviewTo> getUserReviews(@RequestParam(value = "user_id") BigInteger userId) {
        return userReviewService.getUserReviews(userId);
    }
}
