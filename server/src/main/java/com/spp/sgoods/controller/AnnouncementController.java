package com.spp.sgoods.controller;

import com.spp.sgoods.jsonEntity.announcement.CreateAnnouncementRequest;
import com.spp.sgoods.jsonEntity.announcement.ParsedAnnouncementResponse;
import com.spp.sgoods.model.announcement.Announcement;
import com.spp.sgoods.model.announcement.AnnouncementForSearch;
import com.spp.sgoods.model.announcement.AnnouncementForSeller;
import com.spp.sgoods.service.announcement.AnnouncementParserService;
import com.spp.sgoods.service.announcement.AnnouncementService;
import com.spp.sgoods.service.authentication.AuthenticationService;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

@RestController
@RequestMapping("announcement")
@CrossOrigin(maxAge = 3600)
public class AnnouncementController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private AnnouncementService announcementService;

    @Autowired
    private AnnouncementParserService announcementParserService;

    @PostMapping("createAnnouncement")
    private Announcement createAnnouncement(HttpServletRequest request, @RequestBody CreateAnnouncementRequest createAnnouncementRequest){
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.createAnnouncement(createAnnouncementRequest, userId);
    }

    @GetMapping("getAnnouncement")
    private Announcement getAnnouncement(HttpServletRequest request,
                                         @RequestParam("announcement_id") int announcementId){
        String accessToken = request.getHeader("token");
        if(accessToken == null)
            return announcementService.getAnnouncement(announcementId);
        else {
            BigInteger userId = authenticationService.userAuthentication(accessToken);
            return announcementService.getAnnouncement(userId, announcementId);
        }
    }

    @GetMapping(path = "getAnnouncements")
    private List<AnnouncementForSearch> getAnnouncements(
            HttpServletRequest request,
            @RequestParam(value = "query", required = false) String queryString,
            @RequestParam(value = "category", required = false) Integer category,
            @RequestParam(value = "price>", required = false) Integer priceGreater,
            @RequestParam(value = "price<", required = false) Integer priceLess,
            @RequestParam(value = "geoPoint", required = false) List<Double> geoPoint,
            @RequestParam(value = "distance", required = false) Integer distance,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "20") Integer size,
            @RequestParam(value = "sort", required = false) List<String> sortCriteriaList) {

        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();

        if(queryString != null)
            searchQuery.withQuery(boolQuery()
                    .should(matchQuery("name", queryString).fuzziness(5).fuzzyTranspositions(true)).boost(10)
                    .should(matchQuery("description", queryString).fuzziness(5).fuzzyTranspositions(true))
                    .should(matchQuery("productCategoryNamesTree", queryString).fuzziness(5).fuzzyTranspositions(true))
                    .minimumShouldMatch(1));

        BoolQueryBuilder filter = boolQuery();
        if(category != null)
            filter.must(matchQuery("productCategoryTree", category));

        if (priceGreater != null)
            filter.must(rangeQuery("price").gte(priceLess));

        if (priceLess != null)
            filter.must(rangeQuery("price").lte(priceLess));

        if (geoPoint!= null && distance != null)
            filter.must(geoDistanceQuery("geoPoint")
                    .point(new GeoPoint(geoPoint.get(0), geoPoint.get(1))).distance(distance + "km"));

        searchQuery.withFilter(filter);

        Pageable pageable;

        if(sortCriteriaList == null || sortCriteriaList.isEmpty())
            pageable = PageRequest.of(page, size);
        else{
            List<Sort.Order> sortOrderList = new ArrayList<>();
            for(String sortCriteria: sortCriteriaList){
                String[] sortCriteriaSplit = sortCriteria.split(":");
                Sort.Order sortOrder;
                switch (sortCriteriaSplit[1]) {
                    case "asc":
                        sortOrder = Sort.Order.asc(sortCriteriaSplit[0]);
                        break;
                    case "desc":
                        sortOrder = Sort.Order.desc(sortCriteriaSplit[0]);
                        break;

                    default:
                        continue;
                }
                sortOrderList.add(sortOrder);
            }
            pageable = PageRequest.of(page, size, Sort.by(sortOrderList));
        }

        searchQuery.withPageable(pageable);

        Query query = searchQuery.build();

        String accessToken = request.getHeader("token");
        if(accessToken == null)
            return announcementService.getAnnouncements(query);
        else {
            BigInteger userId = authenticationService.userAuthentication(accessToken);
            return announcementService.getAnnouncements(userId, query);
        }
    }

    @GetMapping("getAnnouncementsBySeller")
    private List<AnnouncementForSeller> getAnnouncementsBySeller(HttpServletRequest request){
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.getAnnouncementsBySeller(userId);
    }

    @GetMapping("getAnnouncementsByBuyer")
    private List<Announcement> getAnnouncementsByBuyer(HttpServletRequest request){
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.getAnnouncementsByBuyer(userId);
    }

    @GetMapping("setFavoriteAnnouncement")
    private boolean setFavoriteAnnouncement(HttpServletRequest request,
                                            @RequestParam("announcement_id") long announcementId) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.setFavoriteAnnouncement(userId, announcementId);
    }

    @GetMapping("getFavoriteAnnouncements")
    private List<Announcement> getFavoriteAnnouncement(HttpServletRequest request) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.getFavoriteAnnouncements(userId);
    }

    @GetMapping("deleteFavoriteAnnouncement")
    private boolean deleteFavoriteAnnouncement(HttpServletRequest request,
                                               @RequestParam("announcement_id") long announcementId) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.deleteFavoriteAnnouncement(userId, announcementId);
    }

    @GetMapping("parseAnnouncement")
    private ParsedAnnouncementResponse parseAnnouncement(HttpServletRequest request,
                                                         @RequestParam("url") String url) throws IOException {
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementParserService.parseAnnouncement(url, userId);
    }

    @GetMapping("closeAnnouncement")
    private boolean closeAnnouncement(HttpServletRequest request,
                                      @RequestParam("announcement_id") long announcementId) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return announcementService.closeAnnouncement(userId, announcementId);
    }
}
