package com.spp.sgoods.controller;

import com.spp.sgoods.exeption.AuthenticationException;
import com.spp.sgoods.exeption.GoogleTokenIsNotValidException;
import com.spp.sgoods.jsonEntity.authentication.TokenResponse;
import com.spp.sgoods.jsonEntity.authentication.TokenResponseLogin;
import com.spp.sgoods.service.authentication.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    private TokenResponseLogin googleCallback(@RequestParam(name = "google_token") String googleToken){
        TokenResponseLogin tokenResponseLogin = authenticationService.getTokenLogin(googleToken);
        if(tokenResponseLogin == null)
            throw new GoogleTokenIsNotValidException("Недействительный токен Google OAuth2");
        return tokenResponseLogin;
    }

    @GetMapping("refresh")
    private TokenResponse tokenRefresh(@RequestParam(name = "access_token") String accessToken,
                                       @RequestParam(name = "refresh_token") String refreshToken){
        if(accessToken == null || refreshToken == null)
            throw new AuthenticationException("Не все данные введены корректно");
        return authenticationService.getTokenRefresh(accessToken, refreshToken);
    }

    @GetMapping("test")
    private BigInteger test(HttpServletRequest request){
        String accessToken = request.getHeader("token");
        if (accessToken == null)
            throw new AuthenticationException("Недействительный токен доступа");
        return authenticationService.userAuthentication(accessToken);
    }
}
