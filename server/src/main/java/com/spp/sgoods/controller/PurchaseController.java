package com.spp.sgoods.controller;

import com.spp.sgoods.model.purchaseSystem.OfferWithAnnouncement;
import com.spp.sgoods.model.purchaseSystem.Purchase;
import com.spp.sgoods.service.authentication.AuthenticationService;
import com.spp.sgoods.service.purchaseSystem.OfferService;
import com.spp.sgoods.service.purchaseSystem.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("purchase")
@CrossOrigin(maxAge = 3600)
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private OfferService offerService;

    @GetMapping("purchaseOffer")
    private boolean purchaseOffer(HttpServletRequest request,
                                  @RequestParam("announcement_id") long announcementId,
                                  @RequestParam("card_id") long cardId,
                                  @RequestParam("offer_valid_seconds") int offerValidSeconds){
        BigInteger userId = authenticationService.userAuthentication(request);
        return purchaseService.purchaseOffer(announcementId, userId, cardId, offerValidSeconds);
    }

    @GetMapping("bargainOffer")
    private boolean bargainOffer(HttpServletRequest request,
                                 @RequestParam("announcement_id") long announcementId,
                                 @RequestParam("card_id") long cardId,
                                 @RequestParam("offer_valid_seconds") int offerValidSeconds,
                                 @RequestParam("new_price") int newPrice ,
                                 @RequestParam("description") String description){
        BigInteger userId = authenticationService.userAuthentication(request);
        return purchaseService.bargainOffer(announcementId, userId, cardId, offerValidSeconds, newPrice, description);
    }

    @GetMapping("acceptOffer")
    private boolean acceptOffer(HttpServletRequest request,
                                @RequestParam("announcement_id") long announcementId,
                                @RequestParam("offer_id") int offerId){
        BigInteger userId = authenticationService.userAuthentication(request);
        return purchaseService.acceptOffer(announcementId, userId, offerId);
    }

    @GetMapping("rejectOffer")
    private boolean rejectOffer(HttpServletRequest request,
                                @RequestParam("announcement_id") long announcementId,
                                @RequestParam("offer_id") int offerId){
        BigInteger userId = authenticationService.userAuthentication(request);
        return purchaseService.rejectOffer(announcementId, userId, offerId);
    }

    @GetMapping("completionOfPurchase")
    private Purchase completionOfPurchase(HttpServletRequest request,
                                          @RequestParam("offer_id") int offerId) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return purchaseService.completionOfPurchase(offerId, userId);
    }

    @GetMapping("getOffersByBuyer")
    private List<OfferWithAnnouncement> getAnnouncementsByBuyer(HttpServletRequest request){
        BigInteger userId = authenticationService.userAuthentication(request);
        return offerService.getOffersByBuyer(userId);
    }
}
