package com.spp.sgoods.controller;

import com.spp.sgoods.model.BugReport;
import com.spp.sgoods.service.authentication.AuthenticationService;
import com.spp.sgoods.service.util.BugReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("bugReport")
@CrossOrigin(maxAge = 3600)
public class BugReportController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private BugReportService bugReportService;

    @GetMapping
    private List<BugReport> getBugReports() {
        return bugReportService.getBugReports();
    }

    @PostMapping
    private boolean createBugReport(HttpServletRequest request, @RequestBody BugReport bugReport) {
        BigInteger userId = authenticationService.userAuthentication(request);
        return bugReportService.createBugReport(userId, bugReport);
    }
}
