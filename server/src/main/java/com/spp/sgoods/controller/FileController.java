package com.spp.sgoods.controller;

import com.spp.sgoods.service.authentication.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("uploadFile")
public class FileController {
    @Value("${content.storage.path}")
    private String contentStoragePath;

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping()
    private List<String> uploadFile(HttpServletRequest request, @RequestParam("files") List<MultipartFile> files) throws IOException {
        BigInteger userId = authenticationService.userAuthentication(request);
        File uploadDirectory = new File(contentStoragePath + "/" + userId);
        if (!uploadDirectory.exists()) {
            uploadDirectory.mkdir();
        }
        List<String> filenames = new ArrayList<>();
        for(MultipartFile file : files)
            if (file != null) {
                String filename = UUID.randomUUID().toString() + file.getOriginalFilename();
                String uploadPath = uploadDirectory + "/" + filename;
                file.transferTo(new File(uploadPath));
                filenames.add(filename);
            }
        return filenames;
    }
}
