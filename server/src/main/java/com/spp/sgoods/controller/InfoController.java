package com.spp.sgoods.controller;

import com.spp.sgoods.model.announcement.FieldDescription;
import com.spp.sgoods.model.announcement.ProductCategoryForInfo;
import com.spp.sgoods.service.announcement.FieldDescriptionService;
import com.spp.sgoods.service.announcement.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("info")
public class InfoController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @Autowired
    private FieldDescriptionService fieldDescriptionService;

    @GetMapping("getCategoryList")
    private List<ProductCategoryForInfo> getCategoryList(){
        return productCategoryService.getProductCategoryList();
    }

    @GetMapping("getFieldDescription")
    private List<FieldDescription> getFieldDescription(@RequestParam("category_id") long categoryId) {
        return fieldDescriptionService.getFieldDescriptionList(categoryId);
    }

}
