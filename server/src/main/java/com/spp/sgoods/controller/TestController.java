package com.spp.sgoods.controller;

import com.spp.sgoods.model.announcement.Announcement;
import com.spp.sgoods.model.announcement.AnnouncementForSearch;
import com.spp.sgoods.model.announcement.AnnouncementStatus;
import com.spp.sgoods.repository.AnnouncementForSearchRepository;
import com.spp.sgoods.repository.AnnouncementRepository;
import com.spp.sgoods.service.util.PdfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;

@Controller
@RequestMapping("test")
public class TestController {
    @Autowired
    private AnnouncementRepository announcementRepository;

    @Autowired
    private AnnouncementForSearchRepository service;

    @Autowired
    private ElasticsearchRestTemplate elasticsearch;

    @Value("${elasticsearch.host}")
    private String host;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private PdfService pdfService;

    @GetMapping
    private String testGet(ModelMap variables1, @RequestParam(value = "p", required = false) String p){
//        Map<String, Object> variables = new HashMap<>();
//        LocalDate localDate = LocalDate.now();
//        String date = localDate.format(DateTimeFormatter.ofLocalizedDate( FormatStyle.FULL ).withLocale(new Locale("ru")));
//        Random random = new Random();
//
//        variables.put("numDog", random.nextInt(100000));
//        variables.put("product", "Тарелка");
//        variables.put("price", 100);
//
//        variables.put("date", date);
//
//        variables.put("sellerName", "Продавец");
//        variables.put("sellerPhone", "88005553535");
//        variables.put("sellerPassportNum", 10000000 + random.nextInt(89999999));
//        variables.put("sellerPassportDate", localDate.minusDays(random.nextInt(1000)));
//
//        variables.put("buyerName", "Покупатель");
//        variables.put("buyerPhone", "88005553535");
//        variables.put("buyerPassportNum", 10000000 + random.nextInt(89999999));
//        variables.put("buyerPassportDate", localDate.minusDays(random.nextInt(1000)));
//
//        String a = pdfService.generatePdfThymeleaf(variables, "contract", "C:\\content\\1.pdf");

        return "contract";
    }

    @PostMapping
    private void testPost (){
        taskScheduler.schedule(this::test, Instant.now().plusSeconds(10));
        int a = 5;
    }

    private void test() {
        int a = 5;
    }

    @GetMapping("updateElastic")
    private String updateElastic(){
        elasticsearch.deleteIndex(AnnouncementForSearch.class);
        elasticsearch.createIndex(AnnouncementForSearch.class);
        for (Announcement announcement: announcementRepository.findAllByStatus(AnnouncementStatus.OPEN))
            service.save(new AnnouncementForSearch(announcement));
        return "OK";
    }

}
