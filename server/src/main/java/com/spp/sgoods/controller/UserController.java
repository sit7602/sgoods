package com.spp.sgoods.controller;

import com.spp.sgoods.exeption.ForbiddenException;
import com.spp.sgoods.jsonEntity.user.RegistrationCompletionRequest;
import com.spp.sgoods.jsonEntity.user.UserInfo;
import com.spp.sgoods.model.user.UserAccount;
import com.spp.sgoods.model.user.UserDetails;
import com.spp.sgoods.model.user.UserSettings;
import com.spp.sgoods.service.authentication.AuthenticationService;
import com.spp.sgoods.service.user.UserAccountService;
import com.spp.sgoods.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("getUserInfo")
    private UserInfo getUserInfo(HttpServletRequest request){
        BigInteger userId = authenticationService.userAuthentication(request);
        return userService.getUserInfo(userId);
    }

    @PostMapping("setUserDetails")
    private UserDetails setUserDetails(HttpServletRequest request,
                                @RequestBody UserDetails userDetails){
        BigInteger userId = authenticationService.userAuthentication(request);
        if(userId.equals(userDetails.getUserId()))
            return userService.setUserDetails(userId, userDetails);
        else throw new ForbiddenException();
    }

    @PostMapping("setUserSettings")
    private UserSettings setUserSettings(HttpServletRequest request,
                                 @RequestBody UserSettings userSettings){
        BigInteger userId = authenticationService.userAuthentication(request);
        if(userId.equals(userSettings.getUserId()))
            return userService.setUserSettings(userId, userSettings);
        else throw new ForbiddenException();
    }

    @PostMapping("registrationCompletion")
    private UserInfo registrationCompletion(HttpServletRequest request,
                                            @RequestBody RegistrationCompletionRequest registrationCompletionRequest){
        BigInteger userId = authenticationService.userAuthentication(request);
        return userService.setUserDetailsAndSettings(userId, registrationCompletionRequest.userDetails,
                registrationCompletionRequest.userSettings);
    }

    @GetMapping("getUserCardsInfo")
    private List<UserAccount> getUserCardInfo(HttpServletRequest request){
        BigInteger userId = authenticationService.userAuthentication(request);
        return userAccountService.getUserAccounts(userId);
    }
}
