package com.spp.sgoods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgoodsApplication {
	public static void main(String[] args) {
		SpringApplication.run(SgoodsApplication.class, args);
	}
}
