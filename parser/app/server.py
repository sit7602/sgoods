from flask import Flask, request, jsonify
import avito_parser
import json


app = Flask(__name__)

@app.route('/get_info/')
def get_info():
    try:
        url = request.args.get('url')
        res = avito_parser.get_single_page(url)
        return jsonify(res)
    except Exception:
        return jsonify({"error": True, "message": "Page not found"})

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=7070)
