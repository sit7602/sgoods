from urllib.parse import quote
from datetime import datetime
import locale
import requests
from bs4 import BeautifulSoup

class TooManyRequests(Exception):
    '''raises in fetch_page function if request redirected to https://www.avito.ru/blocked'''
    pass

def agregate_ad_info(ad):
    return {
        'Title': get_title(ad),
        'Description': get_description(ad),
        'Price': get_price(ad),
        'Photo': get_photo(ad)
    }


def get_title(ad):

    text = ad.find('span', attrs={'class': 'title-info-title-text'})
    return text.findAll(text=True)[0]


def get_description(ad):
    desciption = ad.find('div', attrs={'class': 'item-description-text'}).find('p')
    desciption = ''.join(desciption.findAll(text=True))
    return desciption

def get_price(ad):
    price = int(ad.find('span', attrs={'class': 'js-item-price'})['content'])
    return price if price else None

def get_photo(ad):
    photo = ad.find('div', attrs={'class': 'gallery-img-frame js-gallery-img-frame'}).find('img')
    return photo['src'].replace("//", "")

def get_ads_from_page(page):
    print (page[:20])
    return get_beautiful_soup(page)


def fetch_page(page_url):
    '''Returns page html as string

    raises TooManyRequest if request redirected to https://www.avito.ru/blocked
    '''
    responce = requests.get(page_url)
    if responce.url == 'https://www.avito.ru/blocked':
        raise TooManyRequests('IP temporarily blocked')
    return requests.get(page_url).text


def get_beautiful_soup(html):
    return BeautifulSoup(html, 'html.parser')


def is_page_exists(page):
    return get_beautiful_soup(page).find('div', attrs={'class': 'nulus'}) is None

def get_single_page(url):
    result = None
    page = fetch_page(url)
    print('page = ')
    # print(page)
    ad = get_ads_from_page(page)
    print ('ad')
    # print(ad)
    result = agregate_ad_info(ad)
    print('result')
    print (result)
    return result

