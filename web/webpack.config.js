const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const vars = require('./dev.variables');

module.exports = {
    mode: 'development',
    devtool: "source-map",
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.svg']
    },
    stats: {
        children: false
    },
    entry: "./src/index.tsx",
    output: {
        path: path.join(__dirname, '/dist'), // '/', //
        filename: 'bundle.min.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                // exclude: /node_modules/,
                loader: "awesome-typescript-loader"
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            debugmode: vars.development.debug
        })
    ],
    devServer: {
        historyApiFallback: true,
        port: 80,
        host: '0.0.0.0',
        disableHostCheck: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        useLocalIp: false
    },
    externals: {
        // "react": "React",
        // "react-dom": "ReactDOM",
        // global app config object
        config: JSON.stringify({
            apiUrl: 'http://localhost:8080'
        })
    }
}
