import {fetchWithAuth} from "../helpers";

const apiUrl = process.env.API_URL;
const anouncement = 'announcement'

function createListing(formData) {
    return fetchWithAuth(`${apiUrl}/${anouncement}/createAnnouncement`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => response.json())
        .then(r => {
            return r;
        })
        .catch(error => {
        return {error};
    });
}

function closeAnnouncement(id) {
    return fetchWithAuth(`${apiUrl}/announcement/closeAnnouncement?announcement_id=${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json()).then(r => {
        if (r.status && r.status !== 200) {
            return Promise.reject(r.message);
        }
        return r;
    }).catch(error => {
        return Promise.reject(error);
    })
}

/**
 *
 * @param {Object} params
 * @param {Array} prevData
 * @returns {Promise<T | {error: any}>}
 */

function getListings(params=null, prevData=null) {
    let url = `${apiUrl}/${anouncement}/getAnnouncements`;
    if (params) {
        let sp = new URLSearchParams();
        const { search, category, sort, page, size } = params;
        Object.keys(params).forEach((param) => {
            if (params[param] !== null && params[param] !== '') {
               sp.append(param, params[param]);
            }
        });
        url = url + `?${sp.toString()}`;
    }

    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(r => r.json())
        .then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            return r;
        })
        .catch(error => {
        return Promise.reject(error);
    });
}

function getUserListings() {
    return fetchWithAuth(`${apiUrl}/${anouncement}/getAnnouncementsBySeller`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json()).then(r => {
        if (r.status && r.status !== 200) {
            return Promise.reject(r.message);
        }
        return r;
    }).catch(error => {
        return {error};
    });
}

function uploadFile(data) {
    const formData = new FormData();
    Array.from(data).forEach(img => {
        formData.append("files", img);
    });
    return fetchWithAuth(`${apiUrl}/uploadFile`,{
        method: 'POST',
        body: formData
    }).then(response => response.json())
        .then(r => {
            return r;
        })
        .catch(e => {
            console.error(e)
            return Promise.reject(e);
        });
}

function getOffersByBuyer() {
    return fetchWithAuth(`${apiUrl}/purchase/getOffersByBuyer`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json()).then(r => {
        if (r.status && r.status !== 200) {
            return Promise.reject(r.message);
        }
        return r;
    }).catch(error => {
        return {error};
    })
}

function getAnnouncementById(id) {
    return fetchWithAuth(`${apiUrl}/${anouncement}/getAnnouncement?announcement_id=${parseInt(id)}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json()).then(r => {
        if (r.error && r.status !== 200) {
            return Promise.reject(r.message || r.error || r.status)
        }
        return r;
    }).catch(error => {
        return {error};
    });
}

function like(id) {
    const url = `${apiUrl}/${anouncement}/setFavoriteAnnouncement?announcement_id=${id}`;
    return fetchWithAuth(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(r => r.json())
            .then(r => {
                if (r.status && r.status !== 200) {
                    return Promise.reject(r.message);
                }
                return r;
            })
            .catch(error => {
            return Promise.reject(error);
        });
}

function dislike(id) {
    const url = `${apiUrl}/${anouncement}/deleteFavoriteAnnouncement?announcement_id=${id}`;
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(r => r.json())
        .then(r => {
                if (r.status && r.status !== 200) {
                    return Promise.reject(r.message);}
                 return r;
        })
          .catch(error => {
            return Promise.reject(error);
       });
}

function getLiked() {
    const url = `${apiUrl}/${anouncement}/getFavoriteAnnouncements`;
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(r => r.json())
        .then(r => {
                if (r.status && r.status !== 200) {
                    return Promise.reject(r.message);}
                 return r;
        })
        .catch(error => {
            return Promise.reject(error);
   });
}

function importAnnoncement(url) {
    console.log(`query url is ${apiUrl}/announcement/parseAnnouncement?url=${url}`);
    
    return fetchWithAuth(`${apiUrl}/announcement/parseAnnouncement?url=${url}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
    .then(r => {
        if (r.status && r.status !== 200) {
            return Promise.reject(r.message);
        }
        return r;
    }).catch(error => {
        return Promise.reject(error);
    });
}

export const listingService = {
    createListing,
    closeAnnouncement,
    getListings,
    getAnnouncementById,
    getUserListings,
    uploadFile,
    getOffersByBuyer,
    like,
    dislike,
    getLiked,
    importAnnoncement
};
