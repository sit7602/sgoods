// TODO: import config from docker network here
import { authHeader } from "../helpers";
const apiUrl = process.env.API_URL;
import {saveToken, fetchWithAuth} from "../helpers";


function login(token) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    };
    const requestUrl = `${apiUrl}/auth/?google_token=${token}`;
    console.log(process.env)
    console.log(requestUrl)
    return fetch(requestUrl, requestOptions)
        .then(resp => resp.json()).then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            const { tokens, userInfo } = r;
            saveToken(tokens);
            const user = userInfo.user;
            user.userDetails = userInfo.userDetails;
            user.userSettings = userInfo.userSettings;
            user.userCards = userInfo.userCards;
            localStorage.setItem('user', JSON.stringify(user));
            return r;
        }).catch(e => {
            return Promise.reject(e);
        });
}

function register(formData) {
    return fetchWithAuth(`${apiUrl}/user/registrationCompletion`, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => response.json()).then(r => {
        if (r.status === 200) {
            return r;
        }

        return {error: r.error, message: r.message};
    }).catch(e => {
        console.error('register error');
        console.error(e);
    });

}

function logout() {
    localStorage.removeItem("user");
    sessionStorage.removeItem('tokens');
}

function getUserCards() {
    return fetchWithAuth(`${apiUrl}/user/getUserCardsInfo`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
        .then(r => {
            return r;
        }).catch(e => {
            console.log('error')
            console.log(e);
        });
}

function createListing(formData) {
    return fetchWithAuth(`${apiUrl}/announcement/createAnnouncement`, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(r => r.json()).catch(error => {
        return {error};
    });
}

/**
 *
 * @param {Object} announcement_id, card_id, offer_valid_seconds
 */
function purchase(params) {
    let sp = new URLSearchParams();
    Object.keys(params).forEach((param) => {
        sp.append(param, params[param]);
    });
    const url = `${apiUrl}/purchase/purchaseOffer?${sp.toString()}`;
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
        .then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            return r;
        })
        .catch(e => {
            console.error(e)
            return Promise.reject(e);
        });
}

/**
 *
 * @param {Object} params
 */
function acceptPurchaseOffer(params) {
    let sp = new URLSearchParams();
    Object.keys(params).forEach((param) => {
        sp.append(param, params[param]);
    });
    const url = `${apiUrl}/purchase/acceptOffer?${sp.toString()}`;
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
        .then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            return r;
        })
        .catch(e => {
            return Promise.reject(e);
        });
}

/**
 *
 * @param {Object} params
 */
function rejectPurchaseOffer(params) {
    let sp = new URLSearchParams();
    Object.keys(params).forEach((param) => {
        sp.append(param, params[param]);
    });
    const url = `${apiUrl}/purchase/rejectOffer?${sp.toString()}`;
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => r.json())
        .then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            return r;
        }).catch(e => {
            return Promise.reject(e);
        })

}

function completeOfPurchase(params) {
    let sp = new URLSearchParams();
    Object.keys(params).forEach((param) => {
        sp.append(param, params[param]);
    });
    const url = `${apiUrl}/purchase/completionOfPurchase?${sp.toString()}`;
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => r.json())
        .then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            return r;
        })
        .catch(e => {
            return Promise.reject(e);
        });
}

function purchaseBargainOffer(params) {
    let sp = new URLSearchParams();
    Object.keys(params).forEach((param) => {
        sp.append(param, params[param]);
    });
    const url = `${apiUrl}/purchase/bargainOffer?${sp.toString()}`
    return fetchWithAuth(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => r.json())
        .then(r => {
            if (r.status && r.status !== 200) {
                return Promise.reject(r.message);
            }
            return r;
        })
        .catch(e => {
            return Promise.reject(e);
        });
}

function sendBugReport(formData) {
    return fetchWithAuth(`${apiUrl}/bugReport`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(r => r.json()).then(response => {
        return response;
    }).catch(error => {
        return {error};
    });
}

function getInfo() {
    return fetchWithAuth(`${apiUrl}/user/getUserInfo`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => r.json()).then(response => {
        return response;
    }).catch(error => {
        return {error};
    });
}

function updateRating(params) {
    let sp = new URLSearchParams();
    Object.keys(params).forEach((param) => {
        sp.append(param, params[param]);
    });
    return fetchWithAuth(`${apiUrl}/rating/update?${sp.toString()}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => r.json()).then(response => {
        console.log(response);
    }).catch(error => {
        return {error};
    });
    
}

export const userService = {
    login,
    register,
    logout,
    createListing,
    getUserCards,
    getInfo,
    purchase,
    acceptPurchaseOffer,
    rejectPurchaseOffer,
    completeOfPurchase,
    purchaseBargainOffer,
    sendBugReport,
    updateRating
};
