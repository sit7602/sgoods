import { fetchWithAuth } from "../helpers";

const apiUrl = process.env.API_URL;

function getReviewMe() {
    return fetchWithAuth(`${apiUrl}/rating/getReviewMe`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => {
        return r.json();
    }).catch(error => {
        return {error};
    });
}

function getReviewI() {
    return fetchWithAuth(`${apiUrl}/rating/getReviewI`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => {
        return r.json();
    }).catch(error => {
        return {error};
    });
}


export const reviewService = {
    getReviewMe,
    getReviewI
};
