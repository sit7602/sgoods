import { fetchWithAuth } from "../helpers";

const apiUrl = process.env.API_URL;

function getCategories() {
    return fetchWithAuth(`${apiUrl}/info/getCategoryList`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r => {
        return r.json();
    }).catch(error => {
        return {error};
    });
}

function getFieldDescription(id) {    
    return fetchWithAuth(`${apiUrl}/info/getFieldDescription?category_id=${parseInt(id)}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(r=> r.json())
        .then(response => {
            console.log('RESPONSE');
            console.log(response);
            
            return response;
        })
        .catch(error => {
            console.error(error);
            
            return {error};
        });
}

export const categoryService = {
    getCategories,
    getFieldDescription
};
