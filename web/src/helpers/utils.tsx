import {IMG_ERROR_PH} from "../constants/img.constants";
import * as M from 'materialize-css';

export const showMessage = (text, href = null): void => {
    M.toast({
        html: `
            <span>${text}</span>
            ${href !== null ?
                `<a href="${href !== null ? href : '#'}" class="btn-flat toast-action">Открыть</a` :
                ''
            }
        `,
        displayLength: 15000
    });
}

export const handleImgError = (ev): void => {
    ev.target.src = IMG_ERROR_PH;
}