import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import rootReducer from '../reducers';

const loggerMiddleware = createLogger();

export const store = process.env.API_URL === 'http://localhost:8080' ? createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )) :
    createStore(
        rootReducer,
        applyMiddleware(
            thunkMiddleware,
            // loggerMiddleware
        )
    );
