/**
 *
 * @param {Object} tokens {accessToken: string, refreshToken: string}
 */
export function saveToken(tokens) {
    sessionStorage.setItem('tokens', JSON.stringify(tokens));
}

/**
 *
 * @param {Object} token {accessToken: string, refreshToken: string}
 * @returns {Promise<T>}
 */
export function refreshToken(token) {
    const { accessToken, refreshToken } = token;
    const sp = new URLSearchParams();
    sp.set('access_token', accessToken);
    sp.set('refresh_token', refreshToken);
    return fetch(`${process.env.API_URL}/auth/refresh?${sp.toString()}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'token': accessToken,
            'Access-Control-Allow-Origin': '*'
        },
    }).then(r => r.json()).then(resp => {
        if (resp.error) {
            return Promise.reject(resp.error);
        }
        saveToken(resp);
        return resp;
    })
    .catch(e => {
        console.log(e);
        // window.location.href = '/login';
    });
}

/**
 *
 * @param {String} url API full url
 * @param {Object} options simple fetch options, like method or header (without token!)
 * @returns {Promise<Response|void>}
 */
export async function fetchWithAuth(url, options) {

    const loginUrl = '/login';
    let tokenData = null;

    if (sessionStorage.tokens) {
        tokenData = JSON.parse(sessionStorage.tokens);
    } else {
        return window.location.href = loginUrl;
    }

    if (!options.headers) {
        options.headers = {
            'Access-Control-Allow-Origin': '*'
        };
    }

    if (tokenData !== null) {
        if (Date.now() >= tokenData.decayTime * 1000) {
            try {
                const newToken = await refreshToken(tokenData);
                // saveToken(newToken);
                tokenData = newToken;
            } catch (e) {
                console.error('error refresh token');
                console.log(e)
                // return  window.location.replace(loginUrl);
            }
        }

        options.headers.token = tokenData.accessToken;
        options.headers['Access-Control-Allow-Origin'] = '*';
        return fetch(url, options);
    } else {
        window.location.href = loginUrl;
    }

}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}
