export function authHeader() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user && user.tokens) {
        return { ...user.tokens };
    }
    return {};
}
