import { userConstants } from '../constants';
import { userService } from '../services';
import { alertActions } from "./alert.actions";
import { history } from "../helpers";

function login(token) {
    function request(user) { return {type: userConstants.LOGIN_REQUEST, user} }
    function success(user) { return {type: userConstants.LOGIN_SUCCESS, user} }
    function failure(user) { return {type: userConstants.LOGIN_FAILURE, user} }

    return dispatch => {
        dispatch(request({token}));

        userService.login(token).then(
            r => {
                const {userInfo, tokens} = r;

                const user = userInfo.user;
                user.userDetails = userInfo.userDetails;
                user.userSettings = userInfo.userSettings;
                user.userCards = userInfo.userCards;

                dispatch(success(user));

                if (user.userDetails !== null) {
                    history.push('/');
                    return;
                }
                history.push('/registration');
            },
            error => {
                console.log(error);

                dispatch(failure(error.toString()));
                // dispatch(alertActions.error(error.toString()));
                window.location.href = '/login'
            }
        )
    };
}

function register(user, formData) {
    function request(user) { return {type: userConstants.REGISTRATION_REQUEST, user} }
    function success(user) { return {type: userConstants.REGISTRATION_SUCCESS, user} }
    function failure(user) { return {type: userConstants.REGISTRATION_FAILURE, user} }

    return dispatch => {
        dispatch(request(user));
        userService.register(formData).then(
            r => {
                if (!r.error) {
                    dispatch(success(r));
                    alertActions.success(`register success` );
                    history.push('/');
                } else {
                    dispatch(failure(r))
                    alertActions.error(r.message);
                }

            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        )

    };
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function getCards() {
    throw Error('not implemented');
}

function purchase(params) {
    function request(params) { return {type: userConstants.PURCHASE_REQUEST, params } }
    function success(r) { return {type: userConstants.PURCHASE_SUCCESS, r} }
    function failure(r) { return {type: userConstants.PURCHASE_FAILURE, r} }

    return dispatch => {
        dispatch(request(params));
        userService.purchase(params).then(
            r => {
                dispatch(success(r));
                alertActions.success('Успешный резерв');
                history.push('/');
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    }
}

function acceptPurchaseOffer(params) {
    function request(params) { return { type: userConstants.ACCEPT_PURCHASE_REQUEST, params } }
    function success(r) { return { type: userConstants.ACCEPT_PURCHASE_SUCCESS, r } }
    function failure(r) { return { type: userConstants.ACCEPT_PURCHASE_FAILURE, r } }

    return dispatch => {
        dispatch(request(params));
        userService.acceptPurchaseOffer(params).then(
            r => {
                dispatch(success(r));
                alertActions.success('Предложение принято');
                window.location.reload(true)
                // history.push('/account');
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    }
}

function rejectPurchaseOffer(params) {
    function request(params) { return { type: userConstants.REJECT_PURCHASE_REQUEST, params } }
    function success(r) { return { type: userConstants.REJECT_PURCHASE_SUCCESS, r } }
    function failure(r) { return { type: userConstants.REJECT_PURCHASE_FAILURE, r } }

    return dispatch => {
        dispatch(request(params));
        userService.rejectPurchaseOffer(params).then(
            r => {
                dispatch(success(r));
                alertActions.success('Предложение отклонено');
                window.location.reload(true);
            },
            error => {
                dispatch(failure(error.toString()));
            }
        );
    }
}

function completePurchaseOffer(params) {
    function request(params) { return { type: userConstants.COMPLETE_PURCHASE_REQUEST, params } }
    function success(r) { return { type: userConstants.COMPLETE_PURCHASE_SUCCESS, r } }
    function failure(r) { return { type: userConstants.COMPLETE_PURCHASE_FAILURE, r } }

    return dispatch => {
        dispatch(request(params));
        userService.completeOfPurchase(params).then(
            r => {
                dispatch(success(r));
                alertActions.success('Предложение принято');
                history.push('/account');
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    }
}

function purchaseBargainOffer(params) {
    function request(params) { return { type: userConstants.PURCHASE_BARGAIN_REQUEST, params } }
    function success(r) { return { type: userConstants.PURCHASE_BARGAIN_SUCCESS, r } }
    function failure(r) { return { type: userConstants.PURCHASE_BARGAIN_FAILURE, r } }

    return dispatch => {
        dispatch(request(params));
        userService.purchaseBargainOffer(params).then(
            r => {
                dispatch(success(r));
                alertActions.success('Предложение о торге отправлено');
                history.push('/account');
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(error.toString());
            }
        );
    }
}

function sendBugReport(formData) {
    function request(formData) { return {type: userConstants.BUG_REPORT_REQUEST, formData} }
    function success(r) { return {type: userConstants.BUG_REPORT_SUCCESS, r} }
    function failure(r) { return {type: userConstants.COMPLETE_PURCHASE_FAILURE, r} }

    return dispatch => {
        dispatch(request(formData));
        userService.sendBugReport(formData).then(
            r => {
                dispatch(success(r));
                alertActions.success('Отчет об ошибке успешно отправлен');
                
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(
                    'Сегодня не ваш день :( Не удалось отправить отчет об ошибке на сервер'
                    );
            }
        )
    }
    
}

export const userActions = {
    login,
    register,
    logout,
    getCards,
    purchase,
    acceptPurchaseOffer,
    rejectPurchaseOffer,
    completePurchaseOffer,
    purchaseBargainOffer,
    sendBugReport
};
