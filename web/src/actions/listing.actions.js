import {listingConstants, userConstants} from '../constants';
import { listingService } from '../services';
import { alertActions } from "./alert.actions";
import { history } from "../helpers";

function merge(old, new1) {

}

function create(formData) {

    function request(formData) { return {type: listingConstants.CREATE, formData} }
    function success(r) { return {type: listingConstants.CREATE_SUCCESS, r} }
    function failure(r) { return {type: listingConstants.CREATE_FAILURE, r} }

    return dispatch => {
        dispatch(request(formData));
        listingService.createListing(formData).then(
            r => {
                console.log(r);
                
                dispatch(success(r));
                alertActions.success(`Объявление успешно создано` );
                // history.push('/account');
                window.location.href = '/account';
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        )

    };

}

function close(id) {
    function request(id) { return {type: listingConstants.CLOSE_ANNOUNCEMENT, id} }
    function success(r) { return {type: listingConstants.CLOSE_ANNOUNCEMENT_SUCCESS, r} }
    function failure(r) { return {type: listingConstants.CLOSE_ANNOUNCEMENT_FAILURE, r} }

    return dispatch => {
        dispatch(request(id));
        listingService.closeAnnouncement(id).then(
            r => {
                console.log(r);
                dispatch(success(r));
                alertActions.success(`Объявление убрано в архив`);
                // window.location.reload();
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        );
    }
}

function get(params, prevData) {
    function request(params) { return {type: listingConstants.GET_LISTINGS, params }}
    function success(listings) { return {type: listingConstants.GET_LISTINGS_SUCCESS, listings} }
    function failure(r) { return {type: listingConstants.GET_LISTINGS_FAILURE, r} }

    return dispatch => {
        dispatch(request(params));
        listingService.getListings(params).then(
            r => {
                console.log("listingService.getListings(params)");
                console.log(r);
                if (prevData && prevData.length) {
                    prevData = prevData.concat(r)
                    dispatch(success(prevData));
                } else {
                    dispatch(success(r))
                }
            },
            error => {
                dispatch(failure([]));
                alertActions.error(error.toString());
            }
        );
    }
}

function getById(id) {
    function request(id) { return {type: listingConstants.GET_ANNOUNCEMENT_BY_ID_REQUEST, id }}
    function success(singleAnnouncement) { return {type: listingConstants.GET_ANNOUNCEMENT_BY_ID_SUCCESS, singleAnnouncement} }
    function failure(r) { return {type: listingConstants.GET_ANNOUNCEMENT_BY_ID_FAILURE, r} }

    return dispatch => {
      dispatch(request(id));
      listingService.getAnnouncementById(id).then(
          r => {
              dispatch(success(r));
          },
          error => {
              dispatch(failure(error.toString()))
              alertActions.error(error.toString())
          }
      );
    };


}

function getUserListings() {
    function request() { return {type: listingConstants.GET_LISTINGS_USER }}
    function success(listings) { return {type: listingConstants.GET_LISTINGS_USER_SUCCESS, listings} }
    function failure(r) { return {type: listingConstants.GET_LISTINGS_USER_FAILURE, r} }

    return dispatch => {
        dispatch(request());
        listingService.getUserListings().then(
            r => {
                if (r.error) {
                    dispatch(failure(r.error))
                } else {
                    dispatch(success(r));
                }
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(error.toString());
            }
        );
    }
}

function uploadFile(formData) {
    function request(formData) { return {type: listingConstants.UPLOAD_FILE, formData} }
    function success(files) { return {type: listingConstants.UPLOAD_FILE_SUCCESS, files} }
    function failure(r) { return {type: listingConstants.UPLOAD_FILE_FAILURE, r} }

    return dispatch => {
        dispatch(request(formData));
        listingService.uploadFile(formData).then(
            r => {
                dispatch(success(r));
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error('Размер файлов слишком большой');
            }
        );
    }

}

function getOffersByBuyer() {
    function request() { return {type: listingConstants.GET_OFFERS_BY_BUYER }}
    function success(listingOffers) { return {type: listingConstants.GET_OFFERS_BY_BUYER_SUCCESS, listingOffers} }
    function failure(r) { return {type: listingConstants.GET_OFFERS_BY_BUYER_FAILURE, r} }

    return dispatch => {
        dispatch(request());
        listingService.getOffersByBuyer().then(
            r => {
                if (r.error) {
                    dispatch(failure(r.error))
                } else {
                    dispatch(success(r));
                }
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(error.toString());
            }
        );
    }
}

function like(id) {
    function request() { return {type: listingConstants.ANNOUNCEMENT_LIKE } }
    function success(r) { return {type: listingConstants.ANNOUNCEMENT_LIKE_SUCCESS, r} }
    function failure(r) { return {type: listingConstants.ANNOUNCEMENT_LIKE_FAILURE, r} }

    return dispatch => {
        dispatch(request(id));
        listingService.like(id).then(
            r => {
                if (r.error) {
                    dispatch(failure(r.error))
                } else {
                    dispatch(success(r));
                }
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(error.toString());
            }
        );
    }
}

function dislike(id) {
    function request() { return {type: listingConstants.ANNOUNCEMENT_DISLIKE }}
    function success(r) { return {type: listingConstants.ANNOUNCEMENT_DISLIKE_SUCCESS, r} }
    function failure(r) { return {type: listingConstants.ANNOUNCEMENT_DISLIKE_FAILURE, r} }

    return dispatch => {
        dispatch(request(id));
        listingService.dislike(id).then(
            r => {
                if (r.error) {
                    dispatch(failure(r.error))
                } else {
                    dispatch(success(r));
                }
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(error.toString());
            }
        );
    }
}

function getLiked() {
    function request() { return {type: listingConstants.ANNOUNCEMENT_GET_LIKED }}
    function success(r) { return {type: listingConstants.ANNOUNCEMENT_GET_LIKED_SUCCESS, r} }
    function failure(r) { return {type: listingConstants.ANNOUNCEMENT_GET_LIKED_FAILURE, r} }

    return dispatch => {
        dispatch(request());
        listingService.getLiked().then(
            r => {
                if (r.error) {
                    dispatch(failure(r.error))
                } else {
                    dispatch(success(r));
                }
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error(error.toString());
            }
        );
    }
}

function importAnnoncement(url) {
    function request(r) { return {type: listingConstants.ANNOUNCEMENT_IMPORT, r }}
    function success(r) { return {type: listingConstants.ANNOUNCEMENT_IMPORT_SUCCESS, r} }
    function failure(r) { return {type: listingConstants.ANNOUNCEMENT_IMPORT_FAILURE, r} }

    return dispatch => {
        dispatch(request(url));
        listingService.importAnnoncement(url).then(
            r => {
                dispatch(success(r));
            },
            error => {
                dispatch(failure(error.toString()));
                alertActions.error('Не удалось имортировать объявление');
            }
        );
    }

}


export const listingActions = {
    create,
    close,
    get,
    getById,
    getUserListings,
    uploadFile,
    getOffersByBuyer,
    like,
    dislike,
    getLiked,
    importAnnoncement
};
