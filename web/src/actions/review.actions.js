import { reviewConstants } from "../constants";
import { reviewService } from "../services";
import { history } from "../helpers";

function getUserReviews() {
    function request() { return {type: reviewConstants.GET_USER_REVIEW_REQUEST} }
    function success(userReviews) { return {type: reviewConstants.GET_USER_REVIEW_SUCCESS, userReviews} }
    function failure(r) { return {type: reviewConstants.GET_USER_REVIEW_FAILURE, r} }

    return dispatch => {
        dispatch(request());
        reviewService.getReviewMe().then(
            r => {
                dispatch(success(r));
            },
            error => {
                dispatch(failure(error.toString()));
            }
        );
    }
}

function getMyReviews() {
    function request() { return {type: reviewConstants.GET_MY_REVIEW_REQUEST} }
    function success(myReviews) { return {type: reviewConstants.GET_MY_REVIEW_SUCCESS, myReviews} }
    function failure(r) { return {type: reviewConstants.GET_MY_REVIEW_FAILURE, r} }

    return dispatch => {
        dispatch(request());
        reviewService.getReviewI().then(
            r => {
                dispatch(success(r));
            },
            error => {
                dispatch(failure(error.toString()));
            }
        );
    }
}

export const reviewActions = {
    getUserReviews,
    getMyReviews
};
