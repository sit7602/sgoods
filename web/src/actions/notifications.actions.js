import {notificationsConstants, userConstants} from "../constants";
import {alertActions} from "./alert.actions";
import {showMessage} from "../helpers/utils";

const typeEnum = {
    0: 'PURCHASE_OFFER',
    1: 'OFFER_NEW_PRICE',
    2: 'CONFIRMATION_OF_SALES',
    3: 'TRANSFER_CONFIRMATION',
    4: 'SOLD',
    5: 'REJECT'
}

function onMessageReceive(data) {
    function onMessage(data) {
        return {type: notificationsConstants.MESSAGE_RECEIVED, data}
    };

    function setPurchaseMessages(purchaseMessages) {
        return {type: notificationsConstants.MSG_PURCHASE_OFFER, purchaseMessages}
    };

    function setOfferNewMessages(offerNewMessages) {
        return {type: notificationsConstants.MSG_OFFER_NEW_PRICE, offerNewMessages}
    };

    function setConfirmationOfSalesMessages(confirmationOfSalesMessages) {
        return {type: notificationsConstants.MSG_CONFIRMATION_OF_SALES, confirmationOfSalesMessages}
    };

    function setTransferConfirmationMessages(transferConfirmationMessages) {
        return {type: notificationsConstants.MSG_TRANSFER_CONFIRMATION, transferConfirmationMessages}
    };

    function setSoldMessages(soldMessages) {
        return {type: notificationsConstants.MSG_SOLD, soldMessages}
    };

    function setRejectMessages(rejectMessages) {
        return {type: notificationsConstants.MSG_REJECT_MESSAGE, rejectMessages}
    }

    return dispatch => {
        if (!Object.keys(data).includes('type')){
            dispatch(onMessage(data));
        } else {
            const type = typeEnum[data.type];
            switch (type) {
                case 'PURCHASE_OFFER':
                    dispatch(setPurchaseMessages(data));
                    showMessage('У вас новое предложение о покупке', '/account/notifications');
                    break;
                case 'OFFER_NEW_PRICE':
                    dispatch(setOfferNewMessages(data));
                    showMessage('Вам предложили новую цену', '/account/notifications');
                    break;
                case 'CONFIRMATION_OF_SALES':
                    dispatch(setConfirmationOfSalesMessages(data));
                    showMessage('Предложение о покупке подтверждено', '/account/purchases');
                    break;
                case 'TRANSFER_CONFIRMATION':
                    dispatch(setTransferConfirmationMessages(data));
                    showMessage('Предложение о передаче товара', '/account/notifications');
                    break;
                case 'SOLD':
                    dispatch(setSoldMessages(data));
                    showMessage('Сделка успешно совершена', '/account/notifications');
                    break;
                case 'REJECT':
                    dispatch(setRejectMessages(data));
                    showMessage('Предложение отклонено', '/account');
                    break;
                default:
                    dispatch(onMessage(data));
                    break;
            }
        }
    };
}

export const notificationsActions = {
    onMessageReceive
};
