import { alertConstants } from '../constants';
import * as M from "materialize-css";

function success(msg) {
    M.toast({html: msg, classes: 'green'});
    return {type: alertConstants.SUCCESS, msg};
}

function error(msg) {
    M.toast({html: msg, classes: 'red'});
    return {type: alertConstants.ERROR, msg};
}

function clear() {
    return {type: alertConstants.CLEAR};
}

export const alertActions = {
    success,
    error,
    clear
};
