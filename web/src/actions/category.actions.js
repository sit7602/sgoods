import { categoryConstants } from "../constants";
import { categoryService } from "../services";
import { history } from "../helpers";

function get() {
    function request() { return {type: categoryConstants.GET_CATEGORY_REQUEST} }
    function success(categories) { return {type: categoryConstants.GET_CATEGORY_SUCCESS, categories} }
    function failure(r) { return {type: categoryConstants.GET_CATEGORY_FAILURE, r}}

    return dispatch => {
        dispatch(request());
        categoryService.getCategories().then(
            r => {
                dispatch(success(r));
            },
            error => {
                dispatch(failure(error.toString()))
            }
        );
    }
}

function getFieldDescription(id) {
    function request(id) { return {type: categoryConstants.GET_FIELDDESCRIPTION_REQUEST, id} }
    function success(categoryFields) { return {type: categoryConstants.GET_FIELDDESCRIPTION_SUCCESS, categoryFields} }
    function failure(r) { return {type: categoryConstants.GET_FIELDDESCRIPTION_FAILURE, r}}

    return dispatch => {
        dispatch(request(id));
        categoryService.getFieldDescription(id).then(
            r => {
                dispatch(success(r));
            },
            error => {
                dispatch(failure(error.toString()));
            }
        );
    };

}

export const categoryActions = {
    get,
    getFieldDescription
};
