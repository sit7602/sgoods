export type Listing = {
    price: string;
    name: string;
    url: string;
}
export type Seller = {
    id: number;
    firstName: string;
    lastName: string;
    photo: string;
}
export type SellerInfo = {
    hideUserDate: boolean;
    phone: string;
    callTimeStart: null;
    callTimeEnd: null;
    UTC: number;
}
export type AnnouncementPhoto = {
    id: number;
    fileURL: string;
}
export type ProductCategory = {
    parentCategory: number;
    id: number;
    category: string;
}