import { userActions } from "../../actions";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {ReactNode} from "react";
import * as React from "react";
import './style.css';

const url = 'https://avatars.mds.yandex.net/get-pdb/28866/eb88a435-bc51-43af-af93-1ce87213080e/s600';

class PosterPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render(): ReactNode {
        return (
            <div className='content'>
                <div className='row'>
                    <div className='col s7'>
                        <img style={{width: '100%'}} src={url}/>
                    </div>
                    <div className='col s5'>
                        <a className="waves-effect waves-light btn">Связаться с продавцом</a>
                    </div>
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const { authentication } = state;
    const { user } = authentication;
    return user;
}

const actionCreators = {

};

const connectedPosterPage = connect(mapState, actionCreators)(PosterPage);
export {connectedPosterPage as PosterPage };
