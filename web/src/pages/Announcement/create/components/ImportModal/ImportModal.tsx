import * as React from 'react';
import { Modal } from "antd";
import * as Mat from "react-materialize";
import { listingService } from '../../../../../services/listing.service';
import {alertActions} from '../../../../../actions/alert.actions';

type ImportModalProps = {
    closeModal: any;
    visible: boolean;
}

type ImportModalState = {
    url: string;
    loading: boolean;
}

export class ImportModal extends React.Component<ImportModalProps, ImportModalState> {
    constructor(props) {
        super(props);
        this.state = {
            url: '',
            loading: false
        };

        this.validateUrl = this.validateUrl.bind(this);
        this.getAnnouncementData = this.getAnnouncementData.bind(this);
    }

    validateUrl(): boolean {
        const reg = /^(https):\/\/www.avito.ru(\/[a-zA-z0-9\W_]+)(\/[a-zA-z0-9\W_]+)(\/[a-zA-z0-9\W_]+)$/gm;
        return reg.test(this.state.url);
    }

    getAnnouncementData(): void {
        if (this.validateUrl()) {
            this.setState({
                loading: true
            }, function() {
                listingService.importAnnoncement(this.state.url).then(data => {
                    console.log('data from import');
                    console.log(data);
                    this.setState({loading: false});
                    
                    this.props.closeModal(data);
                }).catch(error => {
                    this.setState({loading:false});
                    console.log(error);
                    
                    alertActions.error('Не удалось имортировать объявление');
                    
                });
            });
        }
    }

    render() {
        const { getAnnouncementData } = this;
        const { closeModal, visible  } = this.props;
        const { url, loading } = this.state;
        return (
            <Modal
                title='Импорт объявления'
                centered
                okText='Отправить'
                cancelText='Закрыть'
                onOk={getAnnouncementData}
                onCancel={closeModal}
                visible={visible}
                confirmLoading={loading}
                okButtonProps={{disabled: !this.validateUrl()}}
                cancelButtonProps={{disabled: loading}}
            >
                <Mat.Row>
                    <Mat.Col l={12} s={12}>
                        <Mat.TextInput 
                            m={12}
                            s={12}
                            xl={12}
                            label='Вставьте адрес объявления с avito.ru'
                            value={url}
                            validate
                            error='Неверный адрес объявления'
                            inputClassName={this.validateUrl() ? 'valid' : 'invalid'}
                            onChange={(e) => {
                                this.setState({
                                    url: e.currentTarget.value 
                                });
                            }}
                        />
                    </Mat.Col>
                </Mat.Row>
            </Modal>
        )
    }

}

