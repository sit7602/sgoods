import * as React from "react";
// import 'style';

import * as Mat from "react-materialize";

type DynamicFormProps = {
    data: any[];
    syncState: any;
}

type DynamicFormState = {
    pickedData: any[];
}

const valueTypeMap = {
    '1': 'text',
    '2': 'number',
    '3': 'float',
    '4': 'bool'
};

export class DynamicForm extends React.Component<DynamicFormProps, any> {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
    }

    handleChange(event): void {
        const e = event.currentTarget;
        this.props.syncState(e.id, '' + e.value);
    }

    handleCheckBoxChange(event): void {
        const e = event.currentTarget;
        const value = e.checked ? 'Да' : 'Нет';
        this.props.syncState(e.id, e.value);
    }

    render(): React.ReactNode {
        if (!this.props.data.length) {
            return null
        } else {
            const { handleChange, handleCheckBoxChange } = this;
            return (
                <div className='announcement-create__add-fields-holder'>
                    {
                        this.props.data.map(e => {
                            const type = 'text' // valueTypeMap[e.valueType];

                            if (type === 'text') {
                                return (
                                    <Mat.Col s={4} xl={4}>
                                        <Mat.TextInput id={e.parameter} onChange={handleChange} label={e.parameter} placeholder={e.prompt} />
                                    </Mat.Col>
                                );
                            } else if (type === 'number') {
                                return (
                                    <Mat.Col s={4} xl={4}>
                                        <Mat.TextInput id={e.parameter} onChange={handleChange} label={e.parameter} placeholder={e.prompt} type='number' />
                                    </Mat.Col>
                                );
                            } else if (type === 'float') {
                                return (
                                    <Mat.Col s={4} xl={4}>
                                        <Mat.TextInput id={e.parameter} onChange={handleChange} label={e.parameter} placeholder={e.prompt} type='number' />
                                    </Mat.Col>
                                );
                            } else {
                                return (
                                    <Mat.Col s={4} xl={4}>
                                        <Mat.Checkbox id={e.parameter} onChange={handleCheckBoxChange} label={e.parameter} value={e.parameter} />
                                    </Mat.Col>
                                );
                            }
                        })
                    }
                </div>
            )
        }
    }
}
