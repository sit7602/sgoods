import { Map, Placemark, SearchControl, YMaps } from "react-yandex-maps";
import * as React from "react";


const API_KEY = '137ca00c-15ed-4956-8f82-f3911b5bfb90';

export const AddressPicker = (props) => {
    const {onClick, onLoad, onLoad1, onResultShow} = props;
    const {placeMarkGeometry, selectedAddrString, coordinate1, coordinate2} = props;
    return (
        <YMaps
            enterprise
            query={{ ns: 'use-load-option', apikey: "137ca00c-15ed-4956-8f82-f3911b5bfb90", load: 'Map,Placemark,control.ZoomControl,control.FullscreenControl,geoObject.addon.balloon,geocode' }}
        >
            <Map
                defaultState={{center: [55.75, 37.57], zoom: 9}}
                onClick={onClick}
                onLoad={onLoad}
                width='100%'
                modules={['geocode']}
            >
                <SearchControl
                    options={{float: 'right', noPlacemark: false}}
                    onLoad={onLoad1}
                    onResultShow={onResultShow}
                    onResultselect={onResultShow}
                    onSubmit={onResultShow}
                />
                <Placemark
                    defaultGeometry={[55.75, 37.57]}
                    geometry={[coordinate1, coordinate2]}
                    properties={{ balloonContentBody: selectedAddrString }}
                />
            </Map>
        </YMaps>
    )
}