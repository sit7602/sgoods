import { YMaps, Map, Placemark, SearchControl } from "react-yandex-maps";
import { AddressPicker } from "./components/AddressPicker/AddressPicker";
import { alertActions, categoryActions, listingActions } from "../../../actions";
import { ReactNode, useRef } from "react";
import * as M from "materialize-css";
import * as Mat from "react-materialize";
import { connect } from "react-redux";
import * as moment from "moment";
import * as React from "react";
import { ImportModal } from "./components/ImportModal";
import { TreeSelect } from "antd";
import "./style.css";
import { parse } from "@typescript-eslint/parser";
import { showMessage } from "../../../helpers/utils";
import { DynamicForm } from './components/DynamicForm';
const { TreeNode } = TreeSelect;

type AnnouncementCreatePageProps = {
    importAnnouncement: any;
    category: any;
    categoryFields: any[];
    files: any[];
    create: Function;
    getCategories: Function;
    getCategoryFields: Function;
    uploadFile: Function;
};

type Coordinates = {
    lat: number;
    lng: number;
}

type AnnouncementCreatePageState = {
    listingPhotos: any;
    listingName: string;
    listingPrice: number;
    listingDescription: string;
    listingCategory: number;
    listingLocation: number;
    listingMinPrice: string;
    listingBargain: boolean;
    photoNames: string[];
    coordinates: Coordinates;
    coordinate1: number;
    coordinate2: number;
    yMapInstance: any;
    selectedAddrString: string;
    categoryFields: any[];
    importModalVisible: boolean;
    isImport: boolean;
};


class AnnouncementCreatePage extends React.Component<AnnouncementCreatePageProps, AnnouncementCreatePageState> {
    public map: React.Ref<any>;
    public searchRef: React.Ref<any>;

    constructor(props) {
        super(props);
        this.state = {
            listingPhotos: [],
            listingName: '',
            listingPrice: 0,
            listingDescription: '',
            listingCategory: -1,
            listingLocation: -1,
            listingBargain: false,
            listingMinPrice: '0',
            photoNames: [],
            coordinates: null,
            yMapInstance: null,
            selectedAddrString: '',
            coordinate1: null,
            coordinate2: null,
            categoryFields: [],
            importModalVisible: false,
            isImport: false,
        };

        this.previewImages = this.previewImages.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.mergeArrays = this.mergeArrays.bind(this);

        this.props.getCategories();
    }

    componentDidMount(): void {
        this.initSelect();
    }

    componentWillReceiveProps(nextProps: Readonly<AnnouncementCreatePageProps>, nextContext: AnnouncementCreatePageState): void {
        console.log(nextProps.files);
        if (nextProps.files.length === 0) {
            this.setState({
                listingPhotos: []
            });
        }
        
    }

    componentDidUpdate(prevProps: Readonly<AnnouncementCreatePageProps>, prevState: Readonly<AnnouncementCreatePageState>, snapshot?: any): void {
        this.initSelect();
    }

    initSelect = (): void => {
        const elems = document.querySelectorAll('.select');
        M.FormSelect.init(elems, {});
    };

    previewImages = (): ReactNode => {
        const { listingPhotos } = this.state;
        try {
            const objUrls = Array.from(listingPhotos).map(e => URL.createObjectURL(e));
            return (
                <div className="listing-photos__holder">
                    {
                        objUrls.map((e, i) => (
                            <Mat.MediaBox id={'img_' + i} key={'img_key_' + i}>
                                <img key={i} src={e} />
                            </Mat.MediaBox>
                        )
                        )}
                </div>
            );
            
        } catch (error) {
            const {listingPhotos} = this.state;
            return (
                <div className="listing-photos__holder">
                    {
                        listingPhotos.map((e, i) => (
                            <Mat.MediaBox id={'img_' + i} key={'img_key_' + i}>
                                <img key={i} src={e} />
                            </Mat.MediaBox>
                        )
                        )}
                </div>
            );
        }
        
    }

    handleChange = (event): void => {
        if (event.target.files.length < 6) {
            this.setState({
                listingPhotos: event.target.files
            });
            this.props.uploadFile(event.target.files)
        } else {
            alertActions.error('Максимальное количество фотографий - 5');
        }
    }

    onCategoryChange = (e): void => {
        this.setState({ listingCategory: parseInt(e.currentTarget.value) })
    }

    onResultShow = (e): void => {
        console.log(e);
        // console.log(e.originalEvent.get('coords'));
        console.log(e.get('coords'));
        // console.log(e.get('type'));
        // console.log(e.get('address'));

        // console.log(e.originalEvent.get('coords'));
        // console.log(e.originalEvent.get('type'));
        // console.log(e.originalEvent.get('address'));
        // this.onMapClick(e);
    }

    handleFieldChange = (event): void => {
        const name = event.currentTarget.name;
        if (name === 'name') {
            const newValue = event.currentTarget.value
            this.setState({ 'listingName': newValue })
        }
        if (name === 'price') {
            this.setState({ 'listingPrice': event.currentTarget.value })
            // if (/^[0-9]+$/.test(event.currentTarget.value)) {
            //     console.log(event.currentTarget.value)
            //     this.setState({'listingPrice': event.currentTarget.value.replace(',', '')})
            // } else {
            //     this.setState({'listingPrice': Math.abs(event.currentTarget.value) || 0})
            // }
        }
        if (name === 'minPrice') {
            if (/^[0-9]+$/.test(event.currentTarget.value) && parseInt(event.currentTarget.value) < this.state.listingPrice) {
                this.setState({
                    listingMinPrice: '' + Math.abs(parseInt(event.currentTarget.value))
                })
            } else {
                this.setState({ listingMinPrice: '0' })
            }
        }
        if (name === 'description') {
            this.setState({ 'listingDescription': event.currentTarget.value })
        }
        if (name === 'category') {
            this.setState({ 'listingCategory': event.currentTarget.value })
        }
        if (name === 'location') {
            this.setState({ 'listingLocation': event.currentTarget.value })
        }
    }

    saveListing = async () => {
        const {
            listingName,
            listingPrice,
            listingDescription,
            listingCategory,
            photoNames,
            listingBargain,
            listingMinPrice,
            coordinates,
            categoryFields
        } = this.state

        const check = this.isValid();

        if (check[0]) {
            let fullDescription = listingDescription;
            if (categoryFields.length) {
                fullDescription += '\n';
                categoryFields.forEach(e => {
                    fullDescription += `\n${e.label}: ${e.value}`
                });
            }
            const photos = this.props.files.length > 0 ? this.props.files : [this.state.listingPhotos[0].split('/').pop()]
            const data = {
                "name": listingName,
                "description": fullDescription,
                // @ts-ignore
                "price": Math.round(parseInt(listingPrice)),
                "type": 0,
                "photoPaths": photos,
                "categoryId": listingCategory,
                "bargain": listingBargain,
                "minPrice": parseInt(listingMinPrice),
                "geoPoint": {
                    "lat": coordinates.lat,
                    "lon": coordinates.lng
                },
            }

            this.props.create(data);
        } else {
            showMessage(check[1]);
        }
    }

    onAddressDecoded = (res) => {
        const firstGeoObject = res.geoObjects.get(0);
        const iconCaption = [
            firstGeoObject.getLocalities().length ?
                firstGeoObject.getLocalities()
                : firstGeoObject.getAdministrativeAreas(), firstGeoObject.getThoroughfare()
            || firstGeoObject.getPremise()].filter(Boolean).join(', ');
        const balloonContent = firstGeoObject.getAddressLine()
        const coordinates = res.geoObjects.get(0).geometry.getCoordinates();
        this.setState({ selectedAddrString: balloonContent })
    }

    onMapClick = (e): void => {
        const coords = e.get('coords');
        console.log(coords);
        const [lat, lng] = coords;
        const { yMapInstance } = this.state;

        if (yMapInstance !== null) {
            yMapInstance.geocode(coords).then(res => {
                this.onAddressDecoded(res);
            });
        }

        this.setState({ coordinates: { lat, lng }, coordinate1: lat, coordinate2: lng });
    };

    geocode(yMapInstance): void {
        console.log(yMapInstance);

        this.setState({ yMapInstance })
    }

    isValid = () => {
        const cases = [
            {
                name: 'Загрузите фотографии',
                test: (this.props.files.length > 0 && this.state.listingPhotos.length > 0)  || 
                    (this.state.listingPhotos.length > 0 && this.state.isImport)
            },
            {
                name: 'Выберите категорию',
                test: this.state.listingCategory !== -1
            },
            {
                name: 'Задайте местоположение',
                test: this.state.coordinates !== null
            },
            {
                name: 'Заполните название объявления',
                test: this.state.listingName.length
            },
            {
                name: 'Задайте корректную цену',
                // @ts-ignore
                test: !isNaN(parseInt(this.state.listingPrice)) && parseInt(this.state.listingPrice) > 0
            },
            {
                name: 'Длина описания превышает 1000 символов',
                test: this.state.listingDescription.length <= 1000
            }
        ]
        let allOk = true;
        let reason = null;
        let checkNext = true;
        cases.forEach(c => {
            if (checkNext && !c.test) {
                allOk = false;
                reason = c.name;
                checkNext = false;
            }
        })
        return [allOk, reason]
    }

    onLoad1 = (searchControl) => {
        console.log('SEARCH');
        console.log(searchControl);
        const searchControl1 = searchControl.control.SearchControl;
        console.log(searchControl1);
        // searchControl1.events.add('submit', function () {
        //     console.log('request: ' + searchControl.getRequestString());
        // }, this);

        // searchControl.events.add('submit', function () {
        //     console.log('SEARCH');
        //     console.log('request: ' + searchControl.getRequestString());
        // }, this);
    }

    renderTree = (obj: any): ReactNode => {
        return obj.map(e => {
            if (!e.childrenCategory.length) {
                return (
                    <TreeNode key={e.id} value={e.id} title={e.category} />
                );
            } else {
                return (
                    <TreeNode key={e.id} value={e.id} title={e.category}>
                        {
                            this.renderTree(e.childrenCategory)
                        }
                    </TreeNode>

                );
            }
        })
    }

    mergeArrays(origArr: any[], updatingArr: any[]): any[] {
        if (!origArr.filter(e => e.label === updatingArr[0].label).length) {
            origArr.push(updatingArr[0])
        } else {
            for (var i = 0, l = origArr.length; i < l; i++) {
                for (var j = 0, ll = updatingArr.length; j < ll; j++) {
                    if (origArr[i].label === updatingArr[j].label) {
                        origArr.splice(i, 1, updatingArr[j]);
                        break;
                    }
                }
            }
        }

        return origArr;
    }

    getDynamicFormData = (label: string, value: string): void => {
        let { categoryFields } = this.state;
        categoryFields = this.mergeArrays(categoryFields, [{ label, value }]);
        this.setState({
            categoryFields
        });
    }

    closeImportModal = (data: any): void => {
        if (!data.error && Object.keys(data).length > 0) {
            const photo = `${process.env.API_URL}${data.Photo}`
            this.setState({
                listingDescription: data.Description,
                listingName: data.Title,
                listingPrice: data.Price,
                listingPhotos: [photo],
                isImport: true
            }, function() {
                this.setState({importModalVisible: false});
            })
        } else {
            this.setState({
                importModalVisible: false
            });
        }
        
    }

    render(): ReactNode {
        const {
            listingName,
            listingPrice,
            listingDescription,
            coordinates,
            listingLocation,
            importModalVisible
        } = this.state

        const { category } = this.props;

        const {
            onCategoryChange,
            previewImages,
            handleChange,
            handleFieldChange,
            saveListing,
            closeImportModal
        } = this

        const isValid = this.props.files.length
            && this.state.listingCategory !== -1
            && coordinates
            && listingName.length
            && listingPrice > 0;

        return (
            <div className="content content__announcement-create center-align">
                <div className="row">
                    <div className="col s12">
                        {previewImages()}
                        <p>
                            <input
                                name="photo"
                                id="sdf"
                                className="validate fileinpu1t"
                                type="file" multiple={true}
                                onChange={handleChange}
                                style={{ display: 'none' }}
                            />
                            <label
                                className='btn red'
                                htmlFor="sdf"
                                style={{ marginBottom: 0 }}
                            >
                                <i className="material-icons left">add</i>Загрузить изображения
                            </label>
                            {
                                this.state.listingPhotos.length ?
                                
                                <button style={{marginLeft: 20}} onClick={() => {
                                    this.setState({
                                        listingPhotos: [], 
                                        isImport: false
                                    })
                                }} className='btn green '> <i className="material-icons left">delete</i>Удалить фотографии</button>
                                
                                
                                : null
                            }
                            
                        </p>
                    </div>
                    <div className="col xl6 m6 s12">
                        <p>
                            <input
                                value={listingName}
                                onChange={handleFieldChange}
                                type="text"
                                name="name"
                                className="validate"
                                placeholder="Введите название"
                            />
                            <label htmlFor="name"><span style={{ color: 'red' }}>* </span>Название</label>
                        </p>
                        <p>
                            <input
                                value={listingPrice}
                                onInput={handleFieldChange}
                                type="number"
                                name="price"
                                className="validate"
                                placeholder="Стоимость, ₽"
                            />
                            <label htmlFor="price"><span style={{ color: 'red' }}>* </span>Стоимость</label>
                        </p>
                    </div>
                    <div className="col xl6 m6 s12">
                        <p>
                            <TreeSelect showSearch
                                allowClear
                                treeDefaultExpandAll
                                style={{ width: '100%', marginTop: 16 }}
                                searchPlaceholder='Выберите категорию'
                                filterTreeNode={(value, treeNode) => {
                                    const title = treeNode.title + "";
                                    return title.toLocaleUpperCase().indexOf(value.toLocaleUpperCase()) !== -1;
                                }}
                                value={this.state.listingCategory}
                                onChange={(value) => {
                                    this.setState({
                                        listingCategory: value || -1
                                    });
                                    if (value) {
                                        this.props.getCategoryFields(value);
                                    }

                                }}
                                treeDefaultExpandedKeys={[]}
                            >
                                <TreeNode key={-1} value={-1} title='Выберите категорию' />
                                {
                                    this.props.category.map(e => {
                                        if (!e.childrenCategory.length) {
                                            return (
                                                <TreeNode key={e.id} value={e.id} title={e.category} />
                                            );
                                        } else {
                                            return (
                                                <TreeNode key={e.id} value={e.id} title={e.category}>
                                                    {
                                                        this.renderTree(e.childrenCategory)
                                                    }
                                                </TreeNode>

                                            )
                                        }
                                    })
                                }
                            </TreeSelect>
                        </p>
                        <label htmlFor="category"><span style={{ color: 'red' }}>* </span>Категория</label>
                        <p style={{marginTop: '.4em'}}>
                            <input
                                value={listingDescription}
                                onChange={handleFieldChange}
                                type="text"
                                name="description"
                                className="validate"
                                placeholder="Введите описание (до 1000 символов)"
                            />
                            <label htmlFor="description">Описание</label>
                        </p>

                    </div>
                    <div className="col xl6 m6 s12">
                        <div className="col s4 allow-torg-col">
                            <p style={{ padding: '10px' }} onClick={event => {
                                this.setState({
                                    listingBargain: !this.state.listingBargain
                                });
                            }}>
                                <input type='checkbox'
                                    checked={this.state.listingBargain}
                                    onClick={(e => {
                                        this.setState({
                                            listingBargain: !this.state.listingBargain
                                        });
                                    })} />
                                <span>Разрешить торг</span>
                            </p>
                        </div>

                        <div className="col xl8 m8 s12 ">
                            <p>
                                <input name='minPrice' disabled={!this.state.listingBargain} placeholder='Минимальная сумма продажи'
                                    className='validate' value={this.state.listingMinPrice} onChange={handleFieldChange} />
                                <label htmlFor="minPrice">Минимальная сумма продажи</label>
                            </p>
                        </div>
                    </div>
                    <div className="col s12">
                        <AddressPicker
                            onClick={this.onMapClick.bind(this)}
                            onLoad={this.geocode.bind(this)}
                            onLoad1={this.onLoad1.bind(this)}
                            onResultShow={this.onResultShow.bind(this)}
                            placemarkGeometry={this.state.coordinates ? [this.state.coordinates.lat, this.state.coordinates.lng] : [55.75, 37.57]}
                            selectedAddrString={this.state.selectedAddrString}
                            coordinate1={this.state.coordinate1}
                            coordinate2={this.state.coordinate2}
                        />
                        <p>
                            <input disabled={true} type="text" name='geo' value={this.state.selectedAddrString} />
                            <label htmlFor="geo"><span style={{ color: 'red' }}>* </span>Адрес</label>
                        </p>
                    </div>
                    <div className="col s12">
                        <DynamicForm
                            data={this.props.categoryFields}
                            syncState={this.getDynamicFormData}
                        />
                    </div>
                    <div className="col s12">
                        <button
                            className="btn waves-light"
                            onClick={saveListing}
                        >Опубликовать
                        </button>
                        <button
                            style={{marginLeft: 20}}
                            className='btn green waves-light'
                            onClick={() =>{ 
                                this.setState({
                                    importModalVisible: true,
                                });
                            }}
                            >Импорт объявления</button>
                    </div>
                </div>
                <ImportModal visible={importModalVisible} closeModal={closeImportModal} />
            </div>
        );
    }
}

function mapState(state) {
    const { files } = state.listings;
    const { categories: category, categoryFields } = state.category;
    return { files, category, categoryFields };
}

const actionCreators = {
    create: listingActions.create,
    importAnnouncement: listingActions.importAnnoncement,
    getCategories: categoryActions.get,
    getCategoryFields: categoryActions.getFieldDescription,
    uploadFile: listingActions.uploadFile
};

const connectedRegistrationPage = connect(mapState, actionCreators)(AnnouncementCreatePage);
export { connectedRegistrationPage as AnnouncementCreatePage };
