import { ARROW_URL, formatter, NOT_IMPLEMENTED_PH } from "../../constants/img.constants";
import { UserNotifications } from "./components/UserNotifications";
import { DealConfirmation } from "./components/DealConfirmation";
import { UserReviews } from "./components/UserReviews";
import { listingActions, userActions } from "../../actions";
import { reviewActions } from "../../actions/review.actions";
import { UserPurchases } from "./components/UserPurchases";
import { Link, NavLink, Route } from 'react-router-dom';
import { showMessage } from "../../helpers/utils";
import { ReactNode, ReactElement } from "react";
import { AnnouncementList } from "../../components/AnnouncementList";
import StarRatings from "react-star-ratings";
import { UserRatingModal } from "../../components/UserRatingModal";
import { listingService, userService } from "../../services";
import { Listing } from "../../types/types";
import SockJsClient from "react-stomp";
import * as M from 'materialize-css';
import * as Mat from "react-materialize";
import { connect } from 'react-redux';
import * as React from "react";
import './style.css';
import {HelpBox} from "./components/HelpBox/HelpBox";
import CheckList from "./components/CheckList/CheckList";


type UserAccountProps = {
    listings: any;
    listingOffers: any;
    getUserListings: any;
    getOffersByBuyer: any;
    submitOffer: any;
    cancelOffer: any;
    completeOffer: any;
    user: any;
    purchaseMessages: any[];
    offerNewMessages: any[];
    confirmationOfSalesMessages: any[];
    transferConfirmationMessages: any[];
    getMyReviews: any;
    getUserReviews: any;
    soldMessages: any[];
    rejectMessages: any[];
    getLiked: any;
    sendReport: any;
    uploadFile: any;
    files: any[];
    userReviews: any[];
    myReviews: any[];
};

type UserAccountState = {
    listings: Listing[];
    clientConnected: boolean;
    messages: any;
    userCards: any;
    likedListings: any;
    userLesson: number;
    bugAttachments: any[];
    bugTitle: string;
    bugDescription: string;
    bugModalVisible: boolean;
    userInfo: any;
    userRatingModalVisible: boolean;
    UserRatingModalOfferId: number;
};

class UserAccountPage extends React.Component<UserAccountProps, UserAccountState> {
    constructor(props) {
        super(props);
        this.state = {
            listings: null,
            likedListings: null,
            clientConnected: false,
            messages: [],
            userCards: [],
            userInfo: {},
            userLesson: localStorage.getItem('lessonShown1') === null ? 0 : 10,
            bugAttachments: [],
            bugTitle: "",
            bugDescription: "",
            bugModalVisible: false,
            userRatingModalVisible: false,
            UserRatingModalOfferId: -1
        };

        this.init = this.init.bind(this);
        this.initCollapsible = this.initCollapsible.bind(this);
    }

    init(): void {
        this.props.getUserListings();
        this.props.getLiked();
        this.props.getOffersByBuyer();
        this.props.getUserReviews();
        this.props.getMyReviews();

        userService.getUserCards().then(r => {
            this.setState({ userCards: r })
        })
        listingService.getLiked().then(r => {
            this.setState({ likedListings: r })
        })
        userService.getInfo().then(r => {
            console.log(r);
            this.setState({userInfo: r})
        })

    }

    componentDidMount(): void {
        this.init();
        this.initCollapsible();



        // console.log(localStorage.getItem('lessonShown1'));
        localStorage.setItem('lessonShown1', '1')
    }

    componentDidUpdate(prevProps: Readonly<UserAccountProps>, prevState: Readonly<UserAccountState>, snapshot?: any): void {
        this.initCollapsible();
    }

    initCollapsible(): void {
        const elem = document.querySelector('.collapsible');
        M.Collapsible.init(elem, {})
    }

    componentWillReceiveProps(nextProps: Readonly<UserAccountProps>, nextContext: any): void {
        const {
            purchaseMessages: _purchaseMessages,
            offerNewMessages: _offerNewMessages,
            confirmationOfSalesMessages: _confirmationOfSalesMessages,
            transferConfirmationMessages: _transferConfirmationMessages,
            soldMessages: _soldMessages,
            rejectMessages: _rejectMessages
        } = nextProps;
        const {
            purchaseMessages,
            offerNewMessages,
            confirmationOfSalesMessages,
            transferConfirmationMessages,
            soldMessages,
            rejectMessages
        } = this.props;
        if (purchaseMessages.length !== _purchaseMessages.length
            || offerNewMessages.length !== _offerNewMessages.length
            || confirmationOfSalesMessages.length !== _confirmationOfSalesMessages.length
            || transferConfirmationMessages.length !== _transferConfirmationMessages.length
            || soldMessages.length !== _soldMessages.length
            || rejectMessages.length !== _rejectMessages.length) {
            this.init();
        }
        // Show rating modal a
        if (soldMessages.length !== _soldMessages.length) {
            console.group('SOLD MESSAGE');
            
            let s = _soldMessages[_soldMessages.length - 1].data;
            console.log(s);
            if (typeof(s) === 'string') {
                s = JSON.parse(s)
            }
            console.log(s);
            console.groupEnd();
            this.setState({
                UserRatingModalOfferId: s.offer.id,
                userRatingModalVisible: true
            });
        }
    }

    onOfferSubmit = (offerId, announcementId): void => {
        this.props.submitOffer({
            "offer_id": offerId,
            "announcement_id": announcementId
        });
    }

    onCancel = (offerId, announcementId): void => {
        this.props.cancelOffer({
            "offer_id": offerId,
            "announcement_id": announcementId
        });
    }

    onOfferComplete = (offerId): void => {
        this.props.completeOffer({
            "offer_id": offerId
        });
        const { getUserListings, getOffersByBuyer } = this.props;
        setTimeout(function () {
            getUserListings();
            getOffersByBuyer();
        }, 1000)
    }

    MsgCount = (messages): ReactElement => {
        return (
            <span data-badge-caption=''
                className={messages.length ? "new badge blue lighten-1" : "badge"}
            >
                {messages.length ? '+' + messages.length : 0}
            </span>
        )
    }

    UserAnnouncements = ({ listings }): ReactElement => {
        return (
            <div className='user-announcements'>
                <div className="row">
                    <div className="col s12 center-align">
                        <NavLink className='hoverable light-blue darken-1 btn waves-effect waves-light'
                            to='/listing/edit'>
                            <i className="material-icons left">add</i>Создать объявление
                        </NavLink>
                    </div>
                </div>
                <div className='row' style={{ padding: '10px' }}>
                    <div className="row">
                        <AnnouncementList listings={listings} />
                    </div>
                </div>
            </div>
        );
    };

    UserBalance = ({ userCards, userEquity }): ReactElement => {

        return (
            <div className='balance-info col s12'>
                <div className='balance-info__title'>
                    Баланс на всех картах: <span>{formatter.format(userEquity)}</span> ₽
                </div>
                <ul className='collapsible popout'>
                    {
                        userCards.map((c, i) => (
                            <li key={i}>
                                <div className="collapsible-header"><i className="material-icons">credit_card</i> Карта {c.cardNumber} № {i + 1}</div>
                                <div className='collapsible-body'>
                                    <span>Баланс:</span>
                                    <span className='balance-info__val'>{formatter.format(c.balance)} ₽</span><br />
                                    <span>Зарезервировано:</span>
                                    <span className='balance-info__val'>{formatter.format(c.reserved)} ₽</span>
                                </div>
                            </li>
                        )
                        )
                    }
                </ul>
            </div>
        );
    }

    lessonBG = (): ReactElement => {
        const hints = [
            {
                div: (
                    <div className='lesson-content lesson-content-1'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке находятся ваши объявления.</p>
                    </div>
                ),
                link: '/account/announcements'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-2'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке находятся объявления, помеченные вами как избранные.</p>
                    </div>
                ),
                link: '/account/likedAnnouncements'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-3'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке хранится история сделок с вашим участием.</p>
                    </div>
                ),
                link: '/account/purchases'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-4'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке находятся ваши отзывы о других пользователях, а также отзывы, оставленные о вас.</p>
                    </div>
                ),
                link: '/account/reviews'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-5'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке хранятся переписки с пользователями сервиса.</p>
                    </div>
                ),
                link: '/account/messages'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-6'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке находятся запросы на покупку, ожидающие подтверждения. Запросы появляются, когда кто-то резервирует выставленный вами товар.</p>
                    </div>
                ),
                link: '/account/notifications'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-7'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке находятся запросы на подтверждение передачи товара. Запросы появляются после того, как покупатель подтвердил получение товара.</p>
                    </div>
                ),
                link: '/account/deal_confirmation'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-8'>
                        <img src={ARROW_URL} />
                        <p>На этой вкладке находится информация о банковских картах, привязанных к вашему аккаунту.</p>
                    </div>
                ),
                link: '/account/balance'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-9'>
                        <img src={ARROW_URL} />
                        <p>Нажмите сюда для просмотра информации о функционале блоков личного кабинета.</p>
                    </div>
                ),
                link: '#/account/help'
            },
            {
                div: (
                    <div className='lesson-content lesson-content-10'>
                        <img src={ARROW_URL} />
                        <p>Если у вас возникли проблемы при использовании сервиса, вы можете отправить отзыв о некорректной работе.</p>
                    </div>
                ),
                link: '#/account/send_bug'
            },
        ]
        const nextHint = this.state.userLesson < 9 ? hints[this.state.userLesson + 1] : hints[0];
        const nextLink = nextHint.link;
        if (this.state.userLesson < 10) {
            return (
                <div className='lesson-bg'>
                    <div className='close' onClick={() => {
                        this.setState({ userLesson: 10 })
                    }}><i className='material-icons'>close</i></div>
                    {hints[this.state.userLesson].div}
                    <div className='OK'>
                        <NavLink onClick={() => {
                            this.setState({ userLesson: this.state.userLesson + 1 })
                        }}
                            to={nextLink}
                            className="btn blue"
                        >
                            Дальше
                        </NavLink>
                    </div>
                </div>
            )
        }
        return null
    }

    toggleMenuVisibility = (): void => {
        document.querySelector('.left__menu').classList.toggle('left__menu--active');
        document.querySelector('.content__blur').classList.toggle('content__blur--active');
        document.querySelector('.left__menu-button').classList.toggle('left__menu-button--active');
    }

    uploadFiles = (event): void => {
        if (event.target.files.length) {
            this.props.uploadFile(event.target.files);
        }
    }

    sendBugReport = (): void => {
        const { bugTitle, bugDescription } = this.state;
        if (bugTitle) {
            const reportData = JSON.stringify({
                title: bugTitle,
                description: bugDescription,
                attachments: this.props.files
            });

            this.props.sendReport(reportData);
        }

    }

    closeUserRatingModal = (): void => {
        this.setState({
            userRatingModalVisible: false,
        });
    }

    render(): ReactNode {
        const { MsgCount, UserAnnouncements, UserBalance, closeUserRatingModal } = this;
        const { setState, onOfferSubmit, onOfferComplete, onCancel } = this;
        const { listings, user, listingOffers, transferConfirmationMessages, purchaseMessages, userReviews, myReviews } = this.props;
        const { userCards, likedListings, userRatingModalVisible, UserRatingModalOfferId } = this.state;
        const USER_EQUITY = userCards && userCards.length ? userCards.map(e => e.balance).reduce((s, v) => s + v) : 0;
        const rating = 'user' in this.state.userInfo ? this.state.userInfo.user.rating : 0;
        return (
            <div className='main-container'>
                {this.lessonBG()}
                <div className="left__menu-button">
                    <button onClick={this.toggleMenuVisibility} className="btn-floating btn-large red lighten-1">
                        <i className="material-icons">menu</i>
                    </button>
                </div>
                <div onClick={this.toggleMenuVisibility} className="content__blur"></div>
                <div className='z-depth-3 left__menu light-blue lighten-3'>
                    <div className='user-info'>
                        <div className='user__rating'>
                            <div style={{ display: 'flex' }}>
                                <StarRatings
                                    rating={rating}
                                    starRatedColor="blue"
                                    numberOfStars={5}
                                    starDimension="30px"
                                    starSpacing="3px"
                                    name='rating'
                                />
                            </div>
                        </div>
                        <div className='user__nav'>
                            <NavLink activeClassName='is-active' to="/account/announcements"
                                className={this.state.userLesson !== 0 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке находятся ваши объявления.'/>
                                Мои объявления
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to="/account/likedAnnouncements"
                                className={this.state.userLesson !== 1 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке находятся объявления, помеченные вами как избранные.'/>
                                Избранные объявления
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to="/account/purchases"
                                className={this.state.userLesson !== 2 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке хранится история сделок с вашим участием.'/>
                                Мои покупки
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to="/account/reviews"
                                className={this.state.userLesson !== 3 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке находятся ваши отзывы о других пользователях, а также отзывы, оставленные о вас.'/>
                                Мои отзывы
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to='/account/messages'
                                className={this.state.userLesson !== 4 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке хранятся переписки с пользователями сервиса.'/>
                                Cообщения
                                <span data-badge-caption='' className='badge'>0</span>
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to='/account/notifications'
                                className={this.state.userLesson !== 5 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке находятся запросы на покупку, ожидающие подтверждения. Запросы появляются, когда кто-то резервирует выставленный вами товар.'/>
                                Уведомления {MsgCount(purchaseMessages)}
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to='/account/deal_confirmation'
                                className={this.state.userLesson !== 6 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке находятся запросы на подтверждение передачи товара. Запросы появляются после того, как покупатель подтвердил получение товара.'/>
                                Подтверждение продажи {MsgCount(transferConfirmationMessages)}
                            </NavLink>
                            <NavLink activeClassName='is-active'
                                to='/account/balance'
                                className={this.state.userLesson !== 7 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                            >
                                <HelpBox message='На этой вкладке находится информация о банковских картах, привязанных к вашему аккаунту.'/>
                                Кошелек <span className='graytext'>{formatter.format(USER_EQUITY)} ₽</span>
                            </NavLink>
                            <a
                                className={this.state.userLesson !== 8 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                                onClick={() => { this.setState({ userLesson: 0 }) }}
                            >
                                <HelpBox message='Нажмите сюда для просмотра информации о функционале блоков личного кабинета.'/>
                                <i className='material-icons'>help</i> Помощь
                            </a>
                            <a
                                className={this.state.userLesson !== 9 ? "collapsible-header waves-effect" : "collapsible-header waves-effect lesson--active"}
                                onClick={() => { this.setState({
                                    bugModalVisible: true
                                }); }}
                            >
                                <HelpBox message='Если у вас возникли проблемы при использовании сервиса, вы можете отправить отзыв о некорректной работе.'/>
                                <i className="material-icons">bug_report</i> Отчет об ошибке
                            </a>
                            <div className={this.state.bugModalVisible ? "user-account__modal-bg user-account__modal-bg--active" : "user-account__modal-bg"} onClick={() => {
                                this.setState({
                                    bugModalVisible: false,
                                });
                            }}></div>
                            <div className={this.state.bugModalVisible ? "user-account__modal-container user-account__modal-container--active" : "user-account__modal-container"}>
                                <div className="user-account__modal-title">
                                    Отправка отчета об ошибке
                                </div>
                                <div className="row">
                                    <div className="col s12 center input-field">
                                        <input name='bugTitle' required type="text" className={this.state.bugTitle.length ?
                                            'validate' : 'validate invalid'}
                                            value={this.state.bugTitle} onChange={(e) => {
                                                this.setState({
                                                    bugTitle: e.currentTarget.value
                                                })
                                            }} />
                                        <label htmlFor="bugTitle">Краткое описание</label>
                                        <span className="helper-text" data-error="Обязательное поле"></span>
                                    </div>
                                    <div className="col s12 center input-field">
                                        <input name='bugDescription' type="text" className='validate'
                                            value={this.state.bugDescription} onChange={(e) => {
                                                this.setState({
                                                    bugDescription: e.currentTarget.value
                                                })
                                            }} />
                                        <label htmlFor="bugDescription">Описание проблемы</label>
                                    </div>
                                    <div className="col s12 center">
                                        <input
                                            name="photo"
                                            id="sdf"
                                            className="validate"
                                            type="file" multiple={true}
                                            onChange={this.uploadFiles}
                                            style={{ display: 'none' }}
                                        />
                                        <label className='btn red' htmlFor="sdf">
                                            <i className="material-icons left">cloud_upload</i>Загрузить изображения
                                        </label>

                                        <span hidden={this.props.files.length === 0}
                                            style={{ paddingLeft: 12 }}
                                        >Файлов загружено: {this.props.files.length}
                                        </span>
                                    </div>

                                </div>
                                <div className="user-account__modal-btn-container" >
                                    <Mat.Button disabled={this.state.bugTitle.length === 0} flat 
                                     waves="green" onClick={() => {
                                        this.sendBugReport();
                                        this.setState({
                                            bugModalVisible: false
                                        })
                                    }} > Отправить
                                    </Mat.Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="content user-account--content">
                    <Route exact path='/account'>
                        <UserAnnouncements listings={listings} />
                    </Route>
                    <Route exact path='/account/announcements'>
                        <UserAnnouncements listings={listings} />
                    </Route>
                    <Route path='/account/likedAnnouncements'>
                        <AnnouncementList listings={likedListings} />
                    </Route>
                    <Route path='/account/purchases'>
                        <UserPurchases offers={listingOffers} onSubmit={onOfferComplete} />
                    </Route>
                    <Route path='/account/reviews'>
                        <UserReviews userReviews={userReviews} myReviews={myReviews} />
                    </Route>
                    <Route path='/account/messages'>
                        <img className='not-implemented-ph' src={NOT_IMPLEMENTED_PH} />
                    </Route>
                    <Route path='/account/notifications'>
                        <UserNotifications
                            listings={listings}
                            onSubmit={onOfferSubmit}
                            onCancel={onCancel}
                            onOfferComplete={onOfferComplete}
                        />
                    </Route>
                    <Route path='/account/deal_confirmation'>
                        <DealConfirmation
                            buyer={null}
                            announcement={null}
                            transferConfirmationMessages={transferConfirmationMessages}
                            onCompleteClick={onOfferComplete}
                            offerId={null}
                        />
                    </Route>
                    <Route path='/account/balance'>
                        <UserBalance userCards={userCards} userEquity={USER_EQUITY} />
                    </Route>
                </div>
                <UserRatingModal 
                    offerId={UserRatingModalOfferId}
                    closeModal={closeUserRatingModal}
                    visible={userRatingModalVisible}
                />
            </div>
        );
    }
}

function mapState(state) {
    const {
        authentication: {
            user
        },
        listings: {
            listingOffers,
            files
        },
        notifications: {
            purchaseMessages,
            offerNewMessages,
            confirmationOfSalesMessages,
            transferConfirmationMessages,
            soldMessages,
            rejectMessages
        },
        review: {
            userReviews,
            myReviews
        }
    } = state;
    const l = state.listings
    const listings = l.listings;
    return {
        user,
        userReviews,
        myReviews,
        listings,
        listingOffers,
        purchaseMessages,
        offerNewMessages,
        confirmationOfSalesMessages,
        transferConfirmationMessages,
        soldMessages,
        rejectMessages,
        files
    };
}

const actionCreators = {
    getUserListings: listingActions.getUserListings,
    submitOffer: userActions.acceptPurchaseOffer,
    cancelOffer: userActions.rejectPurchaseOffer,
    completeOffer: userActions.completePurchaseOffer,
    getOffersByBuyer: listingActions.getOffersByBuyer,
    getLiked: listingActions.getLiked,
    uploadFile: listingActions.uploadFile,
    sendReport: userActions.sendBugReport,
    getMyReviews: reviewActions.getMyReviews,
    getUserReviews: reviewActions.getUserReviews
};

const connectedUserAccountPage = connect(mapState, actionCreators)(UserAccountPage);
export { connectedUserAccountPage as UserAccountPage };
