import * as React from "react";
import * as Mat from "react-materialize";
import { Tabs, Descriptions, Badge, Comment, Avatar, Rate, Empty } from "antd";
import * as moment from "moment";
import './style.css'

const { TabPane } = Tabs;
const { Item: DescriptionItem } = Descriptions;
type UserReviewsProps = {
    userReviews: any[];
    myReviews: any[];
};

const myReviewComment = (e): React.ReactNode => {

    const announcement = e.offer.announcement;
    return (
        <Mat.Col key={e.id} l={12} s={12}>
            <div className='z-depth-2 user-review-card'>
                <Comment
                    content={
                        <p>{e.description ? e.description : "Пользователь не оставил комментарий"}</p>
                    }
                    author={e.userTo.firstName + " " + e.userTo.lastName}
                    avatar={<Avatar src={e.userTo.photo} />}
                    datetime={<span>{moment(e.createDate * 1000).fromNow()}</span>}
                    actions={[
                        <Rate disabled defaultValue={e.mark} />
                    ]}
                />
                {DescriptionBlock(announcement)}
            </div>
        </Mat.Col>
    );
};

const userReviewComment = (e): React.ReactNode => {
    const announcement = e.offer.announcement;

    return (
        <Mat.Col key={e.id} l={12} s={12}>
            <div className='z-depth-2 user-review-card'>
                <Comment
                    content={
                        <p>{e.description ? e.description : "Пользователь не оставил комментарий"}</p>
                    }
                    author={e.userFrom.firstName + " " + e.userFrom.lastName}
                    avatar={<Avatar src={e.userFrom.photo} />}
                    datetime={<span>{moment(e.createDate * 1000).fromNow()}</span>}
                    actions={[
                        <Rate disabled defaultValue={e.mark} />
                    ]}
                />
                {DescriptionBlock(announcement)}
            </div>
        </Mat.Col>
    );
}

const DescriptionBlock = (announcement): React.ReactNode => {
    const date = moment(announcement.createDate * 1000).format('DD.MM.YYYY');
    const bargain = announcement.bargain;

    return (
        <Descriptions
                    title="Информация об объявлении"
                    bordered
                    size='small'
                    column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                >
                    <DescriptionItem label='Название'>{announcement.name}</DescriptionItem>
                    {announcement.description.length ? 
                        <DescriptionItem label='Описание'>{announcement.description}</DescriptionItem>
                        : null
                    }
                    <DescriptionItem label='Цена'>{announcement.price} &#x20BD;</DescriptionItem>
                    <DescriptionItem label='Категория'>{announcement.productCategory.category}</DescriptionItem>
                    <DescriptionItem label='Дата создания'>{date}</DescriptionItem>
                    <DescriptionItem label='Торг разрешен'>{bargain ? 'Да' : 'Нет'}</DescriptionItem>
                    
                    
                </Descriptions>
    );
};

export const UserReviews = ({userReviews, myReviews}: UserReviewsProps): React.ReactComponentElement<any> => {    
    return (
        <div>
            <Tabs defaultActiveKey="1">
                <TabPane key="1" tab="Мои отзывы">
                    {myReviews.length ? 
                        <Mat.Row>
                        {myReviews.map(e => {
                            return myReviewComment(e);
                        })}
                        </Mat.Row> :
                        <Empty description={
                            <span>
                              О вас пока не оставили ни одного отзыва
                            </span>
                          }  />
                    }
                    
                </TabPane>
                <TabPane key="2" tab="Отзывы обо мне">
                    {userReviews.length ?
                        <Mat.Row>
                            {userReviews.map(e => {
                                return userReviewComment(e);
                            })}
                        </Mat.Row> :
                        <Empty description={
                            <span>
                              Вы пока не оставили ни одного отзыва
                            </span>
                          }  />
                    }
                </TabPane>
            </Tabs>
        </div>
    )
}