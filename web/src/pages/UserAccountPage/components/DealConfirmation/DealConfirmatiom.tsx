import {ReactComponentElement, ReactNode} from "react";
import * as React from "react";
import './style.css'
import {Empty} from "antd";


type props = {
    buyer: any;
    announcement: any;
    transferConfirmationMessages: any;
    onCompleteClick: any;
    offerId: number;
};

const buyerInfo = (buyer): ReactNode => {
    if (buyer === null) {
        return (
            <span>
                <p>Покупатель: -</p>
            </span>
        )
    } else {
        return (
            <span>
                <p>Покупатель:</p>
                <img className='circle' src={buyer.photo} style={{width: 100, height: 100}}/>
                {buyer.name}
            </span>
        )
    }
}

const announcementInfo = (announcement): ReactNode => {
    if (announcement === null) {
        return (
            <div>
                <p>Объявление: -</p>
                <p>Цена: -</p>
            </div>
        );
    } else {
        return (
            <div>
                <p>Объявление: {announcement.name}</p>
                <p>Цена: {announcement.price}</p>
            </div>
        );
    }
}

function toYes(bool): string {
    return bool ? 'Да' : 'Нет';
}

type DealConfirmationProps = {
    transferConfirmationMessages: any[];
    onCompleteClick: any;
}

export const DealConfirmation = ({transferConfirmationMessages, onCompleteClick}: props): ReactComponentElement<any> => {

    if (transferConfirmationMessages.length > 0) {
        console.log(transferConfirmationMessages);
        const data = JSON.parse(transferConfirmationMessages[0].data);
        const offerId = data.offer.id
        const buyer = {
            photo: data.offer.buyer.photo,
            name: `${data.offer.buyer.firstName} ${data.offer.buyer.lastName}`,
        }
        const endTime = data.offer.offerValidUntil;
        let {sellerAccept, buyerAccept, startTime} = data;
        startTime = startTime || Date.now()
        sellerAccept = sellerAccept || false
        buyerAccept =  buyerAccept || false
        //FIXME: Починить тут дату!!! и на сервере тоже
        return (
            <div className='container'>
                <h3>Подтверждение сделки</h3>
                <div>
                    {buyerInfo(buyer)}
                    <p>Принято продавцом: {toYes(sellerAccept)}</p>
                    <p>Принято покупателем: {toYes(buyerAccept)}</p>
                    <button className='hoverable light-blue darken-1 btn waves-effect waves-light'
                            onClick={(): void=>{onCompleteClick(offerId);}}
                    >
                        <i className="material-icons left">add</i>
                        Подтвердить продажу
                    </button>
                </div>
            </div>
        )
    } else {
        return (
            <Empty description={<span>Список заявок пуст</span>}/>
        )
    }
}
