import * as React from "react";
import "./style.css";
import {ARROW_URL} from "../../../../constants/img.constants";

export const HelpBox = (props) => {
    const { message } = props;
    return (
        <div className='help-box'>
            <img src={ARROW_URL} />
            <p>{message}</p>
        </div>
    )
}