import * as React from "react";
import * as M from "materialize-css";
import "./style.css";

type CheckListProps = {
    announcementId: any;
    submit: any;
    status: any;
    offer: any;
}

type CheckListState = {
    visible: boolean;
    checks: any;
    checkChecked: any;
    buyAllowed: boolean;
}

const parseBeckendChecks = (checks) => {
    return checks
        .sort(({orderQuestion1}, {orderQuestion2})=>orderQuestion2-orderQuestion1)
        .map(({question, description, URL, id})=>{
        return {text: `${question}. ${description}`,question,description, link: URL, id}
    })
}

class CheckList extends React.Component<CheckListProps, CheckListState> {
    public modalInstance: M.Modal;
    constructor(props) {
        super(props);
        this.state = {
            visible: true,
            checkChecked: false,
            buyAllowed: false,
            checks: []
        };

        this.modalInstance = null;
    }

    componentDidMount(): void {
        this.init();
        this.setState({checks: parseBeckendChecks(this.props.offer.announcement.productCategory.checklists)})
    }

    init = () => {
        console.log('INIT CHEK');
        const elem = document.querySelector('.check-modal');
        this.modalInstance = M.Modal.init(elem, {});
        // this.modalInstance.open();
    }

    checcIfAllChec = () => {
        if (this.state.checks.filter(({checked})=>checked !== true).length === 0) {
            this.setState({buyAllowed: true})
        } else {
            this.setState({buyAllowed: false})
        }
    }

    checkRow = (check, i) => {
        return (
            <div className='check-messages__row' key={i}>
                <label>
                    <input type="checkbox" onChange={(e)=>{
                        const { checks } = this.state;
                        checks[i].checked = e.currentTarget.checked;
                        const checcIfAllChec = this.checcIfAllChec;
                        this.setState({checks}, ()=>{
                            checcIfAllChec();
                        })

                    }}/>
                    <span className='check-messages__q'>{check.question}</span>
                </label>
                <div className='check-messages'>
                    <span className='check-messages__desc'>{check.description}</span>
                </div>
                <button className='btn'>
                    <a href={check.link} target={'_blank'} style={{color: "white"}}>Подробнее</a>
                </button>
            </div>
        )
    }

    onClickBuy = () => {
        if (this.state.checkChecked) {
            this.props.submit(this.props.announcementId);
        } else {
            this.setState({visible: true}, ()=> {
                if (this.state.visible) {
                    this.modalInstance.open();
                } else {
                    this.modalInstance.close();
                }
            })
        }
    }

    render() {
        console.log(this.props);
        return (
            <div>
                <div className='modalContCheck'>
                    <div className='modal check-modal'>
                        <div className={this.state.visible ? "check-modal__container check-modal__container--active" : "check-modal__container"}>
                            <div className="modal-content modal-reserve-announcement" style={{position: "relative"}}>
                                <button className='btn red btn-floating modalContCheck__close-btn' onClick={()=>{
                                    console.log('sdf');
                                    console.log(this);
                                    // this.modalInstance.destroy();
                                    this.setState({visible: false}, ()=>{
                                        this.modalInstance.close();
                                    })
                                }}><i className="material-icons">close</i></button>
                                <h4>Проверьте товар перед покупкой!</h4>
                                <div className="row">
                                    {this.state.checks.map((e,i)=>this.checkRow(e,i))}
                                </div>
                            </div>
                            <div className="modal-footer" style={{position: 'relative'}}>
                                <p className='buy-hint'>Нажимая купить вы подтверждаете, что вас устраивает состояние приобретаемого товара.</p>
                                <button
                                    onClick={()=>this.props.submit(this.props.announcementId)}
                                    className={!this.state.buyAllowed ? 'btn-small waves-effect disabled' : 'btn-small waves-effect'}
                                >Купить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <button
                    className='right btn blue btn-small hoverable'
                    style={this.props.status !== "2" ? {display: 'none'}:{}}
                    onClick={this.onClickBuy}
                >
                    Купить
                </button>
            </div>
        );
    }
}

export default CheckList;