import {IMG_ERROR_PH, IMG_PH} from "../../../../constants/img.constants";
import {ReactElement, ReactNode} from "react";
import * as moment from "moment";
import * as React from "react";
import { Empty, Card, Avatar, Typography, Descriptions } from "antd";
import { ShoppingCartOutlined, FieldTimeOutlined, DownloadOutlined } from "@ant-design/icons";
import './style.css';
import CheckList from "../CheckList/CheckList";

const { Title } = Typography;
const { Meta } = Card;
const apiUrl = process.env.API_URL;

type props = {
    offers: any;
    onSubmit: any;
};

const status2text = (status): string => {
    if (status === "0") {
        return "Вы отправили предложение продавцу"
    }
    if (status === "1") {
        return "Предложение о торге отправлено"
    }
    if (status === "2") {
        return "Сделка подтверждена продавцом"
    }
    if (status === "3") {
        return "Истек срок предложения"
    }
    if (status === "4") {
        return "Сделка завершена"
    }
    if (status === "5") {
        return "Предложение не принято продавцом"
    }
    return status
}

const niceDate = (date): string => {
    return moment(date).fromNow();
}

const buyButton = ({status, submit, id, offer}): ReactNode => {
    return (
        <CheckList announcementId={id} status={status} submit={submit} offer={offer}/>
        // <div>
        //     <button className=' btn blue btn-small hoverable'
        //             // style={status !== "2" ? {display: 'none'}:{}}
        //             disabled={status !== "2"}
        //             onClick={(): void=>{
        //                 submit(id);
        //             }}
        //     >
        //         <ShoppingCartOutlined /> Купить
        //     </button>
        // </div>
        );
}

const contractButton = (status, url): ReactNode => {
    return (
        <div >
            <button className='btn green btn-small hoverable'
                hidden={status !== '4' && url === null}
                onClick={(): void => {
                    window.open(`${apiUrl}${url}`, '_blank');
                }}
            >
               <DownloadOutlined /> Загрузить договор
            </button>
        </div>
    );
}

const addDefaultSrc = (ev): void => {
    ev.target.src = IMG_ERROR_PH;
}

const offer = (o, submit): ReactNode => {
    const {
        id,
        buyer,
        offerValidUntil,
        offerStatus,
        announcement,
        contract
    } = o;
    const {seller, name, photos} = announcement;

    const url = photos.length ? `${process.env.API_URL}${photos[0].fileURL}` : IMG_PH;
    const sellerTitle = `Продавец: ${seller.firstName} ${seller.lastName}`;
    const exprireString = !['3', '4', '5'].includes(offerStatus) ? `Истекает: ${niceDate(offerValidUntil*1000)}`: 'Время вышло';
    return (
        <div key={id} className="col l4 m5 s12">
            <Card
                style={{width: '100%', marginBottom: 15}}
                cover={<img src={url} onError={addDefaultSrc}/>}
                actions={[
                    buyButton({status: offerStatus, submit, id, offer: o}),
                    contractButton(offerStatus, contract)
                ]}
                >
                    <Title level={4}>{name}</Title>
                    <Meta
                        avatar={<Avatar src={seller.photo} />}
                        title={sellerTitle}
                        description={<span><FieldTimeOutlined /> {exprireString}</span>   }
                        style={{marginBottom: '20px', marginTop: '20px'}}
                    />
                    <Descriptions size='small' bordered >
                        <Descriptions.Item label="Статус">{status2text(offerStatus)}</Descriptions.Item>

                    </Descriptions>
            </Card>
        </div>
    );
}
const checkItem = (checkOption) => {
    return (
        <div className='col s12'>
            <label>
                <input type="checkbox" />
                <span>{checkOption}</span>
            </label>
        </div>
    )
}

export const UserPurchases = ({offers, onSubmit}: props): ReactElement => {
    if (offers.length > 0) {
        return (
            <div className="row">
                {
                    offers.sort((a,b)=>parseInt(a.offerStatus) - parseInt(b.offerStatus)).map(o => {
                        return offer(o, onSubmit)
                    })
                }
            </div>
        );
    }
    return (<Empty description={<span>Список покупок пуст</span>}/>)
}
