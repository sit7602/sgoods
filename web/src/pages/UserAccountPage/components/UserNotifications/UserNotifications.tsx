import {listings} from "../../../../reducers/listing.reducer";
import {Listing} from '../../../../types/types';

const moment = require('moment');
import {ReactNode} from "react";
import * as React from "react";
import './index.css'
import {Empty, Rate} from "antd";


type props = {
    listings: Listing[];
    onSubmit: any;
    onCancel: any;
    onOfferComplete: any;
};

const nice = (date) => {
    moment.locale('ru')
    return moment(date).fromNow()
}

const parseOffers = (listings, onSubmit): any[] => {
    console.log(listings);
    return listings
        .filter(e => e.offers.length)
        .map(({name, price, offers, photos, id, bargain, seller}) =>
            offers.map(o => {
                return {...o, name, price, onSubmit, photos, announcementId: id, bargain: bargain, seller}
            }))
        .flat()
}

const addDefaultSrc = (ev): void => {
    ev.target.src = 'https://cdn.dribbble.com/users/463734/screenshots/2016799/generic-error_shot_1x.png';
}

const offerStatus2actions = (offerStatus, onSubmit, onCancel, id, announcementId, onOfferComplete): ReactNode => {
    if (offerStatus === '0') {
        return (
            <div className="col s6 right">
                <button className='btn waves-effect right red'
                        onClick={(): void => {
                            onCancel(id, announcementId);
                        }}
                        style={{marginLeft: 10}}
                >Отклонить
                </button>
                <button className='btn waves-effect right blue'
                        onClick={() => {
                            onSubmit(id, announcementId);
                        }}
                >Принять
                </button>
            </div>
        )
    }
    if (offerStatus === '1') {
        return (
            <div className="col s6 right">
                <button className='btn waves-effect right red'
                        onClick={() => {
                            onCancel(id, announcementId);
                        }}
                        style={{marginLeft: 10}}
                >Отклонить
                </button>
                <button className='btn waves-effect right blue'
                        onClick={() => {
                            onSubmit(id, announcementId)
                        }}
                >Принять торг
                </button>
            </div>
        )
    }
    if (offerStatus === '2') {
        return (
            <div className="col s6 right">
                <p style={{textAlign: "right"}}>Запрос на покупку подтвержден</p>
                <button className='btn waves-effect right green'
                        onClick={() => {
                            onOfferComplete(id)
                        }}
                >Подтвердить передачу товара
                </button>
            </div>
        )
    }
    if (offerStatus === '3') {
        return (
            <div className="col s6 right">
                <p style={{textAlign: "right"}}>Вышло время подтверждения</p>
            </div>
        )
    }
    if (offerStatus === '4') {
        return (<div className="col s6 right">
            <p style={{textAlign: "right"}}>Товар продан</p>
        </div>)
    }
    if (offerStatus === '5') {
        return (<div className="col s6 right">
            <p style={{textAlign: 'right'}}>Предложение не принято продавцом</p>
        </div>);
    }
}

const OfferCard = ({id, announcementId, name, description, buyer, offerValidUntil, offerStatus, newPrice, price,
                       onSubmit, onCancel, photos, bargain, onOfferComplete}) => {
    const url = photos[0] ? `${process.env.API_URL}${photos[0].fileURL}` : '';
    return (
        <div key={id} className='row blue lighten-5 black-text rounded-border'>
            <div className="col s12" style={{padding: 10}}>
                <div className='notification__row notification__header'>
                    <img src={url} onError={addDefaultSrc}/>
                    <div className='fcol'>
                        <span className='name'>{name}</span>
                        <span
                            className='date'>{moment(offerValidUntil * 1000 - 1000 * 60 * 60 * 24).format('HH:MM DD-MM-YYYY')}</span>
                    </div>
                </div>
                <div className='notification__row'>
                    <img className='profile-photo circle' src={buyer.photo}/>
                    <p style={{marginBottom:0}}><b>Покупатель: </b> {buyer.firstName} {buyer.lastName}</p>
                </div>
                <div className="notification__row" style={{marginTop: 6}}>
                    <i className="material-icons blue lighten-2 white-text circle">school</i>

                    <p style={{marginBottom:0}}><b>Рейтинг покупателя: </b> <span style={{marginRight: 5}} className='user-rating'>{Math.round(buyer.rating * 10)/10} / 5</span> <Rate disabled defaultValue={buyer.rating} /></p>
                </div>
                <p className='notification__row' style={{marginTop: 6, marginBottom: 6}}><i
                    className='material-icons blue lighten-2 white-text circle'>monetization_on</i>
                    <span> <b> Ваша цена: </b> {price} &#x20BD; </span>
                </p>
                {bargain ?
                    (<><p hidden={!bargain} className='notification__row'><i
                            className='material-icons blue lighten-2 white-text circle'>monetization_on</i>
                            <span> <b> Предложенная цена:</b> {newPrice} &#x20BD; </span>
                        </p>
                            <p className="notification__row" style={{marginTop: 6}}><i
                                className='material-icons blue lighten-2 white-text circle'>short_text</i> <b>Текст
                                предложения: </b> <span> {description} </span>
                            </p>
                        </>
                    )
                    : null}

                <p className='notification__row'><i
                    className='material-icons blue lighten-2 white-text circle'>event_note</i>
                    <b>Истекает: </b> {nice(offerValidUntil * 1000)}
                </p>

                <div className="row" style={{marginBottom: 0}}>
                    {offerStatus2actions(offerStatus, onSubmit, onCancel, id, announcementId, onOfferComplete)}
                </div>
            </div>
        </div>
    )
}

export const UserNotifications = ({listings, onSubmit, onCancel, onOfferComplete}: props) => {
    const offers = parseOffers(listings, onSubmit)
        .sort((a, b) => parseInt(a.offerStatus) - parseInt(b.offerStatus));
    if (offers.length === 0) {
        return (<Empty description={<span>Список запросов на покупку пуст</span>}/>)
    }
    return (
        <div>
            {
                offers.map(o => (
                    <OfferCard
                        id={o.id}
                        name={o.name}
                        buyer={o.buyer}
                        newPrice={o.newPrice}
                        offerValidUntil={o.offerValidUntil}
                        price={o.price}
                        onSubmit={onSubmit}
                        onCancel={onCancel}
                        announcementId={o.announcementId}
                        photos={o.photos}
                        offerStatus={o.offerStatus}
                        bargain={o.bargain}
                        description={o.description}
                        onOfferComplete={onOfferComplete}
                    />
                ))
            }
        </div>
    )
}
