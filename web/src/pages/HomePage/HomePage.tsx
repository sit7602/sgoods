import {listingActions, userActions, categoryActions} from "../../actions";
import {AnnouncementList} from "../../components/AnnouncementList";
import {Notifications} from "components/Notification/Notifications";
import {ARROW_URL} from "../../constants/img.constants";
import "react-datepicker/dist/react-datepicker.css";
import {ReactElement, ReactNode} from "react";
import DatePicker from "react-datepicker";
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import * as M from "materialize-css";
import { TreeSelect } from "antd";
const { TreeNode } = TreeSelect;
import * as React from "react";
import "antd/dist/antd.css";
import './style.css';



function* idGenerator(index): Generator {
    while (true) {
        yield index++;
    }
}

const idGen = idGenerator(0);

const makeSectionBlock = ({name, url, key}): ReactNode => {
    return (
        <div className="col xl2 l2 s3 m3" key={key}>
            <div className="card-category hoverable" style={{height: 300}}>
                <div className="card-image">
                    <img src={url} className="category-card__image"/>
                </div>
                <div className="card-content section-content">
                    <p>{name}</p>
                </div>
            </div>
        </div>
    )
};

const sections = [
    {
        key: idGen.next().value,
        name: 'Одежда',
        url: 'https://cdn.vostok.ru/uploads/global/images/promoblock/original/1644edf3f91e06e96a967b88902149ce36db3ccb.png'

    },
    {
        key: idGen.next().value,
        name: 'Смартфоны и планшеты',
        url: 'https://глав-скупка.рф/images/mobile/block1.png'
    },
    {
        key: idGen.next().value,
        name: 'Автомобили',
        url: 'https://www.ford.ru/content/dam/guxeu/ru/ru_ru/vehicle-images/mondeo/ford-mondeo-ru-768x432-white-platinum.png'
    },
    {
        key: idGen.next().value,
        name: 'Бытовая техника',
        url: 'https://tetrasis-bt.ru/sites/default/files/styles/653_467/public/bez_imeni.png?itok=6jHkTbPy'
    }
];

type HomePageProps = {
    user: object;
    listings: any;
    getListings: any;
    categories: any;
    getCategories: any;
    loading: boolean;
};
type HomePageState = {
    page: number;
    listings: [];
    categories: any[];
    selectedCategory: any;
    sort: string;
    filter: string;
    category: string;
    userLesson: number;
    lessonModal: any;
};

class HomePage extends React.Component<HomePageProps, HomePageState> {
    constructor(props) {
        super(props);
        this.props.getListings({size: 16});
        this.props.getCategories();
        this.state = {
            page: 0,
            listings: [],
            categories: [],
            selectedCategory: -1,
            sort: null,
            filter: null,
            category: null,
            userLesson: localStorage.getItem('lessonShown') === null ? 0 : 1,
            lessonModal: null
        }
    }

    componentDidMount(): void {
        this.fetchCategories();
        this.initModals();
        localStorage.setItem('lessonShown', '1')
    }

    async fetchCategories() {
        const resp = await fetch(process.env.API_URL + '/info/getCategoryList').then(r => r.json()).catch(error => {
            return {error};
        });
        if (resp.error === undefined) {
            this.setState({categories: resp}, () => {
                this.initSelect();
            })
        }
    }

    loadMore = (): void => {
        const {loading} = this.props;
        let {page} = this.state;
        const {updateListingsList} = this;
        if (!loading) {
            page = page + 1;
            this.setState({page}, function () {
                updateListingsList(this.props.listings.length + 4)
            });
        }
    }

    initSelect = (): void => {
        const elems = document.querySelectorAll('.select');
        M.FormSelect.init(elems, {});
    }

    initModals = (): void => {
        const lessonModal = document.querySelector('.modal');
        M.Modal.init(lessonModal);
        this.setState({lessonModal})
    }

    setSort = (e): void => {
        const {updateListingsList} = this;
        this.setState(
            {sort: e.currentTarget.value},
            function() {
                updateListingsList(this.props.listings.length)
            }
        );
    }

    setFilter = (e): void => {
        const {updateListingsList} = this;
        this.setState({filter: `${e.currentTarget.value}`},
            function() {
                updateListingsList();
            }
        );
    }

    updateListingsList = (itemSize= 0): void => {
        const {sort, filter, page} = this.state;
        const size = itemSize === 0 ? 4 : itemSize;
        this.props.getListings({size: size, sort, query: filter, page: 0});
    }

    lessonBG = (): ReactElement => {
        return null;
        if (this.state.userLesson === 0) {
            return (
                <div className='lesson-bg'>
                    <div className='close' onClick={() => {
                        this.setState({userLesson: 1})
                    }}><i className='material-icons'>close</i></div>
                    <div className='lesson-content'>
                        <img src={ARROW_URL}/>
                        <p>Чтобы выставить товар на продажу нажмите кнопку</p>
                    </div>
                </div>
            )
        }
        return null;
    }

    renderTree = (obj: any): ReactNode => {
        return obj.map(e => {
            if (!e.childrenCategory.length) {
                return (
                    <TreeNode key={e.id} value={e.id} title={e.category}/>                                    
                );
            } else {
                return (
                    <TreeNode key={e.id} value={e.id} title={e.category}>                                    
                        {
                            this.renderTree(e.childrenCategory)
                        }
                    </TreeNode>

                );
            }
        })
    }

    render(): ReactNode {
        const {setSort, setFilter} = this;
        let {listings} = this.props;
        const {loading} = this.props;
        if (this.state.selectedCategory !== null) {
            listings = listings.filter(l => l.productCategory.id === this.state.selectedCategory || this.state.selectedCategory === -1)
        }

        return (
            <div className="content">
                {this.lessonBG()}
                <div className="row" hidden>
                    <div className="col s12">
                        <h3 className="row-title">Категории</h3>
                    </div>
                </div>
                <div className="row" hidden style={{marginBottom: 30}}>
                    {sections.map(s => makeSectionBlock(s))}
                </div>
                <div className="row">
                    <div className="col s12">
                        <h3 className="row-title">Все объявления</h3>
                    </div>
                </div>
                <div className="row homepage__filters-holder">
                    <div className="col s12">
                        <h3 className="row-title" style={{fontSize: 16}}>Фильтры и сортировки</h3>
                    </div>
                    <div className="col s3">
                        <div className="row">
                            <div className="input-field col s12">
                                <i className="material-icons prefix">search</i>
                                <input type="text"
                                       id="autocomplete-input"
                                       className="autocomplete"
                                       onChange={setFilter}
                                />
                                <label htmlFor="autocomplete-input">Поиск</label>
                            </div>
                        </div>
                    </div>
                    <div className="col s3" style={{paddingTop: 14, display: 'none'}}>
                        <DatePicker
                            selected={new Date()}
                        />

                    </div>
                    <div className="col s3">
                        <TreeSelect showSearch 
                            allowClear 
                            treeDefaultExpandAll 
                            style={{width: '100%', marginTop: 28}} 
                            searchPlaceholder='Выберите категорию'
                            filterTreeNode = {(value, treeNode) => {
                                const title = treeNode.title + "";
                                return title.toLocaleUpperCase().indexOf(value.toLocaleUpperCase()) !== -1;
                            }}
                            value={this.state.selectedCategory}
                            onChange={(value) => {
                                this.setState({
                                    selectedCategory: value || -1
                                })
                            }}
                            treeDefaultExpandedKeys={[]}
                            >
                                <TreeNode key={-1} value={-1} title='Все категории' />
                            {
                                this.state.categories.map(e => {
                                    if (!e.childrenCategory.length) {
                                        return (
                                            <TreeNode key={e.id} value={e.id} title={e.category}/>                                    
                                        );
                                    } else {
                                        return (
                                            <TreeNode key={e.id} value={e.id} title={e.category}>                                    
                                                {
                                                    this.renderTree(e.childrenCategory)
                                                }
                                            </TreeNode>

                                        )
                                    }
                                })
                            }
                        </TreeSelect>
                    </div>
                    <div className="col s3" style={{paddingTop: 14}}>
                        <select className="select"
                                name="sort"
                                onChange={setSort}
                                value={0}
                        >
                            <option value="0" disabled>Выбрать сортировку</option>
                            <option value="id:desc">Сначала новые</option>
                            <option value="id:asc">Сначала старые</option>
                            <option value="price:asc">По возрастанию цены</option>
                            <option value="price:desc">По убыванию цены</option>
                        </select>
                    </div>
                </div>
                <div className="row">
                    <AnnouncementList listings={listings}/>
                    <button onClick={this.loadMore}
                            disabled={loading} className="btn btn--loadMore">Загрузить еще
                    </button>
                </div>
            </div>
        );
    }
}

function mapState(state): object {
    const {authentication, category: {categories}} = state;
    let {listings} = state;
    const {user} = authentication;
    listings = listings.listings;
    return {user, listings, categories};
}

const actionCreators = {
    getListings: listingActions.get,
    getCategories: categoryActions.get,
};

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export {connectedHomePage as HomePage};
