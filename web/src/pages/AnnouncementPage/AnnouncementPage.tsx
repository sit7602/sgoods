import { Seller, SellerInfo, AnnouncementPhoto, ProductCategory } from "../../types/types";
import { listingActions, userActions } from "../../actions";
import "react-image-gallery/styles/css/image-gallery.css"
import ImageGallery from 'react-image-gallery';
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { userService } from "../../services";
import { Link } from 'react-router-dom';
import * as M from "materialize-css";
import * as Mat from "react-materialize";
import { connect } from 'react-redux';
import { ReactNode } from "react";
import * as React from "react";
import * as moment from 'moment';
import { Rate, Popconfirm } from "antd";
import { QuestionCircleOutlined } from '@ant-design/icons';

import './style.css';
import { Map, Placemark, SearchControl, YMaps } from "react-yandex-maps";
import {showMessage} from "../../helpers/utils";


type props = {
    user: {
        id: bigint;
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    userCards: any;
    announcement: any;
    purchaseGood: Function;
    purchaseBargain: Function;
    getAnnouncementById: Function;
    close: Function;
}

type state = {
    id: number;
    seller: Seller;
    buyer: null;
    sellerInfo: SellerInfo;
    name: string;
    description: string;
    price: number;
    createDate: number;
    status: string;
    type: string;
    photos: AnnouncementPhoto[];
    productCategory: ProductCategory;
    bekendError: boolean;
    numberVisible: boolean;
    dataFetched: boolean;
    cardId: number;
    offerValidSeconds: number;
    modalVisible: boolean;
    modalBargainVisible: boolean;
    bargainDescription: string;
    newPrice: number;
    yMapInstance: any;
    selectedAddrString: string;
}

const formatter = new Intl.NumberFormat();

class AnnouncementPage extends React.Component<any, state> {
    private modalInstance: M.Modal;

    constructor(props) {
        super(props);
        this.state = {
            id: parseInt(window.location.href.split('/')[window.location.href.split('/').length - 1]),
            seller: null,
            buyer: null,
            sellerInfo: null,
            name: null,
            description: null,
            price: null,
            createDate: null,
            status: null,
            type: null,
            photos: null,
            productCategory: null,
            bekendError: false,
            numberVisible: false,
            dataFetched: false,
            cardId: -1,
            offerValidSeconds: 3600,
            modalVisible: false,
            modalBargainVisible: false,
            bargainDescription: "",
            newPrice: 0,
            yMapInstance: null,
            selectedAddrString: '',
        };

        // this.fetchAnnouncementInfo = this.fetchAnnouncementInfo.bind(this);
        this.shouldShowPhone = this.shouldShowPhone.bind(this);
        this.previewPhone = this.previewPhone.bind(this);
        this.showListing = this.showListing.bind(this);
        this.isOwnAnnouncement = this.isOwnAnnouncement.bind(this);
        this.isEnoughMoney = this.isEnoughMoney.bind(this);


        this.props.getAnnouncementById(window.location.pathname.split("/").pop());

        this.modalInstance = null;
    }

    componentDidMount(): void {
        this.init();
        const { userCards } = this.props;
        if (userCards && userCards.length)
            this.setState({
                cardId: userCards[0].id
            });
    }

    componentDidUpdate(prevProps: Readonly<any>, prevState: Readonly<state>, snapshot?: any): void {
        this.init();
    }

    purchase = (): void => {
        const data = {
            "announcement_id": this.state.id,
            "card_id": this.state.cardId,
            "offer_valid_seconds": this.state.offerValidSeconds
        };
        this.props.purchaseGood(data);
    }

    sendBargain = (): void => {
        const data = {
            "announcement_id": this.state.id,
            "card_id": this.state.cardId,
            "offer_valid_seconds": this.state.offerValidSeconds,
            "new_price": this.state.newPrice,
            "description": this.state.bargainDescription
        };
        // console.log(data)
        this.props.purchaseBargain(data);
    }

    closeAnnouncement = (): void => {
        const { id } = this.state;
        this.props.close(id);
    }

    init = (): void => {
        this.initModal();
        this.initSelect();
    }

    initModal = (): void => {
        //TODO: fix closing after selects card in dropdown
        const elem = document.querySelector('.modal');
        this.modalInstance = M.Modal.init(elem, {});
    };

    initSelect = (): void => {
        const elems = document.querySelectorAll('.select');
        M.FormSelect.init(elems, {});
    };

    shouldShowPhone(): boolean {
        return true;
        const { sellerInfo } = this.state;
        const startTime = new Date(sellerInfo.callTimeStart);
        const endTime = new Date(sellerInfo.callTimeEnd);
        const now = new Date();
        return now >= startTime && now <= endTime
    };

    isOwnAnnouncement(): boolean {
        const { user, announcement: { seller } } = this.props;

        if (seller && seller.id && user && user.id) {
            return user.id === seller.id;
        }
        return false
    };

    isEnoughMoney(): boolean {
        const { user } = this.props;
        const { price } = this.props.announcement;
        if (user.userCards.length && Math.max(...user.userCards.map(e=>e.balance)) < price) {
            return false;
        }
        return true;
    };

    previewPhone(): ReactNode {
        const { sellerInfo } = this.props.announcement;
        const { isOwnAnnouncement } = this;
        return (
            isOwnAnnouncement() ?
                null :
                <div className='announcement-page__header-row__right' style={{ justifyContent: 'flex-end', display: 'flex' }}>
                    <button className='btn btn--w200' style={{ marginRight: 10 }} onClick={() => {
                        this.setState({ numberVisible: !this.state.numberVisible })
                    }}>{this.state.numberVisible ? '+7' + sellerInfo.phone : 'Показать номер'}</button>
                    <button className='btn btn--w200 blue'>Написать</button>
                </div>

        )
    };

    validateBargainBtn = (): boolean => {
        const { newPrice, bargainDescription } = this.state;
        return !(newPrice !== 0 && bargainDescription.length > 0);
    };

    geocode(yMapInstance): void {
        const { geoPoint } = this.props.announcement;
        const coords = [geoPoint.lat, geoPoint.lon];
        this.setState({ yMapInstance }, () => {
            this.decodeCoords(coords);
        })
    };

    decodeCoords(coords): void {
        const { yMapInstance } = this.state;
        yMapInstance.geocode(coords).then(res => {
            this.onAddressDecoded(res);
        });
    };

    onAddressDecoded = (res): void => {
        const firstGeoObject = res.geoObjects.get(0);
        const selectedAddrString = firstGeoObject.getAddressLine();
        this.setState({ selectedAddrString })
    };

    toggleModal = (): void => {
        const { modalVisible } = this.state;
        this.setState({ modalVisible: !modalVisible });
    }

    toggleBargainModal = (): void => {
        this.setState({
            modalBargainVisible: !this.state.modalBargainVisible
        })
    }

    isArchived = (): boolean => {
        const { status } = this.props.announcement;

        return status === '3';
    }

    showArchiveText = (): string => {
        return this.isArchived() ?
            '[В архиве]' :
            '';
    }

    showListing(): ReactNode {
        const { shouldShowPhone, previewPhone, isOwnAnnouncement, isEnoughMoney, sendBargain, toggleModal, toggleBargainModal, isArchived, showArchiveText } = this;
        const { photos, seller, name, price, description, productCategory, createDate, geoPoint, status } = this.props.announcement;
        const images = photos.map((p, i) => {
            const url = `${process.env.API_URL}${p.fileURL}`
            return { original: url, thumbnail: url, id: p.id };
        });
        const needHide = isOwnAnnouncement();
        const needHideRemoveBtn = isArchived() || !needHide;

        return (
            <div className="col s12">
                <div style={{ 'width': '100%', userSelect: 'none' }}>
                    <Carousel showArrows infiniteLoop={images.length >= 2} autoPlay={false} showThumbs={false}
                        emulateTouch swipeable
                        useKeyboardArrows>
                        {images.map(image => {
                            return (
                                <img key={image.id} className='image-gallery-image' src={image.original} alt="" />
                            );
                        })}
                    </Carousel>
                </div>
                <div className="row listing__header blue lighten-5" style={{ flexWrap: "wrap" }}>
                    <h3 style={{ width: '100%' }}>{name} {showArchiveText()}</h3>
                    <img className='listing__user-pic circle' src={seller.photo} />
                    <div style={{ display: 'flex', flexDirection: 'column', marginLeft: 10 }}>
                        <p>Продавец: {seller.lastName} {seller.firstName}</p>
                        <p title={`Рейтинг продавца: ${Math.round(seller.rating * 10)/10}`}>
                            <span style={{marginRight: 5}} className='user-rating'>{Math.round(seller.rating * 10)/10} / 5</span>
                            <Rate disabled defaultValue={seller.rating} />
                        </p>

                    </div>
                    <div className='announcement-page__header-row'>
                        <div className='announcement-page__header-row-col-on-mob announcement-page__header-row__left'>
                            <p className="listing__price"><span
                                className='listing__key'>Цена:</span>{formatter.format(price).replace(/,/g, ' ')} &#8381;
                            </p>
                        </div>
                        {shouldShowPhone() ? previewPhone() : (
                            <div className='announcement-page__header-row-col-on-mob announcement-page__header-row__right'>
                                <button className='btn blue'>Написать</button>
                            </div>)}
                    </div>
                </div>
                <div className="row blue lighten-5" style={{ padding: 10 }}>
                    <div className='description-block'>
                        <p className='listing__description-header description-block__key'>Местоположение:</p>
                        <p className="listing__description">{this.state.selectedAddrString}</p>
                    </div>
                    <div className='description-block'>
                        <p className='listing__description-header description-block__key'>Описание:</p>
                        <p className="listing__description">{description}</p>
                    </div>
                    <div className='description-block'>
                        <p className='listing__description-header description-block__key'>Категория:</p>
                        <p className="listing__description">{productCategory.category}</p>
                    </div>
                    <div className='description-block'>
                        <p className='listing__description-header description-block__key'>Истекает:</p>
                        <p className="listing__description">{"через " + moment(createDate * 1000 + 1000 * 60 * 60 * 24 * 14).fromNow(true)}</p>
                    </div>
                    <YMaps
                        enterprise
                        query={{ ns: 'use-load-option', apikey: "137ca00c-15ed-4956-8f82-f3911b5bfb90", load: 'Map,Placemark,control.ZoomControl,control.FullscreenControl,geoObject.addon.balloon,geocode' }}
                    >
                        <Map
                            defaultState={{ center: [geoPoint.lat, geoPoint.lon], zoom: 9 }}
                            onLoad={this.geocode.bind(this)}
                            width='100%'
                            modules={['geocode']}
                        >
                            <Placemark
                                defaultGeometry={[geoPoint.lat, geoPoint.lon]}
                                geometry={[geoPoint.lat, geoPoint.lon]}
                                properties={{ balloonContentBody: this.state.selectedAddrString }}
                            />
                        </Map>
                    </YMaps>
                    <div className='announcement_page--footer' style={{ 'display': 'flex', 'justifyContent': 'space-between', marginTop: 30 }} hidden={needHide}>
                        <div style={{ 'display': 'flex', 'justifyContent': 'flex-start', marginTop: 30 }} >
                            <div className="s2" hidden={needHide}>
                                <button
                                    style={{marginRight:5}}
                                    className="btn btn-small waves-effect"
                                    onClick={()=>{
                                        if (!isEnoughMoney()) {
                                            showMessage('Недостаточно средств на счете.')
                                        } else {
                                            toggleModal();
                                        }
                                    }}
                                >
                                    Сделать предложение продавцу
                                </button>
                            </div>
                            <div className="s4 myDiv" hidden={!this.props.announcement.bargain || needHide}>
                                <button
                                    style={{marginRight:5}}
                                    className="btn btn-small waves-effect blue"
                                    onClick={toggleBargainModal}
                                >
                                    Предложить новую цену
                                </button>
                            </div>
                            <div className="s2" hidden={needHideRemoveBtn}>
                                <Popconfirm
                                    title="Вы уверены, что хотите удалить объявление?"
                                    onConfirm={this.closeAnnouncement}
                                    placement='bottomLeft'
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    okText="Удалить"
                                    cancelText="Отменить">
                                    <button className="btn btn-small red lighten-2 waves-effect waves-light"
                                    > Удалить объявление </button>
                                </Popconfirm>
                            </div>
                            <div className="s2" hidden={needHide}>
                                <button
                                    className="btn red btn-small waves-effect"
                                    onClick={()=>{console.log('report announcement');}}
                                >
                                    Пожаловаться
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    showLoader() {
        return (
            <div style={{ margin: '0 auto', display: 'block', marginTop: 201 }} className="preloader-wrapper big active">
                <div className="spinner-layer spinner-blue">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div>
                    <div className="gap-patch">
                        <div className="circle"></div>
                    </div>
                    <div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                </div>

                <div className="spinner-layer spinner-red">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div>
                    <div className="gap-patch">
                        <div className="circle"></div>
                    </div>
                    <div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                </div>

                <div className="spinner-layer spinner-yellow">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div>
                    <div className="gap-patch">
                        <div className="circle"></div>
                    </div>
                    <div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                </div>

                <div className="spinner-layer spinner-green">
                    <div className="circle-clipper left">
                        <div className="circle"></div>
                    </div>
                    <div className="gap-patch">
                        <div className="circle"></div>
                    </div>
                    <div className="circle-clipper right">
                        <div className="circle"></div>
                    </div>
                </div>
            </div>
        )
    }

    showError(): any {
        return (
            <div className="col s12">
                <h3>Страница временно недоступна</h3>
                <img src='https://i.pinimg.com/originals/e1/04/2e/e1042e5d99353437bc15aa0731b2eaba.png' />
                <Link to='/'>Вернуться на главную</Link>
            </div>
        )
    }

    getCardBalance = (): number => {
        const { userCards } = this.props;
        const { cardId } = this.state;
        return cardId !== -1
            ? userCards.filter(e => e.id === cardId)[0].balance
            : userCards[0].balance;
    }

    render(): ReactNode {
        const { showListing, showError, showLoader } = this;
        if (Object.keys(this.props.announcement).length === 0) {
            return (
                showLoader()
            );
        } else {
            const {
                id,
                seller,
                buyer,
                sellerInfo,
                name,
                description,
                price,
                createDate,
                status,
                type,
                photos,
                productCategory,
            } = this.props.announcement;
            const { cardId, bekendError } = this.state;
            const { userCards } = this.props;
            const { isOwnAnnouncement, sendBargain } = this;
            return (
                <div className="content">
                    <div className="row">
                        {bekendError ? showError() : showListing()}
                    </div>
                    <div onClick={() => { this.setState({ modalVisible: false, modalBargainVisible: false }); }}
                        className={this.state.modalVisible || this.state.modalBargainVisible ? "bottom-modal__background bottom-modal__background--active" : "bottom-modal__background"} />
                    <div className={this.state.modalVisible ? "bottom-modal__container bottom-modal__container--active" : "bottom-modal__container"}>
                        <div className="modal-content modal-reserve-announcement">
                            <h4>Оформить заявку на покупку</h4>
                            <div className="row">
                                <div className="col s4 input-field">
                                    <select
                                        value={this.state.cardId}
                                        onChange={(e) => {
                                            this.setState({ cardId: parseInt(e.currentTarget.value) })
                                        }}
                                        className='select'
                                        name="card"
                                        id="card">
                                        {userCards.map(card => {
                                            const { id, cardNumber } = card;
                                            return (
                                                <option key={id} value={id}>{cardNumber}</option>
                                            );
                                        })}
                                    </select>
                                    <label htmlFor="card">Выберите карту для оплаты</label>
                                </div>
                                <div className="col s8">
                                    <blockquote>
                                        <h4>Текущий баланс на карте: <span>{this.getCardBalance()}</span> &#x20BD;</h4>
                                    </blockquote>
                                </div>
                                <div className="col s4 input-field" style={{marginTop: 25}}>
                                    <select name="time" id="time" className='select' onChange={(e) => {
                                        this.setState({
                                            offerValidSeconds: parseInt(e.currentTarget.value)
                                        })
                                    }}>
                                        <option value="3600">1 час</option>
                                        <option value="21600">6 часов</option>
                                        <option value="43200">12 часов</option>
                                        <option value="86400">1 день</option>
                                    </select>
                                    <label htmlFor="time">Время на подтверждение</label>
                                </div>
                                <div className='col s12'>
                                    <label>
                                        <input type="checkbox" />
                                        <span>Доставка</span>
                                    </label>
                                </div>
                                <div className="col s8 input-field">
                                    <button
                                        onClick={this.purchase}
                                        className={this.props.purchaseLoading
                                            ? 'btn-small waves-effect disabled'
                                            : 'btn-small waves-effect'}
                                    >Сделать предложение продавцу
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className={this.state.modalBargainVisible ? "bottom-modal__container bottom-modal__container--active" : "bottom-modal__container"} style={{ height: '40%' }}>
                        <div className="modal-content">
                            <h4>Предложение новой цены</h4>
                            <div className="row">
                                <div className="col s4 input-field">
                                    <select
                                        value={this.state.cardId}
                                        onChange={(e) => {
                                            this.setState({ cardId: parseInt(e.currentTarget.value) })
                                        }}
                                        className='select'
                                        name="card"
                                        id="card">
                                        {userCards.map(card => {
                                            const { id, cardNumber } = card;
                                            return (
                                                <option key={id} value={id}>{cardNumber}</option>
                                            );
                                        })}
                                    </select>
                                    <label htmlFor="card">Выберите карту для оплаты</label>
                                </div>
                                <div className="col s8">
                                    <blockquote>
                                        <h4>Текущий баланс на карте: <span>{this.getCardBalance()}</span> &#x20BD;</h4>
                                    </blockquote>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col s4 input-field">
                                    <select name="time" id="time" className='select' onChange={(e) => {
                                        this.setState({
                                            offerValidSeconds: parseInt(e.currentTarget.value)
                                        })
                                    }}>
                                        <option value="3600">1 час</option>
                                        <option value="21600">6 часов</option>
                                        <option value="43200">12 часов</option>
                                        <option value="86400">1 день</option>
                                    </select>
                                    <label htmlFor="time">Время на подтверждение</label>
                                </div>

                                <Mat.TextInput value={'' + this.state.newPrice} onChange={e => {
                                    const val = Math.abs(parseInt(e.currentTarget.value)) || 0;
                                    const { price } = this.props.announcement;
                                    if (price > val) {
                                        this.setState({
                                            newPrice: val
                                        });
                                    } else {
                                        this.setState({
                                            newPrice: price - 1
                                        })
                                    }

                                }} validate label='Новая цена' inputClassName='s4' />


                                <div className="col s4 input-field">
                                    <textarea className='materialize-textarea' name="description"
                                        value={this.state.bargainDescription} onChange={event => {
                                            this.setState({
                                                bargainDescription: event.currentTarget.value
                                            })
                                        }}></textarea>
                                    <label htmlFor="description">Описание причины снижения цены</label>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <Mat.Button disabled={this.validateBargainBtn()} onClick={sendBargain}
                                small> Предложить </Mat.Button>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

function mapState(state): object {
    const { userCards } = state.authentication.user;
    const { purchaseLoading } = state.actions;
    const { user } = state.authentication;
    const { singleAnnouncement } = state.listings;
    const announcement = singleAnnouncement;
    return { user, userCards, purchaseLoading, announcement };
}

const actionCreators = {
    purchaseGood: userActions.purchase,
    purchaseBargain: userActions.purchaseBargainOffer,
    getAnnouncementById: listingActions.getById,
    close: listingActions.close
};

const connectedListingPage = connect(mapState, actionCreators)(AnnouncementPage);
export { connectedListingPage as ListingPage };
