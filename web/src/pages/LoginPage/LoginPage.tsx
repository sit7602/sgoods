import GoogleLogin, { GoogleLogout } from 'react-google-login';
import {userActions} from "../../actions";
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {ReactNode} from "react";
import * as React from "react";
import './style.css';

type LoginPageProps = {
    loggingIn: boolean;
    logout: any;
    login: any;
};

class LoginPage extends React.Component<LoginPageProps, any> {
    constructor(props) {
        super(props);
        this.props.logout();
    }

    success = (response): void => {
        const accessToken = response.uc !== undefined ? response.uc.access_token : undefined || response.accessToken;
        this.props.login(accessToken);
    };

    error = (response): void => {
        console.log(response);
    };

    render(): ReactNode {
        const {loggingIn} = this.props;

        const clientId = '302725075150-3ehmnkptnufankminuroda9f7fnnb63i.apps.googleusercontent.com';
        return (
            <div className='content'>
                <span id="splash-overlay" className="splash"/>
                <span id="welcome" className="z-depth-4"/>
                <main className="valign-wrapper">
                <span className="container black-text text-lighten-1 hello-block">
                    <p className="flow-text flow-text-mobile hello-text">Приветствуем вас на </p>
                    <h2 className="title black-text text-lighten-3">Торговой площадке S.GOODS</h2>
                    <blockquote className="flow-text login-flow-text">Место для безопасных сделок</blockquote>
                    <div className="center-align google-login-button">
                        <GoogleLogin
                            onSuccess={this.success}
                            onFailure={this.error}
                            clientId={clientId}
                            responseType="token"
                            buttonText="Войти"
                            scope="profile"
                            className="loginBtn"
                        />
                    </div>
                </span>
                </main>
            </div>
        );
    }
}

function mapState(state) {
    const {loggingIn} = state.authentication;
    return {loggingIn};
}

const actionCreators = {
    login: userActions.login,
    logout: userActions.logout
};

const connectedLoginPage = connect(mapState, actionCreators)(LoginPage);
export {connectedLoginPage as LoginPage};
