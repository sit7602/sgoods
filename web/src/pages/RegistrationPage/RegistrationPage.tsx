import {userActions} from "../../actions";
import * as M from "materialize-css";
import {connect} from "react-redux";
import {ReactNode} from "react";
import * as React from "react";
import "./style.css";
import * as moment from 'moment';
import {showMessage} from "../../helpers/utils";


type RegistrationPageProps = {
    user: {
        id: bigint;
    };
    register: any;
    userCards: any;
};

type RegistrationPageState = {
    phone: string;
    hideUserDate: boolean;
    callTimeStart: any;
    callTimeEnd: any;
    utc: number;
    townId: number;
    address: string;
    currentStep: number;
    defaultUserCardId: number;
};

const parseTime = (strTime) => {
    const hours = parseInt(strTime.split(':')[0]) * 60;
    const minutes = parseInt(strTime.split(':')[1]);
    const endTime = hours + minutes;
    return endTime;
}

const unparseTime = (mintes) => {
    let hours = Math.trunc(mintes/60);
    if (hours < 10) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        hours = "0" + hours.toString()
    }
    let minutes = mintes - hours * 60;
    if (minutes < 10) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        minutes = "0" + minutes.toString();
    }
    return `${hours}:${minutes}`;
}

class RegistrationPage extends React.Component<RegistrationPageProps, RegistrationPageState> {
    constructor(props) {
        super(props);
        this.state = {
            phone: '+78005553535',
            hideUserDate: false,
            callTimeStart: "",
            callTimeEnd: "",
            utc: 5,
            townId: 1,
            address: 'Mira 10',
            currentStep: 1,
            defaultUserCardId: -1,
        };
        this.initPickers.bind(this);
        this.stepClick.bind(this);
    }

    componentDidMount(): void {
        this.init();
    }

    init = (): void => {
        this.initPickers();
        this.initSelect();
        this.initTabs();
    };

    registration = (): void => {

        if (this.state.defaultUserCardId === -1) {
            showMessage('Задайте кредитную карту по умолчанию', '#')
        } else {
            const userId = this.props.user.id;
            const {
                townId,
                hideUserDate,
                phone,
                address,
                utc,
                defaultUserCardId
            } = this.state;
            let {
                callTimeEnd,
                callTimeStart
            } = this.state;

            callTimeStart = hideUserDate
                ? moment(callTimeStart,  'HH:mm:ss').diff(moment()
                    .startOf('day'), 'seconds')
                : null;
            callTimeEnd = hideUserDate
                ? moment(callTimeEnd, 'HH:mm:ss').diff(moment()
                    .startOf('day'), 'seconds')
                : null;
            const randPhone = '' + parseInt(String(Math.random() * 100000000000));
            const formData = {
                userDetails: {
                    userId: userId,
                    phone: randPhone.slice(1)
                },
                userSettings: {
                    userId: userId,
                    userHideDate: hideUserDate,
                    callTimeStart: callTimeStart,
                    callTimeEnd: callTimeEnd,
                    UTC: utc,
                    defaultUserCardId: defaultUserCardId
                },
                townId: townId,
                Address: address
            };
            this.props.register(this.props.user, formData);
        }


    };

    initPickers = (): void => {
        const pickerStart = document.querySelectorAll('.timepicker-start');
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this;
        M.Timepicker.init(pickerStart, {
            twelveHour: false,
            i18n: {
                cancel: "Отмена",
                done: "Сохранить"
            },
            onSelect: function (hour, minute) {
                const callTimeStart = `${hour}:${minute}`;
                console.log(self.state.callTimeEnd);
                let callTimeEnd = self.state.callTimeEnd;
                const thisTime = parseTime(callTimeStart);
                if (callTimeEnd !== '') {
                    const endTime = parseTime(callTimeEnd);
                    if (endTime < thisTime) {
                        // callTimeEnd = unparseTime(thisTime + 1);
                    }
                }
                self.setState({
                    callTimeStart,
                    callTimeEnd
                });
            }
        });

        const pickerEnd = document.querySelectorAll('.timepicker-end');
        M.Timepicker.init(pickerEnd, {
            twelveHour: false,
            i18n: {
                cancel: "Отмена",
                done: "Сохранить"
            },
            onSelect: function (hour, minute) {
                const callTimeEnd = `${hour}:${minute}`;
                let callTimeStart = self.state.callTimeStart;

                if (callTimeStart !== '') {
                    const startTime = parseTime(callTimeStart);
                    const endTime = parseTime(callTimeEnd);
                    if (endTime < startTime) {
                        // callTimeStart = unparseTime(endTime - 1);
                    }
                }

                self.setState({callTimeEnd, callTimeStart});
            }
        })
    };

    initSelect = (): void => {
        const elems = document.querySelectorAll('.select');
        M.FormSelect.init(elems, {});
    };

    initTabs = (): void => {
        const tabs = document.querySelectorAll('.tabs');
        if (tabs) {
            M.Tabs.init(tabs, {});
        }
    };

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    isFormValid = (): boolean => {
        const { phone, address} = this.state;
        return !!address.length && /^((\+7)+([0-9]){10})$/gm.test(phone) ;
    };

    stepClick = (e): void => {
        const elem = e.target;
        const id = parseInt(elem.id.split('-')[1]);
        if (this.isFormValid()) {
            this.setState({currentStep: id})
        } else {
            this.setState({currentStep: 1})
        }
    };

    btnRegisterActive = (): boolean => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        const { currentState } = this.state;
        return this.isFormValid() && currentState === 2;
    };

    render(): ReactNode {
        return (
            <div className="content content__registration-page center-align container">
                <div className="row navSection" style={{display: 'none'}}>
                    <div className="col s12">
                        <h3>Завершение регистрации</h3>
                    </div>
                    <div className="col s12">
                        <ul className="tabs">
                            <li  className="tab col s6">
                                <a id='step-1'
                                   onClick={this.stepClick}
                                   href='#step1'
                                   className={this.state.currentStep === 1 ? "active" : ""}
                                >Личная информация</a>
                            </li>
                            <li  className={this.isFormValid() ? "tab col s6" : 'tab col s6 disabled'}>
                                <a id='step-2'
                                   onClick={this.stepClick}
                                   href='#step2'
                                   className={this.state.currentStep === 2 ? "active" : ""}
                                >Настройки аккаунта</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="row register-form">
                    <div className="col s12">
                        <h3>Завершение регистрации</h3>
                    </div>
                    <div className="col s6 input-field">
                        <input
                            value={this.state.phone}
                            onChange={(e) =>{
                                const val = e.target.value;
                                this.setState({phone: val})
                            }}
                            type="tel"
                            name="phone"
                            className={this.isFormValid() ? 'validate valid' : 'validate invalid'}
                            placeholder='892202021232'
                            disabled
                        />
                        <label className="active" htmlFor="phone">Телефон</label>
                    </div>
                    <div className="col s6 input-field" style={{display: "none"}}>
                        <select className="select"
                                name="town"
                                onChange={(e) => {
                                    this.setState({townId: parseInt(e.currentTarget.value)})
                                }}
                                value={this.state.townId}>
                            <option value="1">Екатеринбург</option>
                        </select>
                        <label htmlFor="town">Город</label>
                    </div>
                    <div className="col s6 input-field">
                        <select
                            name="utc"
                            className="select"
                            onChange={(e) => {
                                this.setState({
                                    utc: parseInt(e.currentTarget.value)
                                })
                            }}
                            value={this.state.utc}
                        >
                            <option value="+4" disabled>+4</option>
                            <option value="+5">+5</option>
                        </select>
                        <label htmlFor="utc">Часовой пояс</label>
                    </div>
                    <div className="col s6 input-field required">
                        <select
                            name="card"
                            className="select"
                            value={this.state.defaultUserCardId}
                            onChange={(e) => {
                                this.setState({defaultUserCardId: parseInt(e.currentTarget.value)});
                            }}>
                                <option key={-1} value="-1" disabled>Выберите карту по умолчанию</option>
                                {this.props.userCards.map(card => {
                                    const {id, cardNumber} = card;
                                    return (
                                        <option key={id} value={id}>{cardNumber}</option>
                                    );
                                })}
                        </select>
                        <label htmlFor="card"><span style={{color: 'red'}}>* </span>Кредитная карта по умолчанию</label>
                    </div>
                    <div className="col s6 input-field">
                        <p>
                            <label>
                                <input
                                    type="checkbox"
                                    checked={this.state.hideUserDate}
                                    onChange={(e) =>
                                        this.setState({hideUserDate: e.target.checked})
                                    }
                                />
                                <span>Скрыть номер телефона</span>
                            </label>
                        </p>
                    </div>
                    <div className='col s6 input-field' hidden={!this.state.hideUserDate}>
                        <input
                            type="text"
                            name="timestart"
                            className="timepicker-start"
                            value={this.state.callTimeStart || '09:00'}
                            onChange={(e) => {
                                console.log('SAD');
                                const callTimeEnd = this.state;
                                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                                // @ts-ignore
                                if (callTimeEnd !== '') {
                                    console.log('IFFF');
                                    console.log(callTimeEnd);
                                }
                                console.log('sdfsdf');
                                console.log(callTimeEnd);

                                this.setState({
                                    callTimeStart: e.target.value
                                })
                            }}
                        />
                        <label className="active" htmlFor="timestart">Время начала</label>
                    </div>
                    <div className='col s6 input-field' hidden={!this.state.hideUserDate}>
                        <input
                            type="text"
                            name="timeend"
                            className="timepicker-end"
                            value={this.state.callTimeEnd || '18:00'}
                            onChange={(e) => {
                                this.setState({
                                    callTimeEnd: e.target.value
                                })
                            }}
                        />
                        <label className="active" htmlFor="timeend">Время окончания</label>
                    </div>
                    <div className="col s6">
                        <button
                            onClick={this.registration}
                            className="btn green waves-effect btn-large"
                        >Завершить регистрацию</button>
                    </div>
                </div>
                <div className="row" hidden={this.state.currentStep === 1}>
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const { userCards } = state.authentication.user;
    const { user } = state.authentication;
    return { user, userCards };
}

const actionCreators = {
    register: userActions.register
};

const connectedRegistrationPage = connect(mapState, actionCreators)(RegistrationPage);
export {connectedRegistrationPage as RegistrationPage};
