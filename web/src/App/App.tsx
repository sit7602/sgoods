import { AnnouncementCreatePage } from '../pages/Announcement/create/AnnouncementCreatePage';
import { RegistrationPage } from "../pages/RegistrationPage/RegistrationPage";
import { ListingPage } from "../pages/AnnouncementPage/AnnouncementPage";
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { PrivateRoute } from '../components/PrivateRoute';
import { UserAccountPage } from '../pages/UserAccountPage';
import { PosterPage } from '../pages/PosterPage';
import { LoginPage } from '../pages/LoginPage';
import { Header } from '../components/Header';
import { HomePage } from '../pages/HomePage';
import PageInterface from './PageInterface';
import { alertActions } from '../actions';
import { connect } from 'react-redux';
import { history } from '../helpers';
import * as React from "react";
import './style.css';

type AppState = {};
type AppProps = {
    alert: any;
    user: any;
};

class App extends React.Component<AppProps, AppState> {

    constructor(props) {
        super(props);
        history.listen((location, action) => {
            // this.props.clearAlerts();
            console.log(location)
        });
    }

    render() {
        const {alert, user} = this.props;
        return (
            <div>
                <Router history={history}>
                    <Header {...user} />
                    <div className="container main-app--container">
                        <Switch>
                            <PrivateRoute exact path="/" component={HomePage}/>
                            <Route path="/login" component={LoginPage}/>
                            <PrivateRoute path="/listing/edit" component={AnnouncementCreatePage}/>
                            <PrivateRoute path='/registration' component={RegistrationPage}/>
                            <PrivateRoute path="/account" component={UserAccountPage}/>
                            <PrivateRoute path="/poster" component={PosterPage}/>
                            <Route path='/listing/:id'>
                                <ListingPage/>
                            </Route>
                            <Redirect from="*" to="/"/>
                        </Switch>

                    </div>
                </Router>
            </div>

        );
    }
}

function mapState(state) {
    const {alert} = state;
    const {user} = state;
    return {alert, user};
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export {connectedApp as App};
