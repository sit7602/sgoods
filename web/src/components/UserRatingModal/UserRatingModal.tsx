import * as React from "react";
import * as Mat from "react-materialize";
import { Modal, Button } from "antd";
import StarRatings from "react-star-ratings";
import { userService } from "../../services";
// import 

type UserRatingModalProps = {
    closeModal: any;
    visible: boolean;
    offerId: number;
};

type UserRatingModalState = {
    mark: number;
    description: string;
};

export class UserRatingModal extends React.Component<UserRatingModalProps, UserRatingModalState> {
    constructor(props) {
        super(props);
        this.state = {
            mark: 5,
            description: ""
        };

        this.sendRating = this.sendRating.bind(this);
        this.validateComment = this.validateComment.bind(this);
    }

    sendRating(): void {
        const { offerId } = this.props;
        const { description, mark } = this.state;
        const data = {
            offer_id: offerId,
            description: description,
            mark: mark
        };        
        if (this.validateComment()) {
            this.props.closeModal();
            userService.updateRating(data)    
        }
    }

    validateComment(): boolean {
        const {mark, description} = this.state;
        return (mark === 5 && description.length === 0) || 
            (description.length > 0);
    }

    render() {
        const { sendRating, validateComment } = this;
        const { closeModal, visible } = this.props;
        const { mark, description } = this.state;
        return (
            <Modal 
                title='Оцените сделку' 
                centered 
                okText='Отправить'
                cancelText='Закрыть'
                onOk={sendRating} 
                onCancel={closeModal} 
                visible={visible}>
                <Mat.Row>
                    <Mat.Col l={12} s={12}>
                        <StarRatings
                            rating={mark}
                            starRatedColor="blue"
                            numberOfStars={5}
                            starDimension="30px"
                            starSpacing="3px"
                            name='rating'
                            changeRating={(mark) => {
                                this.setState({ mark: mark });
                            }}
                        />
                    </Mat.Col>
                    <Mat.Col l={12} s={12}>
                        <Mat.TextInput l={12}
                            m={12}
                            s={12}
                            xl={12} 
                            label='Комментарий'
                            value={description}
                            error='Обязательно при рейтинге < 5'
                            validate
                            inputClassName={validateComment() ? 'valid' : 'invalid'}
                            onChange={(value) => {
                                this.setState({
                                    description: value.currentTarget.value
                                })
                            }} />
                    </Mat.Col>
                </Mat.Row>
            </Modal>
        );
    }
}