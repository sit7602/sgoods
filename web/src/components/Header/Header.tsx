import {Notifications} from "../Notification/Notifications";
import {Link, NavLink} from "react-router-dom";
import {ReactElement, ReactNode} from "react";
import * as Mat from "react-materialize";
import {userActions} from "../../actions/index";
import * as M from "materialize-css";
import {connect} from "react-redux";
import * as React from "react";
import './style.css';


type HeaderProps = {

    photo: string;
    firstName: string;
    lastName: string;
    userDetails: any;
    userSettings: any;
    registered: boolean;
};

type HeaderState = {

}

class Header extends React.Component<HeaderProps, HeaderState>{
    private dropdown: null;
    constructor(props) {
        super(props);
        this.dropdown = null;
    }

    componentDidMount(): void {

        this.initMenu();
    }

    componentDidUpdate(): void {
        this.initMenu();
    }


    initMenu = (): void => {
        const dropdown = document.querySelectorAll('.dropdown-trigger');
        const instances = M.Dropdown.init(dropdown, {});
    };

    lessonMenu = (): ReactElement => {
        const trigger = <Mat.Button>Обучение</Mat.Button>;
        return (
            <div className='right' style={{marginRight: 225, display: 'none'}}>
                  <Mat.Modal header="Выставить товар на продажу?" trigger={trigger}>
                    <div className="Row"><div className="col s12">
                        <p>Как продать товар?</p>
                    </div></div>
                      <div className="row"><div className="col s12">
                          <div className="row"><Mat.Button>Да</Mat.Button>
                      <Mat.Button className='red'>Нет</Mat.Button></div>
                      </div></div>
                  </Mat.Modal>
            </div>
        )
    }

    userMenu = (): ReactNode => {
        const { photo, firstName, lastName } = this.props;
        if (firstName) {
            return (
                <div>
                    <ul id="dropdown1" className="dropdown-content">
                        <li style={window.location.pathname === '/' ? {}:{}}>
                            <Link to='/'>На главную</Link>
                        </li>
                        <li><Link to="/account" >Личный кабинет</Link></li>
                        <li className="divider"/>
                        <li><Link to="/login">Выйти</Link></li>
                    </ul>
                    <div className='userTop--block'>
                        <span className="userNameText">{firstName} {lastName}</span>
                        <div className="userIcon dropdown-trigger btn waves-effect waves-light green" data-target="dropdown1">
                            <img src={photo}/>
                        </div>
                    </div>
                </div>
            );
        }
    };

    searchComponent = (): ReactElement => {
        const { firstName } = this.props;
        if (firstName) {
            return (
                <div hidden className="input-field col s3 m3 l3 xl3 offset-s3 pull-l3 search-bar">
                    <i className="material-icons prefix">search</i>
                    <input type="text" name="search" className="validate" />
                    <label htmlFor="search">Поиск объявлений</label>
                </div>
            );
        }
    }

    notifications = (): ReactElement => {
        const { firstName } = this.props;
        if (firstName) {
            return <Notifications/>
        }
    }

    makeAnnouncementButton = () => {
        const { firstName } = this.props;
        if (firstName) {
            return (
                <NavLink to='/listing/edit'>
                <Mat.Button className='sell-btn'>
                    Разместить объявление
                </Mat.Button>
                </NavLink>
            );
        }
    }

    render(): ReactNode {
        return (
            <div className='navbar-fixed'>
                <nav>
                    <div className="nav nav-wrapper--custom">
                        <Link to={window.location.pathname === '/login' ? "#" : "/"} className="brand-logo">S.GOODS</Link>
                        {this.notifications()}
                        {this.searchComponent()}
                        {this.lessonMenu()}
                        {this.makeAnnouncementButton()}
                        {this.userMenu()}
                        <ul id="nav-mobile" className="right hide-on-med-and-down"/>
                    </div>
                </nav>
            </div>
        );
    }
}

function mapState(state) {
    const { user } = state.authentication;
    const {registered} = state.registration;
    return {...user, registered} || {
        photo: null,
        firstName: null,
        lastName: null,
        userDetails: null,
        userSettings: null,
        registered: false
    };

}

const actionCreators = {

};

const connectedHeader = connect(mapState, actionCreators)(Header);
export { connectedHeader as Header };
