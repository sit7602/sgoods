import { notificationsActions } from "../../actions/notifications.actions";
import { showMessage } from "../../helpers/utils";
import SockJsClient from "react-stomp";
import { connect } from "react-redux";
import * as React from "react";
import {refreshToken} from "../../helpers/fetch-with-auth";


type NotificationsProps = {
    user: any;
    data: any[];
    purchaseMessages: any[];
    offerNewMessages: any[];
    confirmationOfSalesMessages: any[];
    transferConfirmationMessages: any[];
    soldMessages: any[];
    onMessageReceive: any;
};

type NotificationsState = {
    clientConnected: boolean;
    token: string;
};

class Notifications extends React.Component<NotificationsProps, NotificationsState>{
    private clientRef: any;

    constructor(props) {
        super(props);
        this.state = {
            clientConnected: false,
            token: ""
        }
    }

    componentDidMount(): void {
        try {
            const token = JSON.parse(sessionStorage.tokens).accessToken;
            this.setState({token: token});
        } catch(e) {
            // window.location.href = '/login'
            console.log(e)
        }
    }
    

    componentWillUnmount(): void {
        this.clientRef.disconnect();
    }
    onMessageReceived = (msg): void => {
        this.props.onMessageReceive(msg);
    }

    render() {
        if (sessionStorage.tokens) {
            const authToken = this.state.token || JSON.parse(sessionStorage.tokens).accessToken;
            const socketURL = `${process.env.API_URL}/greeting/?sometoken=${authToken}`;
            const { setState } = this;
            return (
                <SockJsClient
                    url={socketURL}
                    topics={["/user/queue/reply", "/user/queue/errors", "/queue/reply"]}
                    ref={(client): void => { this.clientRef = client }}
                    options={{ 'Authorization': authToken }}
                    headers={{ 'Authorization': authToken }}
                    subscribeHeaders={{ 'Authorization': authToken }}
                    onMessage={this.onMessageReceived}
                    onConnect={(): void => {
                        console.log('CONNECTED SOCKET') }}
                    onDisconnect={(): void => {
                        console.log('DISCONNECTED SOCKET') 
                        refreshToken(this.state.token).then(r => {
                            this.setState({
                                token: r.accessToken
                            })
                        })
                    }}
                    
                />
            );
        }
        return null;

    }

}

function mapState(state) {
    const {
        user,
        notifications: {
            data,
            purchaseMessages,
            offerNewMessages,
            confirmationOfSalesMessages,
            transferConfirmationMessages,
            soldMessages
        }
    } = state;
    return {
        user,
        data,
        purchaseMessages,
        offerNewMessages,
        confirmationOfSalesMessages,
        transferConfirmationMessages,
        soldMessages
    };
}

const actionCreators = {
    onMessageReceive: notificationsActions.onMessageReceive
}

const connectedNotifications = connect(mapState, actionCreators)(Notifications);
export {connectedNotifications as Notifications};
