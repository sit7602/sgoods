import { Announcement } from '../Announcement/Announcement'
import {Loader} from "../Loader/Loader";
import {ReactElement} from "react";
import * as React from "react";
import {Empty} from "antd";

type ListingListProps = {
  listings: any;
};

export const AnnouncementList = ({listings}: ListingListProps): ReactElement => {
    if (listings === null) {
        return <Loader/>
    }
    if (listings.length === 0) {

        return <Empty description={<span>Список объявлений пуст</span>}/>
    }
    return (
        <div className='listing-row' key={1}>
            {listings.map(e=>{
                const {
                    name, price, id, createDate, productCategory, description, status,
                    type, photos, offers, isFavorite
                } = e;
                return (
                    <Announcement
                        id={id}
                        name={name}
                        price={price}
                        description={description}
                        photos={photos}
                        createDate={createDate}
                        productCategory={productCategory}
                        status={status}
                        type={type}
                        key={id}
                        offers={offers}
                        isFavorite={isFavorite}
                    />
                )
            })}
        </div>
    )
};
