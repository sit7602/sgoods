import {AnnouncementPhoto, ProductCategory} from "../../types/types";
import {formatter, IMG_PH} from "../../constants/img.constants";
import {handleImgError} from "../../helpers/utils";
import {useState, ReactNode} from "react";
import {NavLink} from "react-router-dom";
import * as moment from 'moment';
import * as React from "react";
import './style.css';
import {connect} from "react-redux";
import {listingActions} from "../../actions";
import { Popconfirm } from "antd";
import { QuestionCircleOutlined } from '@ant-design/icons';


moment.locale('ru')

export type AnnouncementProps = {
    name: string;
    price: number;
    photos: AnnouncementPhoto[];
    id: number;
    description: string;
    createDate: number;
    status: string;
    type: string;
    isFavorite: boolean;
    productCategory: ProductCategory;
    offers: any[];
    like: Function;
    dislike: Function;
    close: Function;
};
export type AnnouncementState = {
    numberVisible: boolean;
    isFavorite: boolean;
}
class Announcement extends React.Component<AnnouncementProps, AnnouncementState> {
    constructor(props) {

        super(props);
        this.state = {
            numberVisible: false,
            isFavorite: this.props.isFavorite
        }
    }

    onLike = (): void => {
        const { id } = this.props;

        if (this.state.isFavorite) {
            this.props.dislike(id);
            this.setState({isFavorite: !this.state.isFavorite})
        } else {
            this.props.like(id);
            this.setState({isFavorite: !this.state.isFavorite})
        }
    }

    onDelete = (): void => {
        const { id } = this.props;
        this.props.close(id);
    }

    render(): ReactNode {
        const {
            id,
            name,
            price,
            photos,
            createDate,
            productCategory,
        } = this.props;
        let {offers} = this.props;
        offers = offers || [];

        const url = photos.length ? `${process.env.API_URL}${photos[0].fileURL}` : IMG_PH;
        const categoryName = productCategory.category;
        const listingUrl = `/listing/${id}`;
        const activeOffersCount = offers.filter(({offerStatus:s})=>['0', '1', '2'].includes(s)).length;

        return (
            <div className='acard' key={id}>
                <div className="acard__image-container">
                    <div className="acard__image-wrapper">
                        <NavLink to={listingUrl} className='acard__image'>
                            <img className='acard__image' src={url} alt=""
                                 onError={handleImgError}/>
                        </NavLink>
                        
                        <a className="acard__add-to-favourites btn-floating halfway-fab waves-effect waves-light"
                           style={{background: 'transparent', boxShadow: 'none'}}
                           onClick={this.onLike}
                        >
                            <i className="material-icons blue add-to-favorites">{this.state.isFavorite ? 'favorite' : 'favorite_border'}</i>
                        </a>
                        <div hidden>
                            <Popconfirm
                                title="Вы уверены, что хотите удалить объявление?"
                                onConfirm={this.onDelete}
                                placement='bottomLeft'
                                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                okText="Удалить"
                                cancelText="Отменить">
                                <a className="acard__delete btn-floating halfway-fab waves-effect waves-light"
                                    style={{ background: 'transparent', boxShadow: 'none' }}
                                    href="#"
                                >
                                    <i className="material-icons acard__delete--icon">delete</i>
                                </a>
                            </Popconfirm>
                        </div>
                    </div>
                </div>
                <div className="acard__content">
                    <NavLink to={listingUrl} className="acard__title">{name}</NavLink>
                    <p className="acard__price">{formatter.format(price).replace(',', ' ')} &#8381;</p>
                    <p className="acard__category">{categoryName}</p>
                    <p className="acard__date">{moment(createDate * 1000).fromNow()}</p>
                    {activeOffersCount ? (<NavLink to='/account/notifications'>Есть заявки на покупку({activeOffersCount})</NavLink>) : ''}
                </div>
            </div>
        );
    }
}

function mapState(state): object {
    return {};
}

const actionCreators = {
    like: listingActions.like,
    dislike: listingActions.dislike,
    close: listingActions.close
};

const connectedPage = connect(mapState, actionCreators)(Announcement);
export {connectedPage as Announcement};
