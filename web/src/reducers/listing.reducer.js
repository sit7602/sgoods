import {listingConstants} from "../constants";

const initialState = {
    listings: [],
    files: [],
    listingOffers: [],
    singleAnnouncement: {}
};

export function listings(state = initialState, action) {
    switch (action.type) {
        case listingConstants.CREATE:
            return {...state, created: true};

        case listingConstants.GET_LISTINGS:
            return {...state, loading: true};

        case listingConstants.GET_LISTINGS_SUCCESS:
            return {...state, listings: action.listings, loading: false};

        case listingConstants.GET_LISTINGS_FAILURE:
            return {...state, listings: null, loading: false};

        case listingConstants.GET_ANNOUNCEMENT_BY_ID_REQUEST:
            return {...state, loading: true};

        case listingConstants.GET_ANNOUNCEMENT_BY_ID_SUCCESS:
            return {...state, singleAnnouncement: action.singleAnnouncement, loading: false};

        case listingConstants.GET_ANNOUNCEMENT_BY_ID_FAILURE:
            return {...state, singleAnnouncement: {}, loading: false};

        case listingConstants.GET_LISTINGS_USER:
            return state;

        case listingConstants.GET_LISTINGS_USER_SUCCESS:
            return {...state, listings: action.listings};

        case listingConstants.GET_LISTINGS_USER_FAILURE:
            return {...state, listings: [], error: true};

        case listingConstants.UPLOAD_FILE:
            return {...state};

        case listingConstants.UPLOAD_FILE_SUCCESS:
            return {...state, files: action.files};

        case listingConstants.UPLOAD_FILE_FAILURE:
            return {...state, files: [], error: true};

        case listingConstants.GET_OFFERS_BY_BUYER:
            return {...state, listingOffers: [], loading: true};

        case listingConstants.GET_OFFERS_BY_BUYER_SUCCESS:
            return {...state, listingOffers: action.listingOffers, loading: false};

        case listingConstants.GET_OFFERS_BY_BUYER_FAILURE:
            return {...state, listingOffers: [], error: true};

        default:
            return state;
    }
}
