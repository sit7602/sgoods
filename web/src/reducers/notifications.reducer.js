import {notificationsConstants} from "../constants";

const initialState = {
    connected: false,
    data: [],
    purchaseMessages: [],
    offerNewMessages: [],
    confirmationOfSalesMessages: [],
    transferConfirmationMessages: [],
    soldMessages: [],
    rejectMessages: []
};

export function notifications(state = initialState, action) {
    switch (action.type) {
        case notificationsConstants.CONNECT:
            return {
                ...state,
                connected: true
            }
        case notificationsConstants.DISCONNECT:
            return {
                ...state,
                connected: false
            }
        case notificationsConstants.MESSAGE_RECEIVED:
            return {
                ...state,
                connected: true,
                data: state.data.concat(action.data)
            }
        case notificationsConstants.MSG_CONFIRMATION_OF_SALES:
            return {
                ...state,
                confirmationOfSalesMessages: state.confirmationOfSalesMessages
                    .concat(action.confirmationOfSalesMessages)
            };
        case notificationsConstants.MSG_OFFER_NEW_PRICE:
            return {
                ...state,
                offerNewMessages: state.offerNewMessages
                    .concat(action.offerNewMessages)
            };
        case notificationsConstants.MSG_PURCHASE_OFFER:
            return {
                ...state,
                purchaseMessages: state.purchaseMessages
                    .concat(action.purchaseMessages)
            };
        case notificationsConstants.MSG_TRANSFER_CONFIRMATION:
            return {
                ...state,
                transferConfirmationMessages: state.transferConfirmationMessages
                    .concat(action.transferConfirmationMessages)
            };
        case notificationsConstants.MSG_SOLD:
            return {
                ...state,
                soldMessages: state.soldMessages
                    .concat(action.soldMessages)
            };
        case notificationsConstants.MSG_REJECT_MESSAGE:
            return {
                ...state,
                rejectMessages: state.rejectMessages
                    .concat(action.rejectMessages)
            }
        default:
            return state;
    }
}
