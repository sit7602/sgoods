import {userConstants} from "../constants";

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user && user.userDetails !== null ? {registered: true, user} : {registered: false, user};

export function registration(state = initialState, action) {
    switch (action.type) {
        case userConstants.REGISTRATION_REQUEST:
            return {
                registered: false,
                user
            };
        case userConstants.REGISTRATION_SUCCESS:
            return {
                registered: true,
                user
            };
        case userConstants.REGISTRATION_FAILURE:
            return {
                registered: false,
                user
            };
        default:
            return state;
    }
}
