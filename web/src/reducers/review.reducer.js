import {reviewConstants} from "../constants";

const initialState = {
    userReviews: [],
    myReviews: []
};

export function review(state = initialState, action) {
    switch (action.type) {
        case reviewConstants.GET_USER_REVIEW_REQUEST:
            return {...state};
        case reviewConstants.GET_USER_REVIEW_SUCCESS:
            return {...state, userReviews: action.userReviews};
        case reviewConstants.GET_USER_REVIEW_FAILURE:
            return {...state};
        case reviewConstants.GET_MY_REVIEW_REQUEST:
            return {...state};
        case reviewConstants.GET_MY_REVIEW_SUCCESS:
            return {...state, myReviews: action.myReviews};
        case reviewConstants.GET_MY_REVIEW_FAILURE:
            return {...state};
        default:
            return state;
    }
}