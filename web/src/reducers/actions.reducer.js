import {userConstants} from "../constants";

const initialState = {};

export function actions(state = initialState, action) {
    switch (action.type) {
        case userConstants.PURCHASE_REQUEST:
            return {
                purchaseLoading: true,
                success: false
            };
        case userConstants.PURCHASE_SUCCESS:
            return {
                purchaseLoading: false,
                success: true
            };
        case userConstants.PURCHASE_FAILURE:
            return {
                purchaseLoading: false,
                success: false
            }
        case userConstants.ACCEPT_PURCHASE_REQUEST:
            return {
                acceptPurchaseLoading: true,
                success: false
            };
        case userConstants.ACCEPT_PURCHASE_SUCCESS:
            return {
                acceptPurchaseLoading: true,
                success: true
            };
        case userConstants.ACCEPT_PURCHASE_FAILURE:
            return {
                acceptPurchaseLoading: false,
                success: false
            };
        case userConstants.COMPLETE_PURCHASE_REQUEST:
            return {
                completePurchaseLoading: true,
                success: false
            };
        case userConstants.COMPLETE_PURCHASE_SUCCESS:
            return {
                completePurchaseLoading: true,
                success: true
            };
        case userConstants.COMPLETE_PURCHASE_FAILURE:
            return {
                completePurchaseLoading: false,
                success: false
            };
        default:
            return state;
    }
}
