import { categoryConstants } from "../constants";

const initialState = {
    categories: [],
    categoryFields: []
};

export function category(state = initialState, action) {
    switch (action.type) {
        case categoryConstants.GET_CATEGORY_REQUEST:
            return {...state, categories: []};
        case categoryConstants.GET_CATEGORY_SUCCESS:
            return {...state, categories: action.categories};
        case categoryConstants.GET_CATEGORY_FAILURE:
            return {...state};
        case categoryConstants.GET_FIELDDESCRIPTION_REQUEST:
            return {...state, categoryFields: []};
        case categoryConstants.GET_FIELDDESCRIPTION_SUCCESS:
            return {...state, categoryFields: action.categoryFields};
        case categoryConstants.GET_FIELDDESCRIPTION_FAILURE:
            return {...state};
        default:
            return state;
    }
}
