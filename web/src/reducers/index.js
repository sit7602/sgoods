import { combineReducers } from 'redux';

import { alert } from './alert.reducer';
import { authentication } from './authentication.reducer';
import { registration } from "./registration.reducer";
import { listings } from "./listing.reducer"
import { category } from "./category.reducer";
import {actions} from "./actions.reducer";
import {notifications} from "./notifications.reducer";
import { review } from "./review.reducer";

const rootReducer = combineReducers({
    authentication,
    registration,
    alert,
    listings,
    category,
    actions,
    notifications,
    review
});
export default rootReducer;
